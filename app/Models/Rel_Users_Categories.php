<?php
 namespace App\Models;

use CodeIgniter\Model;

class Rel_Users_Categories extends Model
{
  protected $table      = 'REL_USERS_CATEGORIES';
  protected $primaryKey = 'id_ruc';
 
  protected $returnType = 'object';
  protected $useSoftDeletes = false;

  protected $allowedFields = ['id_user','id_category'];

  protected $useTimestamps = false;
  protected $createdField  = '';
  protected $updatedField  = '';
  protected $deletedField  = '';

  protected $validationRules    = [];
  protected $validationMessages = [];
  protected $skipValidation     = true;

  public $customer;
}