<?php

namespace App\Models;

use CodeIgniter\Model;

class User extends Model
{
  protected $db;
  protected $table      = 'USERS';
  protected $primaryKey = 'id_user';

  protected $returnType = 'object';
  protected $allowedFields = ['name', 'password', 'email','language'];
  protected $tempReturnType = 'object';

  public $userLevels;               // array con los niveles de permisos del usuario activo
  public $userdata;                 // object with Database fields
  
  public function __construct($id_user)
   {
    $this->db = \Config\Database::connect();
    $this->userdata = $this->find($id_user);
    if(is_null($this->userdata))
     return ;
    $this->getLevels($id_user);
    $this->userdata->token = $this->getToken();
   }
  private function getLevels($id_user)
   {
    $qry = " 
    SELECT level 
      FROM REL_USERS_CATEGORIES
      JOIN USERS_CATEGORIES ON (REL_USERS_CATEGORIES.id_category = USERS_CATEGORIES.id_user_category)
    WHERE id_user = {$id_user}
    ";
    $result = $this->db->query($qry)->getResult();
    foreach ($result as $r)
      $this->userLevels[] = $r->level;
    if (!$result)  // no categories where set
      $this->userLevels[] = [1];     //set to external user by default
   } // end of function

  private function getToken()
   {
     return md5($this->userdata->email.$this->userdata->password);
   }
} // end of class