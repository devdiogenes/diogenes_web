<?php

namespace App\Controllers;

class api2 extends BaseController2
{

  public $permitido; // permitidos todos los niveles de seguridad
  protected $log;
  protected $verbose = false;
  
  public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: POST, OPTIONS, PUT, DELETE");
    $gp = New \App\Models\Global_parms();
    $this->verbose =($gp->getParm('api.verbose')->value == 'true');
    }
    /////////////////////////////////////// p?¿?¿?? ///////////////////////////////////////////////////
    public function PGetDetailsUser()
    {
      $this->allow(array(16));    
      $this->user->log("PGetDetailsUser");
      if(! isset($_POST['id_user']))
        $this->send_response(array());  
      else
        $this->send_response($this->getUser($_POST['id_user']));
    }
    private function getUser($id_user)
    {
      $usr = New \App\Models\User($id_user);
      unset($usr->pager);
      return $usr;
    }
  
      //////////////////////////////////////////// Procedimientos de usuario (?)////////////////////////////
  
  public function MCgetStatus()
   {
    $this->allow(array(1,2,4,8,16));    
    $mc = new \App\Models\Metaclean();
    $id_analysis = $this->getAnalysisFromExtendedToken();
    $this->user->log("MC Status $id_analysis");
    $this->send_response($mc->getStatus($id_analysis));
   }
  
    //////////////////////////////////////////// Metacleaner //////////////////////////////////////////
  public function MCRequestClean()
   {
    $this->allow(array(1,2,4,8,16));      
    $mc = new \App\Models\Metaclean();
    $id_analysis = $this->getAnalysisFromExtendedToken();
    $mc->request($id_analysis);

    $Analysis = New \App\Models\Analysis($id_analysis);
    $Conta = New \App\Models\Conta();
    $data = New \stdClass();    // Clear $data and set it for a new albaran
    $data->codcliente = $this->user->customer->codcliente;
    $data->fecha =date('Y-m-d');
    $data->hora = date('H:i:s');
    $data->cantidad = 1;
    $data->idproducto = 11;
    $data->descripcion = "Limpieza del análisis {$Analysis->domain_name}";
    $data->pvp_unitario = 10;                       // To be read from customers table.
          
    $Conta->albaranCrear($data);
    $this->user->log("MC ordenada limpieza $Analysis->domain_name ($id_analysis)");
    $this->send_response($mc->row);

   }
  /////////////////////////////////////// Verics /////////////////////////////////////////////////////
  /////////////////////////////////////// Verics Utilities ///////////////////////////////////////////
  public function VAnalysisGetStatus()
   {
    $this->allow(array(1,2,4,8,16));                        // user should be logged in
    if(isset($_POST['id_analysis']))
    {
      $this->userowns($_POST['id_analysis']);
      $id_analysis = $_POST['id_analysis'];
    }
    else
      $this->send_response(array('code' => 496, 'error' => 'Invalid parameter','details'=>'No analysis identifier provided'),true);
      $this->user->log("Analysis Get Status $id_analysis");
    $Analysis = New \App\Models\Analysis($id_analysis);
    $this->send_response($Analysis->getExtendedInfo());

   }
  public function VAnalysisGetBrief()
   {
    $this->allow(array(1,2,4,8,16));                        // user should be logged in
    if(isset($_POST['id_analysis']))
    {
      $this->userowns($_POST['id_analysis']);
      $id_analysis = $_POST['id_analysis'];
    }
    else
      $this->send_response(array('code' => 496, 'error' => 'Invalid parameter','details'=>'No analysis identifier provided'),true);
    $Analysis = New \App\Models\Analysis($id_analysis);
    $this->user->log("Analysis Get Brief $id_analysis");
    if ( \is_null($Analysis->getExtendedInfo()->analysis->date_started))
      $this->send_response($Analysis->getExtendedInfo());   // if the analysis isn't even started, it returns the same as VAnalysisGetStatus()
    $response = New \stdClass();
    $response->status   = $Analysis->getExtendedInfo();
    $response->docs     = $Analysis->get_num_docs();
    $response->leakages = $Analysis->get_filtraciones();
    $response->sensible = $Analysis->get_sensible();
    $this->send_response($response);
   }
  public function VAnalysisRequest()
   {
    $this->allow(array(1,2,4,8,16));                        // user should be logged in
    if(isset($_POST['id_company']))
    {
      $id_company = $_POST['id_company'];
    }
    else
      $this->send_response(array('code' => 496, 'error' => 'Invalid parameter','details'=>'No company identifier provided'),true);
    $Company = New \App\Models\Company();
    $domains = $Company->getDomain($id_company);
    if(count($domains)>1)
      $this->send_response(array('code' => 496, 'error' => 'Invalid parameter','details'=>'The company has more than one domain '),true);
    $domain = $domains[0];  // take the single row
    $Analysis = New \App\Models\Analysis();
    $data = $Analysis->analysis_add(array('id_domain'=>$domain->id_domain));
    $this->user->log("VAnalisysRequest {$domain->domain}");
    $data['token'] = $Analysis->getToken($data['id_analysis']);
    $this->send_response(array('token'       => $data['token'],
                               'id_analysis' => $data['id_analysis']));
   }
  /////////////////////////////////////// Verics Report //////////////////////////////////////////////
  public function VRSummary()
   {
    $verics_report = $this->setReportFromExtendedToken();
    if($this->verbose)  
      $this->user->log("VRSSummary {$verics_report->domain}");
    $this->send_response($verics_report->get_s_summary());
   }
  public function VRRaw_Table()
   {
    $verics_report = $this->setReportFromExtendedToken();
    $this->user->log("VRRaw_Table {$verics_report->domain}");
    $this->send_response($verics_report->get_s_raw_table($this->getPag()));
   }
  ////////////////////////////////////// Leakages ////////////////////////////////////////////////
  public function VRLeakages_Equalizer()
   {
    $verics_report = $this->setReportFromExtendedToken();
    if($this->verbose)  
      $this->user->log("VRLeakages_Equalizer {$verics_report->domain}");
    $this->send_response($verics_report->get_s_leakages_g121());
   }
  public function VRLeakages_Cheddar()
   {
    $verics_report = $this->setReportFromExtendedToken();
    if($this->verbose)  
      $this->user->log("VRLeakages_Cheddar {$verics_report->domain}");

    $this->send_response($verics_report->get_s_leakages_g122());
   }
  public function VRLeakages_Table()
   {
    $verics_report = $this->setReportFromExtendedToken();
    $this->user->log("VRLeakages_Table {$verics_report->domain}");
    $this->send_response($verics_report->get_s_leakages_table($this->getPag()));
   }
  ///////////////////////////////////// Documents //////////////////////////////////////////////
  public function VRDocuments_Equalizer()
   {
    $verics_report = $this->setReportFromExtendedToken();
    if($this->verbose)  
      $this->user->log("VRDocuments_Equalizer {$verics_report->domain}");
    $this->send_response($verics_report->get_s_documents_g121());
   }
  public function VRDocuments_Cloud()
   {
    $verics_report = $this->setReportFromExtendedToken();
    if($this->verbose)  
      $this->user->log("VRDocuments_Cloud {$verics_report->domain}");
    $this->send_response($verics_report->get_s_documents_g122());
   }
  public function VRDocuments_Table()
   {
    $verics_report = $this->setReportFromExtendedToken();
    $this->user->log("VRDocuments_Table {$verics_report->domain}");
    $this->send_response($verics_report->get_s_documents_Table($this->getPag()));
   }
  ////////////////////////////////////// Domains ////////////////////////////////////////////////
  public function VRDomains_Equalizer()
   {
    $verics_report = $this->setReportFromExtendedToken();
    if($this->verbose)  
      $this->user->log("VRDomains_Equalizer {$verics_report->domain}");
    $this->send_response($verics_report->get_s_domains_g121());
   }
  public function VRDomains_Map()
   {
    $verics_report = $this->setReportFromExtendedToken();
    if($this->verbose)  
      $this->user->log("VRDomains_Map {$verics_report->domain}");

    $this->send_response($verics_report->get_s_domains_g122());
   }
  public function VRDomains_Table()
   {
    $verics_report = $this->setReportFromExtendedToken();
    $this->user->log("VRDomains_Table {$verics_report->domain}");
    $this->send_response($verics_report->get_s_domains_Table($this->getPag()));
   }
  //////////////////////////////////////// Verics Dashboard //////////////////////////////////////////
  public function VDSnake()
   {
    $this->allow(array(1,2,4,8,16));                        // user should be logged in
    $dashboard = new \App\Models\VericsDashboard($this->user->customer->id_customer);
    if($this->verbose)  
      $this->user->log("VDSnake");
    $this->send_response($dashboard->get_g131());
   }
  public function VDMap()
   {
    $this->allow(array(1,2,4,8,16));                        // user should be logged in
    $dashboard = new \App\Models\VericsDashboard($this->user->customer->id_customer);
    if($this->verbose)  
      $this->user->log("VDMap");
    $this->send_response($dashboard->get_g132());
   }
  public function VDCheddar()
   {
    $this->allow(array(1,2,4,8,16));                        // user should be logged in
    $dashboard = new \App\Models\VericsDashboard($this->user->customer->id_customer);
    if($this->verbose)  
      $this->user->log("VDCheddar");
    $this->send_response($dashboard->get_g133());
   }
  public function VDTable()
   {
    $this->allow(array(1,2,4,8,16));                        // user should be logged in
    $page = isset($_POST['page']) ? $_POST['page'] : 0 ;    
    $itemsPerPage = isset($_POST['itemsPerPage']) ? $_POST['itemsPerPage'] : 0 ;    
    $dashboard = new \App\Models\VericsDashboard($this->user->customer->id_customer);
    $this->user->log("VDTable");
     $table = $dashboard->get_table($page, $itemsPerPage);
     $mc = new \App\Models\Metaclean();
     
     foreach ($table as &$tbl)
     {
       foreach($tbl as &$t)
        {
        if(is_null($t['id_analysis']))
          $t['mcStatus'] = NULL;
        else 
          $t['mcStatus'] = $mc->getStatus($t['id_analysis']);
        }
     }
     $this->send_response($table);

    //$this->send_response($dashboard->get_table($page, $itemsPerPage));
   }
  public function VDgetAnalysis()
   {
    $this->allow(array(1,2,4,8,16));                        // user should be logged in
    $date=date_create();
    date_sub($date,date_interval_create_from_date_string("30 days"));
    $date = date_format($date,"Y-m-d");

    $dateStart = isset($_POST['dateStart']) ? $_POST['dateStart'] : $date ;    
    $dateEnd = isset($_POST['dateEnd']) ? $_POST['dateEnd'] : date('Y-m-d');
    $dashboard = new \App\Models\VericsDashboard($this->user->customer->id_customer);
    $this->user->log("VDgetAnalysis [$dateStart - $dateEnd]");
    $this->send_response($dashboard->getAnalisisList($dateStart,$dateEnd));
   }
  /////////////////////////////////////// Actions ///////////////////////////////////////////////////
  public function Alogin($token='')
   {
    if($token == '')  
      if (isset($_POST['token']))
        $token = $_POST['token'];
      else
      {
        $this->user->log("Failed Login attempt: no token provided");
        $this->send_response(array('code'        => 498,  
                              'error'        => 'Invalid token. No token provided',
                              'details'      => ''),true);
      }
    if (strlen($token)!=32)
      {
      $this->user->log("Failed Login attempt: invalid token $token");
      $this->send_response(array('code'        => 499,  
                                'error'        => 'Invalid token',
                                'details'      => 'Invalid token'),true);
      }
    $id_user = $this->user->login();

    if ($id_user !== true )
    {
      $this->user->log("Failed Login attempt: not found $token");
      $this->send_response(array('code'        => 404,
                                 'error'       => 'Not found',
                                 'details'     => $id_user),true);
    }
    $this->user->log("Loged user {$this->user->userdata->name}");
    $this->send_response(array());
    
   }
  public function Alogout()
   {
    
    if($this->user->loged)
    {
      $this->user->logout();
      $this->user->log('Logout user '.$this->user->userdata->name);
    }
    else
      $this->user->log('Logout user <not loged>');
    echo json_encode('ok');
    die();
   }
  public function AAddCompany()       // just an alias
   {
    $this->AddCompany();
   }
  public function AddCompany() 
   {
    $this->allow(array(1,2,4,8,16));                        // user should be logged in
    // input error control
    if (!isset($_POST['company_name'])) 
    {
      $this->send_response(array('code'        => 400,
                                 'error'       => 'COMPANY_NAME is not set.',
                                 'details'     => "It seems your forgot to add 'name_comercial'"),true);
    }
    if (!isset($_POST['company_domain'])) 
    {
      $this->send_response(array('code'        => 400,
                                 'error'       => 'COMPANY_DOMAIN is not set.',
                                 'details'     => "It seems your forgot to add 'company_domain'"),true);
    }
    $domain = $this->testProtocol(urldecode($_POST['company_domain']));

    // data prepare
    $input_data = array();
    $input_data['id_customer'] = $this->user->customer->id_customer;
    $input_data['company_name'] = urldecode($_POST['company_name']);
    $input_data['domain'] = strtolower(urldecode($_POST['company_domain']));
    $input_data['company_domain'] = strtolower($domain);
    $input_data['company_email'] = urldecode($_POST['company_email']);
    $input_data['company_contact'] = urldecode($_POST['company_contact']);
    $input_data['company_phone'] = urldecode($_POST['company_phone']);
    // Classes load
    $Companies = new \App\Models\Company();
    $Domains = new \App\Models\Domain();
    $Analysis = new \App\Models\Analysis();
    $Conta = New \App\Models\Conta();
    // data recording
    $data = $Companies->company_add($input_data);
    $data = $Domains->domain_add($data);
    $data = $Analysis->analysis_add($data);
    $id_analysis = $data['id_analysis'];
    $token = \App\Models\Analysis::getToken($id_analysis);
    

    $data = New \stdClass();    // Clear $data and set it for a new albaran
    $data->codcliente = $this->user->customer->codcliente;
    $data->fecha =date('Y-m-d');
    $data->hora = date('H:i:s');
    $data->cantidad = 1;
    $data->idproducto = 11;
    $data->descripcion = "Análisis de la web {$input_data['company_domain']}";
    $data->pvp_unitario = 10;                       // To read fron customers table.
          
    $Conta->albaranCrear($data);

    // Log and end
    $this->user->log("Added company {$input_data['company_name']}");
    $this->send_response(array('token'=>$token,
                           'id_analysis'=> $id_analysis));
    
   }
  public function AMetavalueBanById()
   {
    $this->allow(array(1,2,4,8,16));                        // user should be logged in
    // input error control
    if (!isset($_POST['id_metavalue'])) 
    {
      $this->send_response(array('code'        => 400,
                                 'error'       => 'id_metavalue is not set.',
                                 'details'     => "It seems your forgot to add 'id_metavalue'."),true);
    }
    $mv = New \App\Models\Metavalue();
    $data = $mv->find($_POST['id_metavalue']);
    $this->userowns($data->id_analysis);
    $data->is_hidden_by_user = "1";
    $mv->update($data->id_metavalue,(array)$data);
    $this->send_response($data);
   } 
  public function AMetavalueBanByValue()
   {
    $this->allow(array(1,2,4,8,16));                        // user should be logged in
    // input error control
    if (!isset($_POST['id_analysis'])) 
    {
      $this->send_response(array('code'        => 400,
                                 'error'       => 'id_analysis is not set.',
                                 'details'     => "It seems your forgot to add 'id_analysis'."),true);
    }
    if (!isset($_POST['value'])) 
    {
      $this->send_response(array('code'        => 400,
                                 'error'       => 'value is not set.',
                                 'details'     => "It seems your forgot to add 'value'."),true);
    }
    $mv = New \App\Models\Metavalue();
    $this->userowns($_POST['id_analysis']);
    $this->send_response(array('values_changed'=>$mv->BanValue($_POST['id_analysis'],$_POST['value'])));
    
   }
  /////////////////////////////////////// Private functions ////////////////////////////////////////////
  private function send_response($response, $error = 0)       // general output. Format and sends json and die.
   {
    if( $error)                                               // if error, set the header
      {
        header("HTTP/1.1 {$response['code']} {$response['error']}");
        echo json_encode($response, JSON_FORCE_OBJECT);       // and the data
        die();
      }
    // Output compound
    $output = array(
                    'dataUser' => $this->user->userdata,
                    'dataCustomer' => $this->user->customer,
                    'dataWorkingCenter'  => $this->user->wcenter->wcData,
                    'payload' => $response,);
    
    echo json_encode($output);
    die();
   }
  public function login()         // log in
   {
    if (isset($_POST['token']))                                                     //if exists, auth token stored in $token
        $token = $_POST['token'];
    else
      {
        $this->user->log("Failed autoLogin attempt: no token provided");            // no token provided error
        $this->send_response(array('code'        => 498,  
                              'error'        => 'Invalid token. No token provided',
                              'details'      => ''),true);
      }
    if (strlen($token)!=32)                                                         // bad token
    {
      $this->user->log("Failed autoLogin attempt: invalid token $token");
      $this->send_response(array('code'        => 499,  
                              'error'        => 'Invalid token',
                              'details'      => 'Invalid token'),true);
    }
    $id_user = $this->user->login();                                                // Autenticates token and retrieve userid

    if ($id_user !== true )                                                         // the user couldn't logg in
    {
      $this->user->log("Failed autologin attempt: not found $token");               
      $this->send_response(array('code'        => 404,
                                 'error'       => 'Not found',
                                 'details'     => $id_user),true);
    }
   }
  private function allow($perm)   // test for permissions
   {
    $this->permitido = $perm;     //
    if(!$this->seguridad())       // user is not allowed
      if($this->user->loged)      // maybe is not logged in
      {
        $this->user->log("Unauthorised access attempt.");     // if logged in but not allowed
        $this->send_response(array('code'        => 401,
                                   'error'       => 'Unauthorized',
                                   'details'     => 'Acceso restringido'),true);
      }
      else                                                    // User not logged
      {
        $this->user->log("Attempted to access with no loged user.");
        $this->send_response(array("error"=>'No user loged. Please, login.'),true);
      }
   }
  private function getPag()       // retrieves pagination variables passed by POST method
   {
    $pag[0] = isset($_POST['page']) ? $_POST['page'] : 0 ;    
    $pag[1] = isset($_POST['itemsPerPage']) ? $_POST['itemsPerPage'] : 0 ;  
    return $pag;
   }
  private function userowns($id_analysis)   // returns false if  current user does not own the analysis identified by $id_analysis
   {
    if( ! $this->user->ownsAnalysis($id_analysis))
      $this->send_response(array('code' => 497,
                                 'error' => "Invalid parameter",
                                 'details' => "Current user does not own the requested analysis"),true);
   }
  private function setReportFromExtendedToken()   // long token. It does not auth an user. Retrieves VR Object
   {
    if(isset($_POST['token']) and strlen($_POST['token']) == 64 )  // long token. No auth needed.
    {
      $id_analysis =  \App\Models\Analysis::getId($_POST['token']);
      if (! $id_analysis)                                         // token was not found
      $this->send_response(array('code' => 499, 'error' => 'Invalid token','details'=>'No analysis found'),true);
      else
      {
        $this->user->userdata = New \stdClass();
        $this->user->customer = New \stdClass();
        $this->user->wcenter = New \stdClass();
        $this->user->wcenter->wcData = New \stdClass();
      }
    }
    else
    {
      $this->allow(array(1,2,4,8,16));                       // user should be logged in
      if(isset($_POST['id_analysis']))                       // test for existance
      {
        $id_analysis = $_POST['id_analysis'];
        $this->userowns($id_analysis);                       // test for security
        
      }
      else                                                   // 
        $this->send_response(array('code' => 496, 'error' => 'Invalid parameter','details'=>'No analysis identifier provided'),true);
    }
    return new \App\Models\VericsReport($id_analysis); 
   }
  private function getAnalysisFromExtendedToken()   // long token. It does not auth an user. Retrieves id_analysis only
   {
    if(isset($_POST['token']) and strlen($_POST['token']) == 64 )  // long token. No auth needed.
    {
      $id_analysis =  \App\Models\Analysis::getId($_POST['token']);
      if (! $id_analysis)                                         // token was not found
      $this->send_response(array('code' => 499, 'error' => 'Invalid token','details'=>'No analysis found '.$token),true);
      else
      {
        $this->user->userdata = New \stdClass();
        $this->user->customer = New \stdClass();
        $this->user->wcenter = New \stdClass();
        $this->user->wcenter->wcData = New \stdClass();
      }
    }
    else                                                     // no token sent
    {
      $this->allow(array(1,2,4,8,16));                       // user should be logged in
      if(isset($_POST['id_analysis']))                       // test for existance
      {
        $id_analysis = $_POST['id_analysis'];
        $this->userowns($id_analysis);                       // test for security
        
      }
      else                                                   // 
        $this->send_response(array('code' => 496, 'error' => 'Invalid parameter','details'=>'No analysis identifier provided'),true);
    }
    return $id_analysis; 
   }
  private function testProtocol($domain)
    {
    if(\strpos($domain,':') == false)
      $domain = "http://".$domain;
    $fdomain = str_replace("ñ","n",$domain);
    $fdomain = str_replace("Ñ","N",$fdomain);
    if (\filter_var($fdomain, FILTER_VALIDATE_URL)==false)
      $this->send_response(array('code'        => 400,
                                 'error'       => 'Bad syntax.',
                                 'details'     => "$domain does not appear to be a valid URL."),true);
      
      return $domain;
    }
   
}

