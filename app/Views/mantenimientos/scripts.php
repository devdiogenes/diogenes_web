
<script src="<?php echo base_url();?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>



<script src="<?php echo base_url();?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url();?>/plugins/chart.js/Chart.min.js"></script>
<script src="<?php echo base_url();?>/plugins/sparklines/sparkline.js"></script>
<script src="<?php echo base_url();?>/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo base_url();?>/plugins/jqvmap/maps/jquery.vmap.world.js"></script>
<script src="<?php echo base_url();?>/plugins/jquery-knob/jquery.knob.min.js"></script>
<script src="<?php echo base_url();?>/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url();?>/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url();?>/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<script src="<?php echo base_url();?>/plugins/summernote/summernote-bs4.min.js"></script>
<script src="<?php echo base_url();?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="<?php echo base_url();?>/dist/js/adminlte.js"></script>
<script src="<?php echo base_url();?>/dist/js/pages/dashboard.js"></script>
<script src="<?php echo base_url();?>/dist/js/demo.js"></script>