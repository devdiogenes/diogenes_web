<?php

namespace App\Models;

use CodeIgniter\Model;

class Company extends Model
{
  protected $table      = 'COMPANIES';
  protected $primaryKey = 'id_company';

  protected $returnType = 'object';
  protected $useSoftDeletes = false;

  protected $allowedFields = [
    'id_user', 'id_customer', 'name_comercial', 'name_legal',
    'address', 'id_city', 'contact', 'phone', 'email', 'coment'
  ];

  protected $useTimestamps = false;
  protected $createdField  = 'date_added';
  protected $updatedField  = '';
  protected $deletedField  = '';

  protected $validationRules    = [];
  protected $validationMessages = [];
  protected $skipValidation     = true;

  protected $db;

  public function __construct()
  {
    $this->db      = \Config\Database::connect();
  }

  public function company_add($data)
  {

    $builder = $this->db->table($this->table);
    $to_insert = [
      'id_customer' => $data['id_customer'],
      'name_comercial'  => $data['company_name'],
      'contact'  => $data['company_contact'],
      'email'  => $data['company_email'],
      'phone'  => $data['company_phone']
    ];
    $builder->insert($to_insert);
    
    $data['id_company'] = $this->db->insertID();
    return $data;

  }
  public function getName($id_company)
  {
    $qry = "SELECT name_comercial 
            FROM COMPANIES 
            WHERE id_company = $id_company";
    $res = $this->db->query($qry);
    return $res->getResult()[0]->name_comercial;
  }
  public function getDomain($id_company)
  {
    $qry = "
       SELECT * 
        FROM DOMAINS 
       WHERE id_company = $id_company";
$res = $this->db->query($qry);
return $res->getResult();

  }
}
