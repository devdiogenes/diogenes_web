<?php
 namespace App\Models;

use CodeIgniter\Model;

class Wcenters extends Model
{
  protected $table      = 'WORKING_CENTERS';
  protected $primaryKey = 'id_working_center';
 
  protected $returnType = 'object';
  protected $tempReturnType = 'object';
  protected $useSoftDeletes = false;

  protected $allowedFields = ['id_working_center','name','parent','id_company','facturable'];

  protected $db;

  public $wcData ;   // data extracted from DDBB
  protected $sons;   // array of objects sons of this node
  
  public function __construct($id_working_center)
  {
    $this->db      = \Config\Database::connect();
    if ($id_working_center == 0)                    // nothing to do if the node does not exists
        return;
    $this->wcData = $this->find($id_working_center);
    $this->setSons();                 // retrieve the centers tree of the current one
  }

  /**
   * STATIC getMain($id_company) returns a wcenters model with the root element of the company passed as argument
   */
  static function getMain($id_customer)
  {
      $qry = "
      SELECT id_working_center FROM WORKING_CENTERS
        WHERE  parent = -1
         AND   id_customer = $id_customer
      ";
      $db      = \Config\Database::connect();
      $res = $db->query($qry)->getResult();
      
      if ( ! $res)  // there is no root node
        {
          $qry2 = "
          INSERT INTO WORKING_CENTERS 
            (name, parent,id_customer)
          VALUES
            ('Oficina principal', -1, $id_customer)
          ";                                      // insert a new root node
          $db->query($qry2);
          $res = $db->query($qry)->getResult();  //retrieve the node again
        }
      $newWc = $res[0]->id_working_center;        //find the id

      return New Wcenters($newWc);                // return an object
  }
  /**
   *  Set up the nodes sons of the actual node
   */
  private function setSons()
  { 
    $nodes = array();
    $qry = "
    SELECT id_working_center FROM WORKING_CENTERS
      WHERE  parent = {$this->wcData->id_working_center}
    ";
    $res = $this->db->query($qry)->getResult();          // get all nodes pointing this one
    $conta = 0;
    foreach ($res as $r)
    {
        $nodes[$conta] = New Wcenters($r->id_working_center);   // New model
        $nodes[$conta]->setSons();                              // recursive call
        $conta ++;                                              // pointer increase
    }                                    // trivial case for recursive call is an empty result set
    $this->sons = $nodes;
  }
  /**
  * getChild() returns an array of objects with the sons of the current node
  */
  public function getChild()
  {
    return $this->sons;
  }
  /**
   * getChildArray()
   * Returns an array with the data of the whole child nodes hanging from this one
   */
  public function getChildArray()
  {
      $child = array(); // empty array to be filled and returned 
      $conta = 0;         // counter initialized at 0
      foreach($this->sons as $son)  // iterative part
      {
        $child[$conta]['data'] = $son->wcData;     // data
        $child[$conta]['sons'] = $son->getChildArray(); // iterative call
        $conta ++;
      }
    return $child;
  }
  /**
   * erase: deletes the current node if has no childs.
   * If succes, returns true. Otherwise, returns false
   */
 public function erase()
 {if( count($this->sons) == 0)    // no childs-> trivial case
  {
    $qry = "
            DELETE FROM WORKING_CENTERS WHERE id_working_center = {$this->wcData->id_working_center}
    ";
    $this->db->query($qry); // delete from DDBB
    return true;
  }
    return false;
 }
 /**
   * eraseCascade: deletes the current node and all its childs, leaving only the root node.
   */
  public function eraseCascade()
  {
    
   foreach($this->sons as &$son)   // iterative part
   {
     $son->eraseCascade();        // recursive call
     unset($son);                 
   }
   if ($this->wcData->parent == -1) // root node. No erase.
      return;
       // no childs-> trivial case
    $qry = "
             DELETE FROM WORKING_CENTERS WHERE id_working_center = {$this->wcData->id_working_center}
     ";
     $this->db->query($qry);      // delete from DDBB

  }
  /**
   * insertNode( name ) inserts a new node hanging from the current one named 'name'.
   *  
   */
  public function insertNode($name, $facturable = true)
  {
    $qry = "
          INSERT INTO WORKING_CENTERS 
            (name, parent,id_customer, facturable)
          VALUES
            ('$name', {$this->wcData->id_working_center}, {$this->wcData->id_customer}, $facturable)
          ";                  // insert a new node hanging from current named $name
    $this->db->query($qry);
    $this->setSons();         // Refresh the sons array
  }

}