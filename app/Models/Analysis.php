<?php
 namespace App\Models;

use CodeIgniter\Model;

class Analysis extends Model
{
  protected $table      = 'ANALYSIS';
  protected $primaryKey = 'id_analysis';
 
  protected $returnType = 'object';
  protected $tempReturnType = 'object';
  protected $useSoftDeletes = false;

  protected $allowedFields = ['id_company','id_domain','date_requested','date_started',
                              'date_ended','total_size','total_files'];

  protected $useTimestamps = false;
  protected $createdField  = '';
  protected $updatedField  = '';
  protected $deletedField  = '';
  
  protected $validationRules    = [];
  protected $validationMessages = [];
  protected $skipValidation     = true;

  protected $db;
  
  public $exists;
  public $finished;
  public $id_analysis;
  public $id_domain;
  public $domain_name;
  public $row;
public function __construct($id_analysis=0)
   {
    $this->id_analysis = $id_analysis;
    $this->db      = \Config\Database::connect();
    $qry = "SELECT * FROM ANALYSIS WHERE id_analysis = $id_analysis";
    $row = $this->db->query($qry)->getRow();
    $this->exists =( ! $row ? false : true );
    $this->row = $row;    
    if($this->exists)
      {
      $this->id_domain = $row->id_domain;
      $domain = New \App\Models\Domain();
      $this->domain_name = $domain->get_name($this->id_domain);
      }
    $qry = "SELECT date_ended FROM ANALYSIS WHERE id_analysis = $id_analysis";
    $results = $this->db->query($qry)->getRow();
    if($results==NULL){
       $this->finished = false;
    }else{
      $this->finished = True;
    }
   }
public function analysis_add($data)
   {

    $builder = $this->db->table($this->table);
    $to_insert = [
      'id_domain' => $data['id_domain'],
    ];
    $builder->insert($to_insert);
    
    $data['id_analysis'] = $this->db->insertID();
    return $data;

   }
public function get_domain()
   {
    $domain = New \App\Models\Domain();
    $this->domain = $domain->get_name($this->id_domain);
   }
  /**
   *          get_status()
   * return the current status of the analysis:
   *            'queued'
   *            'ongoing'
   *            'done'
   */
public function get_status()
 {
  if(isset($this->status))
    return $this->status;
  $analysis = $this->find($this->id_analysis); //  
  if ($analysis->date_requested != "")
    $this->status = 'queued';
  if ($analysis->date_started != "")
    $this->status = 'ongoing';
  if ($analysis->date_ended != "")
    $this->status = 'done';
  if($analysis->date_ended == "2001-01-01 01:01:01" or $analysis->domain_error ==1)
   $this->status = "error_domain";
  return $this->status;
  
 }
public function get_filtraciones()
  {
    if(isset($this->num_filtraciones))
    return $this->num_filtraciones;
    $qry = "SELECT count(id_metavalue) as f
              FROM METAVALUES
            WHERE
                  id_analysis = $this->id_analysis
              AND weigth = 150";
    $this->num_filtraciones = $this->db->query($qry)->getRow()->f;
    
    return $this->num_filtraciones;
    
  }
public function get_sensible()
 {
    if(isset($this->num_sensible))
    return $this->num_sensible;
    $qry = "SELECT count(id_metavalue) as f
              FROM METAVALUES
            WHERE
                  id_analysis = $this->id_analysis
              AND weigth = 100";
    $this->num_sensible = $this->db->query($qry)->getRow()->f;
    
    return $this->num_sensible;
    
 }
public function get_num_docs()
 {
  if(isset($this->num_docs))
    return $this->num_docs;
  $qry = "SELECT DISTINCT id_document FROM METAVALUES WHERE id_analysis = $this->id_analysis";
  $this->num_docs = count($this->db->query($qry)->getResult());
  return $this->num_docs;
 }
public function getExtendedInfo()
 {
  if (!$this->exists)
    return;  
  $data = New \stdClass();
  $domain = New \App\Models\Domain();

  $data->name_comercial = $domain->getCompanyName($this->id_domain);
  $data->domain = $domain->get_name($this->id_domain);
  $data->date_requested = $this->row->date_requested;
  $data->status = $this->get_status();
  $data->id_analysis = $this->id_analysis;
  $data->id_domain = $this->id_domain;
  $data->analysis = New \stdClass();
  $data->analysis->id_analysis = $this->id_analysis;
  $data->analysis->date_requested= $this->row->date_requested;
  $data->analysis->date_started = $this->row->date_started;
  $data->analysis->date_finished = $this->row->date_ended;
  return $data;
 }
static function getId($token)
 {
  $db = \Config\Database::connect();
  $qry = "
    SELECT id_analysis
     FROM ANALYSIS
    WHERE token = '$token'
  ";
  $ret = $db->query($qry)->getRow();
  if(\is_null($ret))
    return false;
  else
    return $ret->id_analysis;
 }
static function getToken($id_analysis)
 {
  $db = \Config\Database::connect();
  $qry = "
    SELECT token
     FROM ANALYSIS
    WHERE id_analysis = $id_analysis
  ";
  $ret = $db->query($qry)->getRow();
  if(\is_null($ret))
    return false;
  else
    return $ret->token;
 }
}