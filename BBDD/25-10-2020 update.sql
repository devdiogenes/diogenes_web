CREATE TABLE `WORKING_CENTERS` 
( `id_working_center` INT NOT NULL AUTO_INCREMENT ,
 `name` VARCHAR(64) NULL ,
 `parent` INT NOT NULL ,
 `id_company` INT NOT NULL ,
 `facturable` BOOLEAN NOT NULL DEFAULT TRUE,
  PRIMARY KEY (`id_working_center`))
ENGINE = InnoDB;
ALTER TABLE `WORKING_CENTERS` 
  ADD CONSTRAINT `id_company` 
   FOREIGN KEY (`id_company`) REFERENCES `COMPANIES`(`id_company`) 
      ON DELETE NO ACTION 
      ON UPDATE NO ACTION