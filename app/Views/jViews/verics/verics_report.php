<!-- DEBUG-VIEW START 2 APPPATH/Config/../Views/common/header.php -->
<!DOCTYPE html>
<html>
    <!-- J- HEADER -->    
    <script type="text/javascript"  id="debugbar_loader" data-time="1588350280" src="http://ci4.com/?debugbar"></script><script type="text/javascript"  id="debugbar_dynamic_script"></script><style type="text/css"  id="debugbar_dynamic_style"></style>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Suments Data</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> 
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/fontawesome-free/css/all.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/dist/css/adminlte.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/daterangepicker/daterangepicker.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/summernote/summernote-bs4.css">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/jqvmap/jqvmap.min.css">
    </head>


    <div class="content-header"></div>
    <section class="content">
        <!-- SECTION SUMMARY-->
        <div class="card">
            <div class="card-header" style="background-color:#3a8bba">
                <h3 class="card-title">Summary</h3>
            </div>

            <div class="row">
                <div class="col-12 col-md-6">
                    <table class="table table-hover text-nowrap">
                        <tbody>
                            <tr>
                                <th>Nº de filtraciones</th>
                                <td><p class="<?php echo ($s_summary["total_leakages"] > 0 ? "font-weight-bold text-danger" : "text-success" ); ?>">
                                        <?php echo $s_summary["total_leakages"]; ?>
                                    </p></td>
                            </tr>
                            <tr>
                                <th>Compañía</th>
                                <td><p><?php echo $s_summary["company_name"]; ?></p></td>
                            </tr>
                            <tr>
                                <th>Dominio original</th>
                                <td><a href="<?php echo $s_summary["company_domain"]; ?>" target="_blank"><p><?php echo $s_summary["company_domain"]; ?></p></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-12 col-md-6">
                    <table class="table table-hover text-nowrap">
                        <tbody>
                            <tr>
                                <th>Total subdomains</th>
                                <td><p><?php echo $s_summary["total_subdomains"]; ?></p></td>
                            </tr>
                            <tr>
                                <th>Documents found</th>
                                <td><p><?php echo $s_summary["total_documents"]; ?></p></td>
                            </tr>
                            <tr>
                                <th>processing time</th>
                                <td><p><?php echo $s_summary["total_time"]; ?></p></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--</div>-->

        <!-- SECTION LEAKAGES -->
        <div class="card">
            <div class="card-header" style="background-color:#3a8bba">
                <h3 class="card-title">Potenciales filtraciones</h3>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-12">
                        <!-- Information section -->
                        <div class="card">
                            <div class="card-header" style="background-color:#d7e8f1">
                                <h3 class="card-title">
                                    <a ><i class="fas fa-info-circle"></i> ¿Que es esto?</a>
                                </h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                                    </button>
                                </div>
                                <!-- /.card-tools -->
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <p>Estas son las filtraciones que hemos encontrado en tus documentos.
                                    Las tienes organizadas por tipo de documento y por frecuencia de aparición.
                                    <a href="http://suments.com/index.php/2020/04/26/lectura-informes/" target=”_blank”> Leer mas...</a></p>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <div class="card-body">
                        <!-- Graph section -->
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="card-body">
                                    <div class="chart">
                                        <canvas id="s_leakages_g121" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="card-body">
                                    <canvas id="s_leakages_g122" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="s_leaakages_table" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Dato</th>
                                    <th>Nº de apariciones</th>
                                    <th>Severidad</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <?php foreach ($s_leakages["table"] as $myRow): ?>
                                    <tr>
                                        <td><?php echo $myRow["metadata"]; ?></td>
                                        <td><?php echo $myRow["metadata_count"]; ?></td>
                                        <td><?php echo $myRow["severity"]; ?></td>
                                        <td><a href="#raw_data"><small class="badge badge-info" style="background-color:#3a8bba"><i class="far fa-eye"></i> Mas detalles</small></a></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Dato</th>
                                    <th>Nº de apariciones</th>
                                    <th>Severidad</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>

                <!-- SECTION DOCUMENTS -->
                <div class="card">
                    <div class="card-header" style="background-color:#3a8bba">
                        <h3 class="card-title" >Documentos</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="col-12 col-md-12">
                            <!-- Information section -->
                            <div class="card">
                                <div class="card-header" style="background-color:#d7e8f1">
                                    <h3 class="card-title">
                                        <a ><i class="fas fa-info-circle"></i> ¿Que es esto?</a>
                                    </h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                    <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    Así es como se distribuyen las filtraciones de tus datos según los tipos de documentos.

                                    <a href="http://suments.com/index.php/2020/04/26/lectura-informes/" target=”_blank”> Leer mas...</a>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="card-body">
                                    <canvas id="s_documents_g121" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="card-body">
                                    <canvas id="s_documents_g122" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                </div>
                            </div>

                        </div>

                        <div class="card-body">
                            <table id="table_documents" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Filtraciones</th>
                                        <th>Datos</th>
                                        <th>Documento</th>
                                        <th>Tipo de archivo</th>
                                        <th>Dominio</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php foreach ($s_documents["table"] as $myRow): ?>
                                        <tr>
                                            <td><?php echo $myRow["metadata_total"]; ?></td>
                                            <td><?php echo $myRow["metadata"]; ?></td>
                                            <td><a href="<?php echo $myRow["url"]; ?>" target=”_blank”><?php echo $myRow["document"]; ?></a></td>
                                            <td><?php echo $myRow["filetype"]; ?></td>
                                            <td><a href="<?php echo $myRow["domain"]; ?>" target=”_blank”><?php echo $myRow["domain"]; ?></a></td>
                                            <td><a href="#raw_data"><small class="badge badge-info" style="background-color:#3a8bba"><i class="far fa-eye"></i> Mas detalles</small></a></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Filtración</th>
                                        <th>Documento</th>
                                        <th>Tipo de archivo</th>
                                        <th>Dominio</th>
                                        <th>URL</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- SECTION DOMAINS -->
                <div class="card">
                    <div class="card-header" style="background-color:#3a8bba">
                        <h3 class="card-title">Dominios y subdominios</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="col-12 col-md-12">
                            <!-- Information section -->
                            <div class="card">
                                <div class="card-header" style="background-color:#d7e8f1">
                                    <h3 class="card-title">
                                        <a ><i class="fas fa-info-circle"></i> ¿Que es esto?</a>
                                    </h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                    <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    Aquí tienes como es la distribución de filtraciones agrupadas por dominios.

                                    <a href="http://suments.com/index.php/2020/04/26/lectura-informes/" target=”_blank”> Leer mas...</a>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="card-body">
                                    <div class="chart">
                                        <canvas id="s_domains_g121" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="card-body">
                                    <div class="chart">
                                        <div class="mh-100 h-100 p-3" id="s_domains_g122" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="card-body">
                            <table id="table_domains" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Pais</th>
                                        <th>Dominio</th>
                                        <th>Filtraciones</th>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($s_domains["table"] as $myRow): ?>
                                        <tr>
                                            <td></td>
                                            <td><?php echo $myRow["country"]; ?></td>
                                            <td><a href="<?php echo $myRow["domain"]; ?>" target=”_blank”><?php echo $myRow["domain"]; ?></a></td>
                                            <td><?php echo $myRow["total_leakages"]; ?></td>
                                            <td><a href="#raw_data"><small class="badge badge-info" style="background-color:#3a8bba"><i class="far fa-eye"></i> Mas detalles</small></a></td>

                                        </tr>
                                    <?php endforeach; ?>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>Pais</th>
                                        <th>Dominio</th>
                                        <th>Filtraciones</th>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- card 4 -->
                <div class="card">
                    <div class="card-header" style="background-color:#3a8bba">
                        <a name="raw_data"></a>
                        <h3 class="card-title">Información para descargar</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        </div>
                    </div>


                    <div class="card-body">
                        <div class="col-12 col-md-12">
                            <!-- Information section -->
                            <div class="card">
                                <div class="card-header" style="background-color:#d7e8f1">
                                    <h3 class="card-title" >
                                        <a ><i class="fas fa-info-circle"></i> ¿Que es esto?</a>
                                    </h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                    <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    Aquí tienes toda la infomación sin agrupar. ¿Hay alguna sección que te gustaría que añadiesemos? ¡Descargatela y cuentanos tu opinión!
                                    <a href="http://suments.com/index.php/2020/04/26/lectura-informes/" target=”_blank”> Leer mas...</a>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <table id="table_raw" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Severidad</th>
                                    <th>Dato</th>
                                    <th>Documento</th>
                                    <th>Tipo</th>
                                    <th>Pais</th>
                                    <th>Dominio</th>
                                    <th>Link</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($s_raw_data["table"] as $myRow): ?>
                                    <tr class="<?php echo ($myRow["id_metaseverity"] == 3 ? "table-warning" : "table-danger" ); ?>">
                                        <td><?php echo $myRow["severity"]; ?></td>
                                        <td><?php echo $myRow["data"]; ?></td>
                                        <td><?php echo $myRow["document"]; ?></td>
                                        <td><?php echo $myRow["filetype"]; ?></td>
                                        <td><?php echo $myRow["country"]; ?></td>
                                        <td><?php echo $myRow["domain"]; ?></td>
                                        <td><a href="<?php echo $myRow["url"]; ?>" target=”_blank”><?php echo $myRow["url"]; ?></a></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Severidad</th>
                                    <th>Dato</th>
                                    <th>Documento</th>
                                    <th>Tipo</th>
                                    <th>Pais</th>
                                    <th>Dominio</th>
                                    <th>Link</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                </section>
            </div>

            <script src="<?php echo base_url(); ?>/plugins/jquery/jquery.min.js"></script>
            <script src="<?php echo base_url(); ?>/plugins/jquery-ui/jquery-ui.min.js"></script>

            <script src="<?php echo base_url(); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
            <script src="<?php echo base_url(); ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
            <script src="<?php echo base_url(); ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
            <script src="<?php echo base_url(); ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>


            <script src="<?php echo base_url(); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
            <script src="<?php echo base_url(); ?>/plugins/chart.js/Chart.min.js"></script>
            <script src="<?php echo base_url(); ?>/plugins/sparklines/sparkline.js"></script>
            <script src="<?php echo base_url(); ?>/plugins/jquery-knob/jquery.knob.min.js"></script>
            <script src="<?php echo base_url(); ?>/plugins/moment/moment.min.js"></script>
            <script src="<?php echo base_url(); ?>/plugins/daterangepicker/daterangepicker.js"></script>
            <script src="<?php echo base_url(); ?>/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
            <script src="<?php echo base_url(); ?>/plugins/summernote/summernote-bs4.min.js"></script>
            <script src="<?php echo base_url(); ?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
            <script src="<?php echo base_url(); ?>/dist/js/adminlte.js"></script>

            <script src="<?php echo base_url(); ?>/dist/js/demo.js"></script>

            <script>
                $.widget.bridge('uibutton', $.ui.button)
                        $(function () {

                        var donutChartCanvas = $('#s_leakages_g122').get(0).getContext('2d')
                                var donutData = {
                                labels: <?php echo $s_leakages["g122"]["labels"]; ?>,
                                        datasets: [
                                        {
                                        data: <?php echo $s_leakages["g122"]["data"]; ?>,
                                                backgroundColor: ['#3a8bba', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
                                        }
                                        ]
                                }
                        var donutOptions = {
                        maintainAspectRatio: false,
                                responsive: true,
                                legend: {
                                display: true,
                                        position: 'right',
                                        align: 'left'
                                },
                                title: {
                                display: false,
                                        text: "<?php echo $s_leakages["g122"]["title"]; ?>"
                                }
                        }
                        var donutChart = new Chart(donutChartCanvas, {
                        type: 'polarArea',
                                data: donutData,
                                options: donutOptions,
                        })

                                var barChartCanvas = $('#s_documents_g121').get(0).getContext('2d')
                                var barChartData = {
                                labels: <?php echo $s_leakages["g121"]["labels"]; ?>,
                                        datasets: [
                                        {
                                        backgroundColor: 'rgba(60,141,188,0.9)',
                                                borderColor: 'rgba(60,141,188,0.8)',
                                                pointRadius: false,
                                                pointColor: '#3b8bba',
                                                pointStrokeColor: 'rgba(60,141,188,1)',
                                                pointHighlightFill: '#fff',
                                                pointHighlightStroke: 'rgba(60,141,188,1)',
                                                data: <?php echo $s_leakages["g121"]["data"]; ?>
                                        },
                                        ]
                                }
                        var barChartOptions = {
                        responsive: true,
                                maintainAspectRatio: false,
                                datasetFill: false,
                                legend: {
                                display: false
                                },
                                title: {
                                display: false,
                                        text: "<?php echo $s_leakages["g121"]["title"]; ?>"
                                },
                                scales: {
                                xAxes: [{
                                gridLines: {
                                display: false,
                                }
                                }],
                                        yAxes: [{
                                        gridLines: {
                                        display: true,
                                        },
                                                ticks: {
                                                min: 0
                                                }
                                        }]
                                }

                        }
                        var barChart = new Chart(barChartCanvas, {
                        type: 'bar',
                                data: barChartData,
                                options: barChartOptions
                        })

                                var stackedBarChartCanvas = $('#s_domains_g121').get(0).getContext('2d')

                                var my = {
                                labels: <?php echo $s_domains["g121"]["labels"]; ?>,
                                        datasets: [

<?php foreach ($s_domains["g121"]["datasets"] as $dataset): ?>
                                            ,{
                                            label: '<?php echo $dataset["label"]; ?>',
                                                    data: <?php echo $dataset["data"]; ?>,
                                                    backgroundColor: '<?php echo $dataset["backgroundColor"]; ?>',
                                            },
<?php endforeach; ?>
                                        ]
                                }

                        var stackedBarChartData = jQuery.extend(true, {}, my)

                                var stackedBarChartOptions = {
                                responsive: true,
                                        maintainAspectRatio: false,
                                        scales: {
                                        xAxes: [{
                                        stacked: true,
                                        }],
                                                yAxes: [{
                                                stacked: true,
                                                        ticks: {
                                                        min: 0,
                                                                max: 100
                                                        }
                                                }]
                                        }
                                }

                        var stackedBarChart = new Chart(stackedBarChartCanvas, {
                        type: 'bar',
                                data: stackedBarChartData,
                                options: stackedBarChartOptions
                        })

                                var ctx = document.getElementById("s_documents_g122").getContext('2d');
            // End Defining data
                        var scatterOptions = {
                        tooltips: {
                        enabled: false
                        },
                                responsive: true, // Instruct chart js to respond nicely.
                                maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height  
                                responsive: true,
                                legend: {
                                display: false
                                },
                                scales: {
                                xAxes: [{
                                gridLines: {
                                display: false,
                                        ticks: {
                                        min: 0,
                                                max: 10,
                                                stepSize: 0.01
                                        }
                                }

                                }],
                                        yAxes: [{
                                        gridLines: {
                                        display: true,
                                                ticks: {
                                                stepSize: 0.01,
                                                        suggestedMin: 0.5,
                                                        suggestedMax: 5
                                                }
                                        }


                                        }]
                                }
                        }
            // End Defining data
                        function generateData(n, xCenter, yCenter) {
                        var data = [];
                        var spread = 0.4
                                for (var i = 0; i < n; i++) {
                        data.push({
                        x: (Math.random() - 0.5) * spread + xCenter,
                                y: (Math.random() - 0.5) * spread + yCenter
                        })
                        }
                        return data
                        }
                        var scatterChartData = {
                        datasets: [
                        {
                        label: 'My First dataset',
                                backgroundColor: "#3b8bba66",
                                data: generateData(100, 1, 2)
                        },
                        {
                        label: 'My Second dataset',
                                backgroundColor: "#ffeebd99",
                                data: generateData(50, 1, 3)
                        },
                        {
                        label: 'My Second dataset',
                                backgroundColor: "#f7c6cbBB",
                                data: generateData(5, 1, 4)
                        },
                        {
                        label: 'My First dataset',
                                backgroundColor: "#3b8bba66",
                                data: generateData(800, 2, 2)
                        },
                        {
                        label: 'My Second dataset',
                                backgroundColor: "#ffeebd99",
                                data: generateData(20, 2, 3)
                        },
                        {
                        label: 'My Second dataset',
                                backgroundColor: "#f7c6cbBB",
                                data: generateData(50, 2, 4)
                        },
                        {
                        label: 'My First dataset',
                                backgroundColor: "#3b8bba66",
                                data: generateData(250, 3, 2)
                        },
                        {
                        label: 'My Second dataset',
                                backgroundColor: "#ffeebd99",
                                data: generateData(100, 3, 3)
                        },
                        {
                        label: 'My Second dataset',
                                backgroundColor: "#f7c6cbBB",
                                data: generateData(45, 3, 4)
                        },
                        {
                        label: 'My First dataset',
                                backgroundColor: "#3b8bba33",
                                data: generateData(1500, 4, 2)
                        },
                        {
                        label: 'My Second dataset',
                                backgroundColor: "#ffeebd99",
                                data: generateData(500, 4, 3)
                        },
                        {
                        label: 'My Second dataset',
                                backgroundColor: "#f7c6cbBB",
                                data: generateData(50, 4, 4)
                        },
                        {
                        label: 'My First dataset',
                                backgroundColor: "#3b8bba33",
                                data: generateData(250, 5, 2)
                        },
                        {
                        label: 'My Second dataset',
                                backgroundColor: "#ffeebd99",
                                data: generateData(50, 5, 3)
                        },
                        {
                        label: 'My Second dataset',
                                backgroundColor: "#f7c6cbBB",
                                data: generateData(5, 5, 4)
                        },
                        ]
                        }

                        var myChart = new Chart(ctx, {
                        showTooltips: false,
                                type: 'scatter',
                                data: scatterChartData,
                                options: scatterOptions
                        });
                        var ctx = document.getElementById('s_leakages_g121');
                        var myChart = new Chart(ctx, {
                        type: 'bar',
                                data: {
                                labels: <?php echo $s_leakages["g121"]["labels"]; ?>,
                                        datasets: [
                                        {
                                        data: <?php echo $s_leakages["g121"]["data"]; ?>,
                                                backgroundColor: '#3b8bba',
                                        },
                                        ]
                                },
                                options: {
                                legend: {
                                display: false,
                                },
                                        scales: {
                                        xAxes: [{ stacked: false }],
                                                yAxes: [{ stacked: false }]
                                        }
                                }
                        });
            ////////////////////////////////////////////////////////
            /////   TABLES

                        $('#s_leakages_table').DataTable({
                        dom: 'Bfrtip',
                                buttons: [
                                        'csv', 'excel'
                                ],
                                "paging": true,
                                "lengthChange": false,
                                "searching": true,
                                "ordering": true,
                                "info": true,
                                "autoWidth": false,
                                "responsive": true,
                        });
                        $('#s_domains_table').DataTable({
                        dom: 'Bfrtip',
                                buttons: [
                                        'csv', 'excel'
                                ],
                                "paging": true,
                                "lengthChange": false,
                                "searching": true,
                                "ordering": true,
                                "info": true,
                                "autoWidth": false,
                                "responsive": true,
                        });
                        $('#s_documents_table').DataTable({
                        dom: 'Bfrtip',
                                buttons: [
                                        'csv', 'excel'
                                ],
                                "paging": true,
                                "lengthChange": false,
                                "searching": true,
                                "ordering": true,
                                "info": true,
                                "autoWidth": false,
                                "responsive": true,
                        });
                        $('#s_raw_table').DataTable({
                        dom: 'Bfrtip',
                                buttons: [
                                        'csv', 'excel'
                                ],
                                "paging": true,
                                "lengthChange": false,
                                "searching": true,
                                "ordering": true,
                                "info": true,
                                "autoWidth": false,
                                "responsive": true,
                        });
                        }
                        );</script>


            <!--MAP-->

            <script src="<?php echo base_url(); ?>/plugins/jqvmap//jquery.vmap.js"></script>
            <script src="<?php echo base_url(); ?>/plugins/jqvmap/maps/jquery.vmap.world.js"></script>

            <!--END MAP-->
            <script>

                $(function () {
                $('#s_domains_g122').vectorMap({
                map: 'world',
                        scaleColors: ['#C8EEFF', '#0071A4'],
                        color: '#d7e8f1',
                        backgroundColor: 'transparent',
                        hoverColor: '#3b8bba',
                        markerStyle: {
                        initial: {
                        fill: '#F8E23B',
                                stroke: '#383f47'
                        }
                        },
                        markers: [
<?php foreach ($s_domains["g122"] as $dataset): ?>
                            ,{
                            latLng: <?php echo $dataset["coordinates"]; ?>,
                                    name: '<?php echo $dataset["domain"]; ?>'
                            },
<?php endforeach; ?>

                        ],
                });
                });

            </script>
