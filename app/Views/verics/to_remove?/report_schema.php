<div class="content-header"></div>
<section class="content">

    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Schemas availables for [PRODUCT NAME] for [USER]</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <select class="form-control select2bs4" style="width: 100%;">
                                <option selected="selected">Suments - Verics</option>
                                <option>Carlos - Test</option>
                                <option>Yael - Test</option>
                                <option>[USER] - [SCHEMA NAME]</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>