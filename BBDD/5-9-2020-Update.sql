/* Creacion de la tabla de centros de trabajo */

CREATE TABLE `WORKING_CENTERS` 
    ( `id_working_center` INT NOT NULL AUTO_INCREMENT ,
    `id_company` INT NOT NULL ,
    `parent` INT NOT NULL ,
    `name` VARCHAR(256) NOT NULL ,
         PRIMARY KEY (`id_working_center`)
    )
    ENGINE = InnoDB;

/* establecimiento de claves foráneas */

ALTER TABLE `WORKING_CENTERS` 
    ADD CONSTRAINT `id_company` 
    FOREIGN KEY (`id_company`) 
        REFERENCES `COMPANIES`(`id_company`) 
ON DELETE NO ACTION 
ON UPDATE NO ACTION;

/* modificacion de la tabla USERS */

ALTER TABLE `USERS` 
    ADD `id_working_center` INT NOT NULL AFTER `md5_password`;

/* nueva clave foránea para la tabla USERS. Por alguna razón que desconozco, no puedo crear la clave foránea */
ALTER TABLE `USERS` 
    ADD CONSTRAINT `id_working_center` 
    FOREIGN KEY (`id_working_center`) 
        REFERENCES `WORKING_CENTERS`(`id_working_center`)  
    ON DELETE NO ACTION 
    ON UPDATE NO ACTION;