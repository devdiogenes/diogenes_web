<?php
//
namespace App\Controllers;

include(APPPATH . 'Libraries/GroceryCrudEnterprise/autoload.php');

use GroceryCrud\Core\GroceryCrud;

class mantenimientos extends BaseController
{

  public
    $permitido = [2, 4, 8, 16, 256]; // Excluidos Usuario externo

  public
  function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
  {
    parent::initController($request, $response, $logger);
    $this->seguridad();
  }

  public
  function index($tabla = 'usuarios')
  {
    $crud  = $this->_getGroceryCrudEnterprise();
    $parms = $this->getParms($tabla);
    $crud->setTable($parms['tabla']);
    $crud->setSubject($parms['singular'], $parms['plural']);
    foreach ($parms['relacion'] as $relacion) {
      $crud->setRelation($relacion[0], $relacion[1], $relacion[2]);
    }
    foreach ($parms['dependencia'] as $dep) {
      $crud->setDependentRelation($dep[0], $dep[1], $dep[2]);
    }
    foreach ($parms['n2n'] as $n2n) {
      $crud->setRelationNtoN($n2n[0], $n2n[1], $n2n[2], $n2n[3], $n2n[4], $n2n[5]);
    }
    foreach ($parms['displayas'] as $das) {
      $crud->displayAs($das[0], $das[1]);
    }
    foreach ($parms['fieldtype'] as $ftp) {
      $crud->fieldType($ftp[0], $ftp[1]);
    }
    foreach ($parms['uploadfields'] as $upl) {
      $crud->setFieldUpload($upl[0], $upl[1], $upl[2]);
    }
    if (count($parms['columns']) != 0) {
      $crud->columns($parms['columns']);
      $crud->editFields($parms['columns']);
      $crud->addFields($parms['columns']);
    }


    $output = $crud->render();
    $salida = $this->_example_output($output);
    $this->carga_vistas($salida);
  }

  private
  function carga_vistas($salida)
  {
    $userdata = array(
      'username' => $this->user->username,
      'avatar'   => $this->user->avatar
    );
    echo view("common/header");
    echo view("common/body");
    //echo view("common/top");
    echo view("common/menu", $userdata);
    echo view("common/content");
    echo $salida;
    echo view("mantenimientos/scripts");
    echo view("common/content_end");
    echo view("common/footer");
    echo view("common/body_end");
    echo view("common/end");
  }

  private
  function _example_output($output = null)
  {
    if (isset($output->isJSONResponse) && $output->isJSONResponse) {
      header('Content-Type: application/json; charset=utf-8');
      echo $output->output;
      exit;
    }
    //$output->elementos = $this->getParms();
    return view('mantenimientos/Mantenimientosv.php', (array) $output);
  }

  private
  function getParms($tabla = '')
  {
    $parms = array(
      'usuarios'    => array(
        'tabla'        => 'USERS',
        'singular'     => 'Usuario',
        'plural'       => 'Usuarios',
        'columns'      => array('name', 'password', 'email', 'Categorías'),
        'relacion'     => array(),
        'dependencia'  => array(),
        'n2n'          => array(
          array('Categorías', 'REL_USERS_CATEGORIES', 'USERS_CATEGORIES', 'id_user', 'id_category', 'name')
        ),
        'displayas'    => array(
          array('name', 'Nombre')
        ),
        'fieldtype'    => array(),
        'uploadfields' => array(
          array('avatar', '/users/avatars', '/users/avatars')
        )
      ),
      'clientes'    => array(
        'tabla'        => 'CUSTOMERS',
        'singular'     => 'Cliente',
        'plural'       => 'Clientes',
        'columns'      => array(
          'id_user', 'id_priority', 'name_comercial',
          'name_legal', 'contact', 'phone', 'email',
          'web', 'coment'
        ),
        'relacion'     => array(
          array('id_user', 'USERS', 'name'),
          array('id_priority', 'PRIORITIES', 'name'),
          array('id_city', 'GEO_CITIES', 'name')
        ),
        'dependencia'  => array(),
        'n2n'          => array(),
        'displayas'    => array(
          array('id_user', 'Usuario'),
          array('id_priority', 'Prioridad'),
          array('date_added', 'Fecha de alta'),
          array('name_comercial', 'Nombre comercial'),
          array('name_legal', 'Razón social'),
          array('address', 'Dirección'),
          array('id_city', 'Localidad'),
          array('contact', 'Persona de contacto'),
          array('phone', 'Teléfono'),
          array('coment', 'Observaciones')
        ),
        'fieldtype'    => array(
          array('email', 'email')
        ),
        'uploadfields' => array()
      ),
      'cias'        => array(
        'tabla'        => 'COMPANIES',
        'singular'     => 'Compañía',
        'plural'       => 'Compañías',
        'columns'      => array(
          'id_user', 'id_customer', 'name_comercial', 'name_legal', 'contact', 'phone',
          'email', 'coment'
        ),
        'relacion'     => array(
          array('id_user', 'USERS', 'name'),
          array('id_customer', 'CUSTOMERS', 'name_comercial'),
          array('id_city', 'GEO_CITIES', 'name')
        ),
        'dependencia'  => array(),
        'n2n'          => array(),
        'where'        => array( 'id_analysis', 'NULL'),
        'displayas'    => array(
          array('id_user', 'Usuario'),
          array('date_added', 'Fecha de alta'),
          array('name_comercial', 'Nombre comercial'),
          array('name_legal', 'Razón social'),
          array('address', 'Dirección'),
          array('id_city', 'Localidad'),
          array('contact', 'Persona de contacto'),
          array('phone', 'Teléfono'),
          array('coment', 'Observaciones'),
          array('id_customer', 'Cliente')
        ),
        'fieldtype'    => array(
          array('email', 'email'),
          array('web', 'url')
        ),
        'uploadfields' => array()
      ),
      'dominios'    => array(
        'tabla'        => 'DOMAINS',
        'singular'     => 'Dominio',
        'plural'       => 'Dominios',
        'columns'      => array('id_company', 'domain'),
        'relacion'     => array(
          array('id_company', 'COMPANIES', 'name_comercial')
        ),
        'dependencia'  => array(),
        'n2n'          => array(),
        'displayas'    => array(
          array('id_company', 'Compañía'),
          array('domain', 'Dominio'),
          array('active', 'Activo'),
          array('date_chk_last', 'Fecha del último chequeo')
        ),
        'fieldtype'    => array(
          array('original', 'checkbox_boolean'),
          array('active', 'checkbox_boolean'),
          array('domain', 'url')
        ),
        'uploadfields' => array()
      ),
      'prioridades' => array(
        'tabla'        => 'PRIORITIES',
        'singular'     => 'Prioridad',
        'plural'       => 'Prioridades',
        'columns'      => array(),
        'relacion'     => array(),
        'dependencia'  => array(),
        'n2n'          => array(),
        'displayas'    => array(
          array('name', 'Nombre'),
          array('weith', 'Peso')
        ),
        'fieldtype'    => array(),
        'uploadfields' => array()
      ),
      'categorias'  => array(
        'tabla'        => 'USERS_CATEGORIES',
        'singular'     => 'Categoría',
        'plural'       => 'Categorías',
        'columns'      => array(),
        'relacion'     => array(),
        'dependencia'  => array(),
        'n2n'          => array(),
        'displayas'    => array(
          array('name', 'Nombre'),
          array('level', 'Nivel')
        ),
        'fieldtype'    => array(),
        'uploadfields' => array()
      ),
      'metavaluesBanned'    => array(
        'tabla'        => 'METAVALUES_BANNED',
        'singular'     => 'Metavalue',
        'plural'       => 'Metavalues baneados',
        'columns'      => array('name'),
        'relacion'     => array(),
        'dependencia'  => array(),
        'n2n'          => array(),
        'displayas'    => array(
          array('name', 'Metavalue')
        ),
        'fieldtype'    => array(),
        'uploadfields' => array()
      ),
    );
    if ($tabla == '')
      return $parms;
    else
      return $parms[$tabla];
  }

  //--------------------------------------------------------------------

  private
  function _getDbData()
  {
    $db = (new \Config\Database())->default;
    return [
      'adapter' => [
        'driver'   => 'Pdo_Mysql',
        'host'     => $db['hostname'],
        'database' => $db['database'],
        'username' => $db['username'],
        'password' => $db['password'],
        'charset'  => 'utf8'
      ]
    ];
  }

  private
  function _getGroceryCrudEnterprise($bootstrap = true, $jquery = true)
  {
    $db     = $this->_getDbData();
    $config = (new \Config\GroceryCrudEnterprise())->getDefaultConfig();

    $groceryCrud = new GroceryCrud($config, $db);
    return $groceryCrud;
  }

  /* TIPOS DE CAMPO
   'string' //default
   'text'
   'date'
   'enum'
   'enum_searchable'
   'datetime'
   'hidden'
   'timestamp'
   'int'
   'password'
   'numeric'
   'checkbox_boolean'
   'url'
   'email'
   'color'
   'dropdown'
   'dropdown_search' // version 2.3.0 or later
   'relational_native' // version 2.3.0 or later
   'multiselect_searchable' // version 2.3.4 or later
   'multiselect_native' // version 2.3.4 or later
   'float' // version 2.7.1 or later
   'time' // version 2.7.4 or later
   'invisible' // version 2.8.0 or later
  */
  /* REGLAS DE VALIDACION
  *  
   required - Field is required
   requiredWith - Field is required if any other fields are present
   requiredWithout - Field is required if any other fields are NOT present
   equals - Field must match another field (email/password confirmation)
   different - Field must be different than another field
   accepted - Checkbox or Radio must be accepted (yes, on, 1, true)
   numeric - Must be numeric
   integer - Must be integer number
   boolean - Must be boolean
   array - Must be array
   length - String must be certain length
   lengthBetween - String must be between given lengths
   lengthMin - String must be greater than given length
   lengthMax - String must be less than given length
   min - Minimum
   max - Maximum
   listContains - Performs in_array check on given array values (the other way round than in)
   in - Performs in_array check on given array values
   notIn - Negation of in rule (not in array of values)
   ip - Valid IP address
   ipv4 - Valid IP v4 address
   ipv6 - Valid IP v6 address
   email - Valid email address
   emailDNS - Valid email address with active DNS record
   url - Valid URL
   urlActive - Valid URL with active DNS record
   alpha - Alphabetic characters only
   alphaNum - Alphabetic and numeric characters only
   ascii - ASCII characters only
   slug - URL slug characters (a-z, 0-9, -, _)
   regex - Field matches given regex pattern
   date - Field is a valid date
   dateFormat - Field is a valid date in the given format
   dateBefore - Field is a valid date and is before the given date
   dateAfter - Field is a valid date and is after the given date
   contains - Field is a string and contains the given string
   subset - Field is an array or a scalar and all elements are contained in the given array
   containsUnique - Field is an array and contains unique values
   creditCard - Field is a valid credit card number
   instanceOf - Field contains an instance of the given class
   optional - Value does not need to be included in data array. If it is however, it must pass validation.
   arrayHasKeys - Field is an array and contains all specified keys.
  * 
  */
}
