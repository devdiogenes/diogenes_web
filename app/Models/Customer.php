<?php
 namespace App\Models;

use CodeIgniter\Model;

class Customer extends Model
{
  protected $table      = 'CUSTOMERS';
  protected $primaryKey = 'id_customer';
 
  protected $returnType = 'object';
  protected $useSoftDeletes = false;

  protected $allowedFields = ['id_user','id_priority','name_comercial','name_legal',
                              'address','id_city','contact','phone','email','web','coment'];

  protected $useTimestamps = false;
  protected $createdField  = 'date_added';
  protected $updatedField  = '';
  protected $deletedField  = '';

  protected $validationRules    = [];
  protected $validationMessages = [];
  protected $skipValidation     = true;

  public $customer;
}