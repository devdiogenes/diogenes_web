<?php

namespace App\Models;

use CodeIgniter\Model;

class Domain extends Model
{
  protected $table      = 'DOMAINS';
  protected $primaryKey = 'id_domain';

  protected $returnType = 'object';
  protected $useSoftDeletes = false;

  protected $allowedFields = [
    'id_user', 'id_company', 'id_analysis', 'domain',
    'original', 'date_chk_last', 'id_city', 'id_ip'
  ];
  protected $useTimestamps = false;
  protected $createdField  = '';
  protected $updatedField  = '';
  protected $deletedField  = '';

  protected $validationRules    = [];
  protected $validationMessages = [];
  protected $skipValidation     = true;

  public function __construct()
   {
    $this->db      = \Config\Database::connect();
   }

  public function domain_add($data)
   {

    $builder = $this->db->table($this->table);
    $to_insert = [
      'id_company' => $data['id_company'],
      'domain'  => $data['company_domain']
    ];
    $builder->insert($to_insert);

    $data['id_domain'] = $this->db->insertID();
    return $data;
   }

  public function get_name($id_domain)
   {
    $qry = "SELECT domain 
            FROM DOMAINS 
            WHERE id_domain = $id_domain";
    $res = $this->db->query($qry);
    return $res->getResult()[0]->domain;
   }
  public function getCompanyName($id_domain)
   {
    $qry = "SELECT name_comercial 
    FROM DOMAINS
      JOIN COMPANIES ON (DOMAINS.id_company = COMPANIES.id_company)
    WHERE id_domain = $id_domain";
    $res = $this->db->query($qry);
    return $res->getResult()[0]->name_comercial;
   }
} 
