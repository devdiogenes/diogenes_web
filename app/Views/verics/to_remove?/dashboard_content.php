<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard VERICS</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>


<section class="content">
    <div class="container-fluid">
        <div class="card">
            <form action="/verics" method="POST" >
                <h1 class="m-0 text-dark">
                    <select name="customer">
<?php 
foreach ($customerdd as $c)
{?>
                        <option value="<?=$c['id_customer']?>" <?=$c['selected']?>> <?=$c['name_comercial']?> </option>
<?php 

} ?>
                        </select>
                    <button type="submit">Cambiar</button>
            </form>
            </h1>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="info-box">
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="lineChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info-box">
                    <div class="card-body">
                        <div id="world-map-front" style="height: 250px; width: 100%;"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info-box">
                    <div class="card-body">
                        <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                    </div>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Company</th>
                            <th>Domain</th>
                            <th>Date requested</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
<?php
foreach ($table as $row)
{
?> 

                    <tr>
                     <td> <?=$row['name_comercial']?></td>
                     <td> <a href='http://<?=$row['domain']?>' target='blank'><?=$row['domain']?></a></td>
                     <td> <?=$row['date_requested']?></td>
                     <td> <?=$row['status']?> </td>
                     <td> <?=$row['launchable']?></td> 
                    </tr>  
<?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Company</th>
                            <th>Domain</th>
                            <th>Date requested</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
