<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard VERICS</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>


<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <form action="/verics" method="POST">
                                <select class="form-control select2" style="width: 100%;">
                                    <?php foreach ($s_internal["customer_dropdown"] as $option) : ?>)
                                    <option <?php echo ($option["selected"] === True ? "selected='selected'" : ""); ?> " value = <?php echo $option["id_customer"]; ?>>
                                            <?php echo $option["name_comercial"]; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <button type=" submit" class="btn btn-block btn-default">Seleccionar</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <form action="/mantenimientos/index/cias">
                            <button type="submit" class="btn btn-block btn-default">Añadir companía</button>
                        </form>

                    </div>

                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="info-box">
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="g131" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info-box">
                    <div class="card-body">
                        <div id="g132" style="height: 250px; width: 100%;"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info-box">
                    <div class="card-body">
                        <canvas id="g133" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                    </div>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-body">
                <table id="s_summary_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Compañía</th>
                            <th>Dominio</th>
                            <th>Fecha de solicitud</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php foreach ($s_summary["table"] as $row) : ?>
                            <tr>
                                <td><?php echo $row["name_comercial"]; ?></td>
                                <td><a href="<?php echo $row["domain"]; ?>" target="_blank">
                                        <?php echo $row["domain"]; ?></a></td>
                                <td><?php echo $row["date_requested"]; ?></td>
                                <td>
                                    <?php $id_analysis = $row['id_analysis']; ?>
                                    <?php echo ($row["status"] == "ready" ?
                                        "<a href='/verics/report/{$id_analysis}'>
                                        <small class='badge badge-info' style='background-color:#3a8bba'>
                                            <i class='far fa-eye'></i>
                                                Ver report
                                            </small></a>"
                                        :
                                        ""); ?>
                                </td>
                                <td>
                                    <?php echo ($row["status"] == "working" ?
                                        "<small class='badge badge-warning'>
                                            <i class='far fa-clock'></i>
                                                Analizando
                                            </small>"
                                        :
                                        ""); ?>
                                </td>
                                <td>
                                    <?php echo ($row["status"] == "queue" ?
                                        "<small class='badge badge-danger'>
                                            <i class='far fa-clock'></i> 
                                                En cola
                                            </small>"
                                        :
                                        ""); ?>
                                </td>
                                <td>
                                    <?php $id_customer = $row['id_customer']; ?>
                                    <?php $id_domain = $row['id_domain']; ?>
                                    <?php echo ($row["launchable"] == True ?
                                        "<a href='/verics/launch/{$id_customer}/{$id_domain}x'>
                                            <small class='badge badge-info' style='background-color:#3a8bba'>
                                            <i class='fas fa-caret-square-right'></i> 
                                            Analizar</a>"
                                        :
                                        ""); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Compañía</th>
                            <th>Dominio</th>
                            <th>Fecha de solicitud</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>



<script src="<?php echo base_url(); ?>/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/jquery-ui/jquery-ui.min.js"></script>

<script src="<?php echo base_url(); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script src="<?php echo base_url(); ?>/plugins/chart.js/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/jqvmap/maps/jquery.vmap.world.js"></script>

<!--<script src="<?php echo base_url(); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>-->
<!--<script src="<?php echo base_url(); ?>/plugins/sparklines/sparkline.js"></script>-->
<!--<script src="<?php echo base_url(); ?>/plugins/jquery-knob/jquery.knob.min.js"></script>-->
<!--<script src="<?php echo base_url(); ?>/plugins/moment/moment.min.js"></script>-->
<!--<script src="<?php echo base_url(); ?>/plugins/daterangepicker/daterangepicker.js"></script>-->
<!--<script src="<?php echo base_url(); ?>/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>-->
<!--<script src="<?php echo base_url(); ?>/plugins/summernote/summernote-bs4.min.js"></script>-->
<!--<script src="<?php echo base_url(); ?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>-->
<!-- <script src="<?php echo base_url(); ?>/dist/js/adminlte.js"></script>-->
<!--<script src="<?php echo base_url(); ?>/dist/js/pages/dashboard.js"></script>-->
<!--<script src="<?php echo base_url(); ?>/dist/js/demo.js"></script> -->

<script>
    $.widget.bridge('uibutton', $.ui.button)
    $(function() {


        var areaChartCanvas = $('#g131').get(0).getContext('2d')

        var areaChartData = {
            labels: <?php echo $s_summary["g131"]["labels"]; ?>,
            datasets: [{
                backgroundColor: 'rgba(60,141,188,0.6)',
                borderColor: 'rgba(60,141,188,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(60,141,188,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data: <?php echo $s_summary["g131"]["data"]; ?>
            }]
        }

        var areaChartOptions = {
            maintainAspectRatio: false,
            responsive: true,
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false,
                    }
                }],
                yAxes: [{
                    gridLines: {
                        display: true,
                    },
                    ticks: {
                        min: 0
                    }
                }]
            }
        }

        // This will get the first returned node in the jQuery collection.
        var areaChart = new Chart(areaChartCanvas, {
            type: 'line',
            data: areaChartData,
            options: areaChartOptions
        })


        //-------------
        //- DONUT CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var donutChartCanvas = $('#g133').get(0).getContext('2d')
        var donutData = {
            labels: <?php echo $s_summary["g133"]["labels"]; ?>,
            datasets: [{
                data: <?php echo $s_summary["g133"]["data"]; ?>,
                borderColor: ['#f56954', '#F8E23B'],
                pointColor: ['#f56954', '#F8E23B'],
                backgroundColor: ['#f5695499', '#F8E23B99'],
            }]
        }
        var donutOptions = {
            maintainAspectRatio: false,
            responsive: true,
            legend: {
                display: true,
                position: "bottom"
            }
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        var donutChart = new Chart(donutChartCanvas, {
            type: 'polarArea',
            data: donutData,
            options: donutOptions
        })

        $('#s_summary_table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'csv', 'excel'
            ],
            "paging": false,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });


        ////////////////////////////////////////////////////////
    });
</script>


<!--MAP-->

<script src="<?php echo base_url(); ?>/plugins/jqvmap//jquery.vmap.js"></script>
<script src="<?php echo base_url(); ?>/plugins/jqvmap/maps/jquery.vmap.world.js"></script>

<!--END MAP-->
<script>
    $(function() {
        $('#g132').vectorMap({
            map: 'world',
            scaleColors: ['#C8EEFF', '#0071A4'],
            color: '#d7e8f1',
            backgroundColor: 'transparent',
            hoverColor: '#3b8bba',
            markerStyle: {
                initial: {
                    fill: '#F8E23B',
                    stroke: '#383f47'
                }
            },
            markers: [

                {
                    latLng: 1,
                    name: 'caca'
                },
            ],
        });
    });
</script>