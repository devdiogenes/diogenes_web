<?php
//apidoc -f api2_doc.php -o ../../documentation/doc -c ../../documentation/apidoc.json 

namespace App\Controllers;

class api2_doc extends BaseController
{
    /**
     * 
     * @api {POST} /VDTable Table
     * @apiName DTable
     * @apiGroup Verics dashboard
     * @apiVersion  2.0.0
     * 
     * 
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     * @apiparam (Control) {int} [page]  Página de la tabla a visualizar
     * @apiparam (Control) {int} [itemsPerPage]  Número de entradas en la tabla
     * 
     * @apiSuccess (200) {Array} payload Array con los objetos que describen cada empresa
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     token : "69abd4abf577d7cfd6d370f146611fea"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *     dataUser : {...},
     *     dataCustomer :{...},
     *     dataWorkingCenter : {...},
     *     payload :[
     *	              {
     *                 name_comercial: "Monsters S.A."
     *                 domain: "https://www.pixar.com"
     *                 date_requested: "2020-05-19 00:25:54"
     *                 status: "ready"
     *                 id_analysis: 480
     *                 id_customer: 35
     *                 id_domain: 2289
     *                 launchable: true
     *                },
     *                {
     *                 name_comercial: "The Boss Baby"
     *                 domain: "https://www.dreamworks.com"
     *                 date_requested: "2019-09-21 16:37:10"
     *                 status: "ready"
     *                 id_analysis: 87
     *                 id_customer: 35
     *                 id_domain: 1067
     *                 launchable: true
     *                },
     * 
     *              {...}
     *       ]
     * }
     * 
     * @apiError (Error 4xx) {int}  404 notFound
     * @apiError (Error 4xx) {int}  498 notActiveUser 
     * @apiError (Error 4xx) {int}  499 invalidToken
     * 
     * @apiErrorExample 404
     * {
     *     payload : [
     *           {
     *              code : 404
     *              error : NotFound
     *              details : Token no encontrado
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *     payload : [
     *           {
     *              code : 499
     *              error : invalidToken
     *              details : "invalidToken"
     *            }
     *          ]
     * }
     */
    public function VDTable()
    {
    }

    /**
     * 
     * @api {POST} /VDSnake Snake
     * @apiName DSnake
     * @apiGroup Verics dashboard
     * @apiVersion  2.0.0
     * 
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     * 
     * @apiSuccess (200) {Obj} payload Datos y etiquetas para el gráfico Snake
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     token : "69abd4abf577d7cfd6d370f146611fea"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *     dataUser : {...},
     *     dataCustomer :{...},
     *     dataWorkingCenter : {...},
     *     payload : [
     *        {
     *          data : [455,839,10765],
     *          labels : ["2020-08-04","2020-08-05","2020-08-12"]
     *         }
     *        ]
     * }
     * 
     * @apiError (Error 4xx) {int}  404 notFound
     * @apiError (Error 4xx) {int}  498 notActiveUser 
     * @apiError (Error 4xx) {int}  499 invalidToken
     * 
     * @apiErrorExample 404
     * {
     *     payload : [
     *           {
     *              code : 404
     *              error : NotFound
     *              details : Token no encontrado
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *     payload : [
     *           {
     *              code : 499
     *              error : invalidToken
     *              details : "invalidToken"
     *            }
     *          ]
     * }
     */
    public function VDSnake()
    {
    }

    /**
     * 
     * @api {POST} /VDMap Map
     * @apiName DMap
     * @apiGroup Verics dashboard
     * @apiVersion  2.0.0
     * 
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     * 
     * @apiSuccess (200) {Obj} payload Datos y etiquetas para el mapa
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     token : "69abd4abf577d7cfd6d370f146611fea"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *     dataUser : {...},
     *     dataCustomer :{...},
     *     dataWorkingCenter : {...},
     *     payload : [
     *          {
     *            name : "Diemen"
     *            latitude : 52.3091
     *            longitude : 4.94019
     *            domain : "https://www.pixar.com"
     *          },
     *          {
     *             name : Alboraya
     *             latitude : 39.4845,
     *             longitude : -0.38072,
     *             domain : https://www.dreamworks.com
     *          },
     *          {...}
     *        ]
     * }
     * 
     * @apiError (Error 4xx) {int}  404 notFound
     * @apiError (Error 4xx) {int}  498 notActiveUser 
     * @apiError (Error 4xx) {int}  499 invalidToken
     * 
     * @apiErrorExample 404
     * {
     *     payload : [
     *           {
     *              code : 404
     *              error : NotFound
     *              details : Token no encontrado
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *     payload : [
     *           {
     *              code : 499
     *              error : invalidToken
     *              details : "invalidToken"
     *            }
     *          ]
     * }
     */
    public function VDMap()
    {
    }

    /**
     * 
     * @api {POST} /VDCheddar Cheddar
     * @apiName DCheddar
     * @apiGroup Verics dashboard
     * @apiVersion  2.0.0
     * 
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     * 
     * @apiSuccess (200) {Obj} payload Datos y etiquetas para el gráfico Cheddar
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     token : "69abd4abf577d7cfd6d370f146611fea"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *     dataUser : {...},
     *     dataCustomer :{...},
     *     dataWorkingCenter : {...},
     *     payload : [
     *          {
     *               data : ["5518","4167"],
     *               labels : ["Filtracion","Sensible"]
     *          }
     *        ]
     * }
     * 
     * @apiError (Error 4xx) {int}  404 notFound
     * @apiError (Error 4xx) {int}  498 notActiveUser 
     * @apiError (Error 4xx) {int}  499 invalidToken
     * 
     * @apiErrorExample 404
     * {
     *     payload : [
     *           {
     *              code : 404
     *              error : NotFound
     *              details : Token no encontrado
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *     payload : [
     *           {
     *              code : 499
     *              error : invalidToken
     *              details : "invalidToken"
     *            }
     *          ]
     * }
     */
    public function VDCheddar()
    {
    }

    /**
     * 
     * @api {POST} /VRSummary Summary
     * @apiName RSummary
     * @apiGroup Verics Report
     * @apiVersion  2.0.0
     * 
     * 
     * @apiParam  {int} id_analysis Identificador único de un análisis
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     * 
     * @apiSuccess (200) {Obj} payload Resumen de los datos que se han encontrado en el análisis del dominio
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     id_analysis : 9875,
     *     token : "6fc75ac20a807a7e0c0814a5215af250"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *     dataUser : {...},
     *     dataCustomer :{...},
     *     dataWorkingCenter : {...},
     *     payload : [
     *          {
     *               total_leakages : 5995,
     *               total_time : "25 h 40 m",
     *               total_subdomains : 840,
     *               total_documents : 17109,
     *               company_name : "Monsters Inc.",
     *               company_domain : "https://www.pixar.com"
     *            }
     *      ]
     * }
     * 
     * @apiError (Error 4xx) {Obj}  401 Acceso restringido a $id_analysis
     * @apiError (Error 4xx) {Obj}  404 $id_analysis no entontrado
     * @apiError (Error 4xx) {Obj}  498 notActiveUser 
     * @apiError (Error 4xx) {Obj}  499 invalidToken
     * 
     * @apiErrorExample 401
     * {
     *     payload : [
     *           {
     *              code : 401
     *              error : Unauthorized,
     *              details : "Acceso restringido a Verics Dashboard de $id_customer"
     *            }
     *          ]
     * } 
     * 
     * @apiErrorExample 404
     * {
     *     payload : [
     *           {
     *              code : 404
     *              error : NotFound,
     *              details : "$id_analysis no encontrado"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *      payload : [
     *           {
     *              code : 499
     *              error : invalidToken,
     *              details : "Token no valido"
     *            }
     *          ]
     * } 
     * 
     */
    public function VRSummary()
    {
    }

    /**
     * 
     * @api {POST} /VRLeakages_Equalizer Leakages_Equalizer
     * @apiName RLeakages_Equalizer
     * @apiGroup Verics Report
     * @apiVersion  2.0.0
     * 
     * 
     * @apiParam  {int} id_analysis Identificador único de un análisis
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     * 
     * @apiSuccess (200) {Obj} payload Numero de veces que aparecen las 5 filtraciones más frecuentes
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     id_analysis : 9875,
     *     token : "6fc75ac20a807a7e0c0814a5215af250"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *     dataUser : {...},
     *     dataCustomer :{...},
     *     dataWorkingCenter : {...},
     *     payload : [
     *       {
     *          data : [386, 268, 589, 3920],
     *          labels : ["Mike Wazowski", "James P. Sullivan", "Randall Boggs", "Otros"]
     *       }
     * }
     * 
     * @apiError (Error 4xx) {Obj}  401 Acceso restringido a $id_analysis
     * @apiError (Error 4xx) {Obj}  404 $id_analysis no entontrado
     * @apiError (Error 4xx) {Obj}  498 notActiveUser 
     * @apiError (Error 4xx) {Obj}  499 invalidToken
     * 
     * @apiErrorExample 401
     * {
     *     payload : [
     *           {
     *              code : 401
     *              error : Unauthorized,
     *              details : "Acceso restringido a Verics Dashboard de $id_customer"
     *            }
     *          ]
     * } 
     * 
     * @apiErrorExample 404
     * {
     *     payload : [
     *           {
     *              code : 404
     *              error : NotFound,
     *              details : "$id_analysis no encontrado"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *      payload : [
     *           {
     *              code : 499
     *              error : invalidToken,
     *              details : "Token no valido"
     *            }
     *          ]
     * } 
     * 
     */
    public function VRLeakages_Equalizer()
    {
    }

    /**
     * 
     * @api {POST} /VRLeakages_Cheddar Leakages_Cheddar
     * @apiName RLeakages_Cheddar
     * @apiGroup Verics Report
     * @apiVersion  2.0.0
     * 
     * 
     * @apiParam  {int} id_analysis Identificador único de un análisis
     * 
     * @apiSuccess (200) {Obj} payload Distribución de las 5 filtraciones más frecuentes
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     id_analysis : 9875,
     *     token : "6fc75ac20a807a7e0c0814a5215af250"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *    dataUser : {...},
     *    dataCustomer :{...},
     *    dataWorkingCenter : {...},
     *    payload: [
     *              data : [386, 268, 202, 200, 134, 3920],
     *              labels : ["Mike", "Wazowski", "James", "Sullivan", "Robert", "Otros"]
     *      ]
     * }
     * 
     * @apiError (Error 4xx) {Obj}  401 Acceso restringido a $id_analysis
     * @apiError (Error 4xx) {Obj}  404 $id_analysis no entontrado
     * @apiError (Error 4xx) {Obj}  498 notActiveUser 
     * @apiError (Error 4xx) {Obj}  499 invalidToken
     * 
     * @apiErrorExample 401
     * {
     *     payload : [
     *           {
     *              code : 401
     *              error : Unauthorized,
     *              details : "Acceso restringido a Verics Dashboard de $id_customer"
     *            }
     *          ]
     * } 
     * 
     * @apiErrorExample 404
     * {
     *     payload : [
     *           {
     *              code : 404
     *              error : NotFound,
     *              details : "$id_analysis no encontrado"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *      payload : [
     *           {
     *              code : 499
     *              error : invalidToken,
     *              details : "Token no valido"
     *            }
     *          ]
     * } 
     * 
     */
    public function VRLeakages_Cheddar()
    {
    }

    /**
     * 
     * @api {POST} /VRLeakages_Table Leakages_Table
     * @apiName RLeakages_Table
     * @apiGroup Verics Report
     * @apiVersion  2.0.0
     * 
     * 
     * @apiParam  {int} id_analysis Identificador único de un análisis
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     * @apiparam (Control) {int} [page]  Página de la tabla a visualizar
     * @apiparam (Control) {int} [itemsPerPage]  Número de entradas en la tabla 
     * 
     * @apiSuccess (200) {Obj} payload Información sobre la filtración que se ha encontrado
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     id_analysis : 9875,
     *     token : "6fc75ac20a807a7e0c0814a5215af250"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *    dataUser : {...},
     *    dataCustomer :{...},
     *    dataWorkingCenter : {...},
     *    payload: [
     *          {
     *           metadata : "Mike",
     *           url : "https://www.pixar.com/myDocument.JPG",
     *           filetype : "JPG",
     *           severity : "Filtracion",
     *           metadata_count :"386"
     *          },
     *      ]
     * }
     * 
     * @apiError (Error 4xx) {Obj}  401 Acceso restringido a $id_analysis
     * @apiError (Error 4xx) {Obj}  404 $id_analysis no entontrado
     * @apiError (Error 4xx) {Obj}  498 notActiveUser 
     * @apiError (Error 4xx) {Obj}  499 invalidToken
     * 
     * @apiErrorExample 401
     * {
     *     payload : [
     *           {
     *              code : 401
     *              error : Unauthorized,
     *              details : "Acceso restringido a Verics Dashboard de $id_customer"
     *            }
     *          ]
     * } 
     * 
     * @apiErrorExample 404
     * {
     *     payload : [
     *           {
     *              code : 404
     *              error : NotFound,
     *              details : "$id_analysis no encontrado"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *      payload : [
     *           {
     *              code : 499
     *              error : invalidToken,
     *              details : "Token no valido"
     *            }
     *          ]
     * } 
     * 
     */
    public function VRLeakages_Table()
    {
    }

    /**
     * 
     * @api {POST} /VRDocuments_Equalizer Documents_Equalizer
     * @apiName RDocuments_Equalizer
     * @apiGroup Verics Report
     * @apiVersion  2.0.0
     * 
     * 
     * @apiParam  {int} id_analysis Identificador único de un análisis
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     *
     * @apiSuccess (200) {Obj} payload Número de filtraciones y datos sensibles encontrados en cada tipo de archivos
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     id_analysis : 9875,
     *     token : "6fc75ac20a807a7e0c0814a5215af250"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *    dataUser : {...},
     *    dataCustomer :{...},
     *    dataWorkingCenter : {...},
     *    payload : [
     *          { 
     *            data: [243980, 34794, 15537, 2779, 838, 515, 404, 345, 263, 249, 242],
     *            labels":["JPG", "PDF","PNG", "GIF", "DOC", "XLSX", "HTML", "DOCX", "MP4", "XLS", "Otros"]
     *          }
     *      ]
     * }
     * 
     * @apiError (Error 4xx) {Obj}  401 Acceso restringido a $id_analysis
     * @apiError (Error 4xx) {Obj}  404 $id_analysis no entontrado
     * @apiError (Error 4xx) {Obj}  498 notActiveUser 
     * @apiError (Error 4xx) {Obj}  499 invalidToken
     * 
     * @apiErrorExample 401
     * {
     *     payload : [
     *           {
     *              code : 401
     *              error : Unauthorized,
     *              details : "Acceso restringido a Verics Dashboard de $id_customer"
     *            }
     *          ]
     * } 
     * 
     * @apiErrorExample 404
     * {
     *     payload : [
     *           {
     *              code : 404
     *              error : NotFound,
     *              details : "$id_analysis no encontrado"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *      payload : [
     *           {
     *              code : 499
     *              error : invalidToken,
     *              details : "Token no valido"
     *            }
     *          ]
     * } 
     * 
     */
    public function VRDocuments_Equalizer()
    {
    }

    /**
     * 
     * @api {POST} /VRDocuments_Cloud Documents_Cloud
     * @apiName RDocuments_Cloud
     * @apiGroup Verics Report
     * @apiVersion  2.0.0
     * 
     * 
     * @apiParam  {int} id_analysis Identificador único de un análisis
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     * 
     * @apiSuccess (200) {Obj} payload Tipos de archivos con el número de filtraciones y datos sensibles encontrados
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     id_analysis : 9875,
     *     token : "6fc75ac20a807a7e0c0814a5215af250"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *    dataUser : {...},
     *    dataCustomer :{...},
     *    dataWorkingCenter : {...},
     *    payload : [
     *              { 
     *                  DOC : {
     *                      position_style : 1,
     *                      position_sensible : 3,
     *                      position_leakage : 5,
     *                      total_style : 0,
     *                      total_sensible : 0,
     *                      total_leakage : 273,
     *                      position : 1 
     *                  },
     *                  PDF : { 
     *                          position_style :1,
     *                          position_sensible : 3,
     *                          position_leakage : 5,
     *                          total_style : 0,
     *                          total_sensible : 0,
     *                          total_leakage : 129 ,
     *                          position : 2
     *                      },
     *                  ... : {...}
     *             }
     *        ]
     * }
     * 
     * @apiError (Error 4xx) {Obj}  401 Acceso restringido a $id_analysis
     * @apiError (Error 4xx) {Obj}  404 $id_analysis no entontrado
     * @apiError (Error 4xx) {Obj}  498 notActiveUser 
     * @apiError (Error 4xx) {Obj}  499 invalidToken
     * 
     * @apiErrorExample 401
     * {
     *     payload : [
     *           {
     *              code : 401
     *              error : Unauthorized,
     *              details : "Acceso restringido a Verics Dashboard de $id_customer"
     *            }
     *          ]
     * } 
     * 
     * @apiErrorExample 404
     * {
     *     payload : [
     *           {
     *              code : 404
     *              error : NotFound,
     *              details : "$id_analysis no encontrado"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *      payload : [
     *           {
     *              code : 499
     *              error : invalidToken,
     *              details : "Token no valido"
     *            }
     *          ]
     * } 
     * 
     */
    public function VRDocuments_Cloud()
    {
    }

    /**
     * 
     * @api {POST} /VRDocuments_Table Documents_Table
     * @apiName RDocuments_Table
     * @apiGroup Verics Report
     * @apiVersion  2.0.0
     * 
     * 
     * @apiParam  {int} id_analysis Identificador único de un análisis
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     * @apiparam (Control) {int} [page]  Página de la tabla a visualizar
     * @apiparam (Control) {int} [itemsPerPage]  Número de entradas en la tabla
     * 
     * @apiSuccess (200) {Obj} payload Información sobre el archivo afectado y el dominio al que pertenece  
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     id_analysis : 9875,
     *     token : "6fc75ac20a807a7e0c0814a5215af250"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *    dataUser : {...},
     *    dataCustomer :{...},
     *    dataWorkingCenter : {...},
     *    payload : [
     *              {
     *                  metadata":"Mike Wazowski",
     *                  url":"https://www.pixar.com/myDocument.JPG",
     *                  filetype":"JPG",
     *                  domain":"https://www.pixar.com",
     *                  severity":"Leakage",
     *                  metadata_total":"1639",
     *                  document":"myDocument.jpg"
     *                },
     *              {
     *                  metadata":"James P. Sullivan",
     *                  url":"https://www.pixar.com/myOtherDocument.PDF",
     *                  filetype":"PDF",
     *                  domain":"https://www.pixar.com",
     *                  severity":"Leakage",
     *                  metadata_total":"1639",
     *                  document":"myOtherDocument.pdf"
     *              },
     *              {...}
     *           ]
     * }
     * 
     * @apiError (Error 4xx) {Obj}  401 Acceso restringido a $id_analysis
     * @apiError (Error 4xx) {Obj}  404 $id_analysis no entontrado
     * @apiError (Error 4xx) {Obj}  498 notActiveUser 
     * @apiError (Error 4xx) {Obj}  499 invalidToken
     * 
     * @apiErrorExample 401
     * {
     *     payload : [
     *           {
     *              code : 401
     *              error : Unauthorized,
     *              details : "Acceso restringido a Verics Dashboard de $id_customer"
     *            }
     *          ]
     * } 
     * 
     * @apiErrorExample 404
     * {
     *     payload : [
     *           {
     *              code : 404
     *              error : NotFound,
     *              details : "$id_analysis no encontrado"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *      payload : [
     *           {
     *              code : 499
     *              error : invalidToken,
     *              details : "Token no valido"
     *            }
     *          ]
     * } 
     * 
     */
    public function VRDocuments_Table()
    {
    }

    /**
     * 
     * @api {POST} /VRDomains_Equalizer Domains_Equalizer
     * @apiName RDomains_Equalizer
     * @apiGroup Verics Report
     * @apiVersion  2.0.0
     * 
     * 
     * @apiParam  {int} id_analysis Identificador único de un análisis
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     * 
     * @apiSuccess (200) {Obj} payload Número de filtraciones y datos sensibles encontrados en cada dominio y subdominio
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     id_analysis : 9875,
     *     token : "6fc75ac20a807a7e0c0814a5215af250"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {    
     *    dataUser : {...},
     *    dataCustomer :{...},
     *    dataWorkingCenter : {...},
     *    payload: [
     *          {
     *              meta : "https://www.pixar.com",
     *              metaseverity_name :"Filtracion",
     *              value : 43
     *          },
     *          {
     *              meta : "https://www.pixar.com,
     *              metaseverity_name : "Sensible",
     *              value : 157
     *          },
     *          {...}
     *     ]
     * }
     * 
     * @apiError (Error 4xx) {Obj}  401 Acceso restringido a $id_analysis
     * @apiError (Error 4xx) {Obj}  404 $id_analysis no entontrado
     * @apiError (Error 4xx) {Obj}  498 notActiveUser 
     * @apiError (Error 4xx) {Obj}  499 invalidToken
     * 
     * @apiErrorExample 401
     * {
     *     payload : [
     *           {
     *              code : 401
     *              error : Unauthorized,
     *              details : "Acceso restringido a Verics Dashboard de $id_customer"
     *            }
     *          ]
     * } 
     * 
     * @apiErrorExample 404
     * {
     *     payload : [
     *           {
     *              code : 404
     *              error : NotFound,
     *              details : "$id_analysis no encontrado"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *      payload : [
     *           {
     *              code : 499
     *              error : invalidToken,
     *              details : "Token no valido"
     *            }
     *          ]
     * } 
     * 
     */
    public function VRDomains_Equalizer()
    {
    }

    /**
     * 
     * @api {POST} /VRDomains_Map Domains_Map
     * @apiName RDomains_Map
     * @apiGroup Verics Report
     * @apiVersion  2.0.0
     * 
     * 
     * @apiParam  {int} id_analysis Identificador único de un análisis
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     * 
     * @apiSuccess (200) {Obj} payload Coordenadas y nombre de la ciduad donde se han encontrado dominios/subdominios que presentan filtraciones
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     id_analysis : 9875,
     *     token : "6fc75ac20a807a7e0c0814a5215af250"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *    dataUser : {...},
     *    dataCustomer :{...},
     *    dataWorkingCenter : {...},
     *    payload: [
     *          {
     *              name : "Belfast",
     *              latitude : "54.5973",
     *              longitude":"5.9301"
     *          },
     *          {
     *              name : "Albacete",
     *              latitude : "38.9943",
     *              longitude":"1.8585"
     *          },
     *          {
     *              name : "Madrid",
     *              latitude : "40.4168",
     *              longitude":"3.7038"
     *          },
     * ]
     * }
     * 
     * @apiError (Error 4xx) {Obj}  401 Acceso restringido a $id_analysis
     * @apiError (Error 4xx) {Obj}  404 $id_analysis no entontrado
     * @apiError (Error 4xx) {Obj}  498 notActiveUser 
     * @apiError (Error 4xx) {Obj}  499 invalidToken
     * 
     * @apiErrorExample 401
     * {
     *     payload : [
     *           {
     *              code : 401
     *              error : Unauthorized,
     *              details : "Acceso restringido a Verics Dashboard de $id_customer"
     *            }
     *          ]
     * } 
     * 
     * @apiErrorExample 404
     * {
     *     payload : [
     *           {
     *              code : 404
     *              error : NotFound,
     *              details : "$id_analysis no encontrado"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *      payload : [
     *           {
     *              code : 499
     *              error : invalidToken,
     *              details : "Token no valido"
     *            }
     *          ]
     * } 
     * 
     */
    public function VRDomains_Map()
    {
    }

    /**
     * 
     * @api {POST} /VRDomains_Table Domains_Table
     * @apiName RDomains_Table
     * @apiGroup Verics Report
     * @apiVersion  2.0.0
     * 
     * 
     * @apiParam  {int} id_analysis Identificador único de un análisis
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     * @apiparam (Control) {int} [page]  Página de la tabla a visualizar
     * @apiparam (Control) {int} [itemsPerPage]  Número de entradas en la tabla
     * 
     * @apiSuccess (200) {Obj} payload Información sobre las filtraciones y datos sensibles encontrados en cada archivo de un dominio/subdominio
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     id_analysis : 9875,
     *     token : "6fc75ac20a807a7e0c0814a5215af250"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *    dataUser : {...},
     *    dataCustomer :{...},
     *    dataWorkingCenter : {...},
     *    payload: [
     *          {
     *              value : "Mike Wazowski",
     *              domain": "https://www.pixar.com",
     *              metaseverity_name : "Filtracion",
     *              city_name : "Mostropolis"
     *          },
     *          {
     *              value : "James P Sullivan",
     *              domain": "https://www.pixar.com",
     *              metaseverity_name : "Filtracion",
     *              city_name : "Mostropolis"
     *          },
     *          {...}
     *      ]
     * }
     * 
     * @apiError (Error 4xx) {Obj}  401 Acceso restringido a $id_analysis
     * @apiError (Error 4xx) {Obj}  404 $id_analysis no entontrado
     * @apiError (Error 4xx) {Obj}  498 notActiveUser 
     * @apiError (Error 4xx) {Obj}  499 invalidToken
     * 
     * @apiErrorExample 401
     * {
     *     payload : [
     *           {
     *              code : 401
     *              error : Unauthorized,
     *              details : "Acceso restringido a Verics Dashboard de $id_customer"
     *            }
     *          ]
     * } 
     * 
     * @apiErrorExample 404
     * {
     *     payload : [
     *           {
     *              code : 404
     *              error : NotFound,
     *              details : "$id_analysis no encontrado"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *      payload : [
     *           {
     *              code : 499
     *              error : invalidToken,
     *              details : "Token no valido"
     *            }
     *          ]
     * } 
     * 
     */
    public function VRDomains_Table()
    {
    }

    /**
     * 
     * @api {POST} /VRRaw_Table Raw_Table
     * @apiName RRaw_Table
     * @apiGroup Verics Report
     * @apiVersion  2.0.0
     * 
     * 
     * @apiParam  {int} id_analysis Identificador único de un análisis
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     * @apiparam (Control) {int} [page]  Página de la tabla a visualizar
     * @apiparam (Control) {int} [itemsPerPage]  Número de entradas en la tabla
     * 
     * @apiSuccess (200) {Obj} payload Toda la información del análisis en bruto
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     id_analysis : 9875,
     *     token : "6fc75ac20a807a7e0c0814a5215af250"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *    dataUser : {...},
     *    dataCustomer :{...},
     *    dataWorkingCenter : {...},
     *    payload: [
     *      {
     *           id_metavalue : 29793609,
     *           id_document : 565484,
     *           id_metatag : 10979,
     *           value : "James P. Sullivan",
     *           weigth : 150,
     *           id_analysis : 187,
     *           url : "https://www.pixar.com",
     *           domain : "https://SUBDOMAIN.pixar.com",
     *           filetype : "JPG",
     *           metaseverity_name : "Filtracion",
     *           id_domain : 2239,
     *           id_city : 81,
     *           city_name : "Monstropolis",
     *           is_hidden : 0
     *        },
     *        {...}
     *    ]
     * }
     * 
     * @apiError (Error 4xx) {Obj}  401 Acceso restringido a $id_analysis
     * @apiError (Error 4xx) {Obj}  404 $id_analysis no entontrado
     * @apiError (Error 4xx) {Obj}  498 notActiveUser 
     * @apiError (Error 4xx) {Obj}  499 invalidToken
     * 
     * @apiErrorExample 401
     * {
     *     payload : [
     *           {
     *              code : 401
     *              error : Unauthorized,
     *              details : "Acceso restringido a Verics Dashboard de $id_customer"
     *            }
     *          ]
     * } 
     * 
     * @apiErrorExample 404
     * {
     *     payload : [
     *           {
     *              code : 404
     *              error : NotFound,
     *              details : "$id_analysis no encontrado"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *      payload : [
     *           {
     *              code : 499
     *              error : invalidToken,
     *              details : "Token no valido"
     *            }
     *          ]
     * } 
     * 
     */
    public function VRRaw_Table()
    {
    }

    /**
     * 
     * @api {post} /ALogin Login
     * @apiName Login
     * @apiGroup Identificacion
     * @apiVersion  2.0.0
     * 
     * 
     * @apiParam  (Identificacion) {string{32}} token Token de identificación único
     * 
     * @apiSuccess (200) {Obj} dataUser Información relativa al usuario
     * @apiSuccess (200) {Obj} dataCustomer Información relativa al cliente
     * @apiSuccess (200) {Obj} dataWorkingCenter Información relativa al centro de trabajo
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     token : "6fc75ac20a807a7e0c0814a5215af250"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *        dataUser: {
     *            id_user : 31,
     *            name : "James P. Sullivan",
     *            email : "JamesSullivan@MonstersInc.com",
     *            date_created : "2020-11-09 03:50:45"
     *          },
     *         dataCustomer: {
     *            id_customer : 59,
     *            id_user : 31,
     *            id_priority : 3,
     *            date_added : "2020-09-10 11:19:32",
     *            name_comercial : "Monsters",
     *            name_legal : "Monsters Inc.",
     *            address : "Monstropolis address",
     *            id_city : 15,
     *            contact : "James P. Sullivan",
     *            phone : "555-489-789",
     *            email : info@MostersInc.com,
     *            web : "https://www.pixar.com",
     *            coment : "¡Que gran película!"
     *          },
     *         dataWorkingCenter: {
     *            id_working_center : 7,
     *            name : "Headquartes Monsters Inc.",
     *            parent : -1,
     *            id_customer : 59,
     *            facturable : 1
     *          },
     *       payload : []
     * }
     *
     * @apiError (Error 4xx) {Obj}  404 NotFount
     * @apiError (Error 4xx) {Obj}  499 invalidToken
     *
     * @apiErrorExample 404
     * {
     *      payload : [
     *           {
     *              code : 404
     *              error : notFound,
     *              details : "Token not found"
     *            }
     *          ]
     * } 
     * 
     * @apiErrorExample 499
     * {
     *      payload : [
     *           {
     *              code : 499
     *              error : invalidToken,
     *              details : "Token no valido"
     *            }
     *          ]
     * } 
     */
    public function ILogin()
    {
    }

    /**
     * 
     * @api {post} /ALogout Logout
     * @apiName Logout
     * @apiGroup Identificacion
     * @apiVersion  2.0.0
     * 
     * 
     * @apiParam  (Identificacion) {string{32}} token Token único de identificación del usuario
     * 
     * @apiSuccess (200) {Obj} success Logged out
     * 
     * @apiParamExample  {Obj} Request-Example:
     * {
     *     token : "6fc75ac20a807a7e0c0814a5215af250"
     * }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *    payload : [ 
     *          {
     *              code : 200,
     *              details: "Logged out"
     *          }
     *         ]
     * }
     * 
     * 
     */
    public function ILogout()
    {
    }

    /**
     * 
     * @api {post} /AAddCompany AAddCompany
     * @apiName AddCompany
     * @apiGroup Acción
     * @apiVersion  2.0.0
     * 
     * 
     * @apiParam  {String{32}} company_name Nombre para identificar a la compañía
     * @apiParam  {String{64}} company_domain Dominio a analizar
     * @apiParam  {String{32}} [company_email] Email de contacto de la compañía
     * @apiParam  {String{32}} [company_contact] Persona de contacto
     * @apiParam  {String{32}} [company_phone] Telefono de contacto
     * @apiParam  (Identificacion) {string{32}} [token] Token único de identificación del usuario
     * 
     * @apiSuccess (200) {Obj} payload Obj con los detalles de la empresa añadida
     * 
     * @apiParamExample  {Obj} Request-Example:
     *  {
     *     company_name : "Monsters Inc",
     *     company_domain : "https://www.pixar.com",
     *     company_email  : "JamesPSullivan@MostersInc.com",
     *     company_contact : "James P. Sullivan",
     *     phone : "555-468-789",
     *     token : "6fc75ac20a807a7e0c0814a5215af250"
     *  }
     * 
     * 
     * @apiSuccessExample {Obj} Success-Response:
     * {
     *    dataUser : {...},
     *    dataCustomer :{...},
     *    dataWorkingCenter : {...},
     *    payload : [ 
     *        {
     *          name_comercial: "Monsters Inc."
     *          domain: "https://www.pixar.com"
     *          date_requested: "2020-05-19 00:25:54"
     *          status: "ready"
     *          id_analysis: 480
     *          id_customer: 35
     *          id_domain: 2289
     *          launchable: true
     *         }
     *       ]
     * }
     * 
     * @apiError (Error 4xx) {Obj}  400 badRequest
     * @apiError (Error 4xx) {Obj}  401 Acceso restringido a $id_analysis
     * @apiError (Error 4xx) {Obj}  498 notActiveUser 
     * @apiError (Error 4xx) {Obj}  499 invalidToken
     * 
     * @apiErrorExample 401
     * {
     *     payload : [
     *           {
     *              code : 400
     *              error : badRequest
     *              details : "Alguno de los parámetros nos está bien formateado"
     *            }
     *          ]
     * } 
     * 
     * @apiErrorExample 401
     * {
     *     payload : [
     *           {
     *              code : 401
     *              error : Unauthorized,
     *              details : "Acceso restringido a Verics Dashboard de $id_customer"
     *            }
     *          ]
     * } 
     * 
     * 
     * @apiErrorExample 498
     * {
     *     payload : [
     *           {
     *              code : 498
     *              error : notActiveUser
     *              details : "Usuario no activo"
     *            }
     *          ]
     * }
     * 
     * @apiErrorExample 499
     * {
     *      payload : [
     *           {
     *              code : 499
     *              error : invalidToken,
     *              details : "Token no valido"
     *            }
     *          ]
     * } 
     * 
     * 
     */
    public function AAddCompany()
    {
    }
}