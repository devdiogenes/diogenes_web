<?php
 namespace App\Models;

use CodeIgniter\Model;

class Metavalue extends Model
{
  protected $table      = 'METAVALUES';
  protected $primaryKey = 'id_metavalue';
  protected $returnType = 'object';
  protected $tempReturnType = 'object';
  protected $useSoftDeletes = false;
  protected $allowedFields = ['is_hidden_by_user'];
  protected $db;
  
  public function __construct()
   {
    $this->db      = \Config\Database::connect();
   }

  public function BanValue($id_analysis,$value)
  {
    $value = \urlencode($value);
    $qry = "
    UPDATE METAVALUES
     SET is_hidden_by_user = 1
    WHERE value = '$value'
     AND id_analysis = $id_analysis
    ";
    $this->db->query($qry);
    //echo(var_dump($qry));
    return $this->db->affectedRows();
  }
}