<div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">Status</th>
                      <th style="width: 10px">Indexable</th>
                      <th>Size</th>
                      <th>Links(I/E)</th>
                      <th>Filetype</th>
                      <th>Time</th>
                      <th>URL</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                         <span class="badge bg-warning">300</span></td>
                        </td>
                        <td>
                            Yes
                        </td>
                        </td>
                        <td>
                            350kB
                        </td>
                        <td>
                            100
                            (30 internos)
                            (30 externo)
                            (30 subdominio)
                        </td>
                        <td>
                            HTML, CSS, JS, PNG, JPG,
                        </td>
                        <td>
                            <span class="badge bg-success">< 1s</span>
                            <br>
                            <span class="badge bg-warning"> 1s < t < 3s</span>
                            <br>
                            <span class="badge bg-danger"> 3s <</span>
                         </td>
                      <td> <a href="https://www.google.com/flights?hl=en" target=">blank">https://www.google.com/flights?hl=en</a></td>
                    </tr>
                    <tr>
                      <td>
                         <span class="badge bg-danger">404</span></td>
                        </td>
                        <td>
                            Yes/No
                        </td>
                        <td>
                            350kB
                        </td>
                        <td>
                            100
                        </td>
                        <td>
                            CSS
                        </td>
                        <td>
                            <span class="badge bg-danger">< 1s</span></td>
                        </td>
                      <td> <a href="https://www.google.com/flights?hl=en" target=">blank">https://www.google.com/flights?hl=en</a></td>
                    </tr>
                    <tr>
                      <td>
                         <span class="badge bg-success">200</span></td>
                        </td>
                        <td>
                            Yes/No
                        </td>
                        <td>
                            350kB
                        </td>
                        <td>
                            100
                        </td>
                        <td>
                            JS
                        </td>
                        <td>
                            <span class="badge bg-success">< 1s</span>
                            <br>
                            <span class="badge bg-warning"> 1s < t < 3s</span>
                            <br>
                            <span class="badge bg-danger"> 3s <</span>
                         </td>
                        </td>
                      <td> <a href="https://www.google.com/flights?hl=en" target=">blank">https://www.google.com/flights?hl=en</a></td>
                    </tr>
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->