################################################################################
# Help                                                                         #
################################################################################
Help()
{
   # Display Help
    echo "################################################################################"
    echo "Usage:"
    echo "./deploy.sh -d --> Checkout of last annotated tag in DEV"
    echo "./deploy.sh -p --> Checkout of last annotated tag in MASTER"
    echo "./deploy.sh -t beta2.0 --> Checout the specified tag if it exists"
    echo "################################################################################"
}

git pull
git fetch

while getopts d:p:t:h: option
do
    case "${option}" in
        d) # Get last annotated tag in DEV and checkout
            tagTarget=$(git describe --match beta* --abbrev=0)
            echo $tagTarget
            chk_tag_valid=true
            ;;
        d)  # Get last annotated tag in MASTER and checkout
            tagTarget=$(git describe --match v* --abbrev=0)
            echo $tagTarget
            chk_tag_valid=true
            ;;
        t) # Check if a tag exist. If so, checkout it.
            tag=${OPTARG}
            IFS=$' '
            tagListRaw=$(git tag)
            unset IFS

            if [[ "${tagListRaw[@]}" =~ "${tag}" ]]
            then
                tagTarget=$tag
                chk_tag_valid=true
            else
                echo "$tag"
                echo "Does not exist in the current repo."
                chk_tag_valid=false
            fi
            ;;
        h) # display Help
            Help
            exit
            ;;
        \?) # incorrect option
            echo "Error: Invalid option"
            Help
            exit
            ;;
    esac
done

if [ $chk_tag_valid ]
    then 
    echo "hola" 
        
        git checkout tags/$tagTarget -b $tagTarget
fi

