<?php
 namespace App\Models;

//use CodeIgniter\Model;
      
define('CODEJERCICIO', '2020');
define('CODSERIE','A');
define('CODDIVISA','EUR');
define('IDEMPRESA',1);
define('EDITABLE',1);
define('CODIMPUESTO','IVA0');

class Conta 
// It won't be a CI Model but a Suments Model, then no extents Model
{
    protected $db;
    public $cliente;

    public function __construct()
    {
    $this->db      = \Config\Database::connect('conta',false);
    }
    public function albaranCrear($data)
    {
        $this->clienteLeer($data->codcliente);

        $data->cifnif = $this->cliente->cifnif;
        $data->nombrecliente = $this->cliente->razonsocial;
        
        $data->codejercicio = CODEJERCICIO;
        $data->codserie = CODSERIE;
        $data->coddivisa = CODDIVISA;
        $data->idempresa = IDEMPRESA;
        $data->editable = EDITABLE;
        $data->codimpuesto = CODIMPUESTO;

        $data->total = $data->pvp_unitario * $data->cantidad;
        $data->idalbaran = $this->albaranCrearCabecera($data);

        $data->referencia = $this->productoLeer($data->idproducto)->referencia;
        $this->albaranCrearLinea($data);
    }
    private function albaranCrearCabecera($data)
    {
        $data->codigo = $this->albaranSiguienteCodigo();
        $qry = "
            INSERT INTO albaranescli
             (codcliente, cifnif, codejercicio, codigo,  editable, idempresa, fecha, hora, nombrecliente,
              coddivisa, codserie, total,codalmacen )
            VALUES ('{$data->codcliente}', '{$data->cifnif}', '{$data->codejercicio}', '{$data->codigo}',
                    {$data->editable}, {$data->idempresa}, '{$data->fecha}', '{$data->hora}', '{$data->nombrecliente}',
                    '{$data->coddivisa}', '{$data->codserie}', {$data->total},'ALG')
        ";
        $this->db->query($qry);     // no return expected
        // now, we need the id of the record previously inserted. Our best oprion is to find it
        $qry ="
            SELECT idalbaran 
            FROM albaranescli
            WHERE   codcliente = {$data->codcliente}
                AND fecha = '{$data->fecha}'
                AND hora = '{$data->hora}'
            ";

        return $this->db->query($qry)->getRow()->idalbaran;
    }
    private function albaranCrearLinea($data)
    {
        $qry = "
            INSERT INTO lineasalbaranescli
                (cantidad, codimpuesto, descripcion, idalbaran, idproducto, pvptotal, pvpunitario, referencia)
            VALUES
                ({$data->cantidad}, '{$data->codimpuesto}', '{$data->descripcion}', {$data->idalbaran},
                 {$data->idproducto}, {$data->total}, {$data->pvp_unitario}, '{$data->referencia}')
            ";
        $this->db->query($qry);     // no return expected
    }
    private function albaranSiguienteCodigo()
    {
        $qry ="
            SELECT max(codigo) as codigo
                FROM albaranescli
            ";
        $codigo = $this->db->query($qry)->getRow()->codigo;
        $left = substr($codigo,0,5);
        $right = substr($codigo,5,6);
        $right = $right+1;
        return($left.substr('000000'.$right,-6));
    }
    public function clienteLeer($codcliente)
    {
        $qry = "
            SELECT *
            FROM clientes
            WHERE codcliente = $codcliente
        ";
        $this->cliente = $this->db->query($qry)->getRow();
        return $this->cliente;
    }
    public function productoLeer($idproducto)
    {
        $qry = "
            SELECT *
            FROM productos
            WHERE idproducto = $idproducto
        ";
        return $this->db->query($qry)->getRow();
    }
}