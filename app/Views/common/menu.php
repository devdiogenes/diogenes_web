<!-- J- MENU -->

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="" class="brand-link">
    
        <img src="<?php echo base_url();?>/images/logos/S.png" alt="Suments Data" class="brand-image img-circle elevation-3" style="opacity: 1">
        <span class="brand-text font-weight-light">Suments Data</span>
    </a>
    <div class="sidebar">
        <!-- Panel de usuario -->
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">

            <div class="image">
                <!--<img src="<?= "/users/avatars/$avatar" ?>" class="img-circle elevation-2" alt="User Image">-->
                <img src="https://robohash.org/<?php echo rand();?>?set=set<?php echo array_rand([1,2,3,4],1);?>" class="img-circle elevation-2"  alt="<?= $username ?>">
            </div>
            <div class="info">
                <a href="#" class="d-block"><?= $username ?></a>
            </div>
        </div>

        <!-- Menú izquierda -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
                <!-- Cada elemento <li></li>  es una entrada del menu.  El elemento <li></li> puede contener a su vez 
                otros elementos <ul></ul>, y entonces, se convertirá en un submenú . Para mostrar el simbolo '<' a
                la derecha, hay que incluir la siguiente línea detrás de el elemento:
                <i class="fas fa-angle-left right"></i>
                -->

                <li class="nav-header"><i class="fab fa-product-hunt"></i> Mi dashboard</li>
                <li class="nav-item has-treeview">
                    <a href="/verics" class="nav-link">
                        <i class="fas fa-check"></i>
                        <p>Verics</p>
                    </a>
                </li>

                <li class="nav-header">INTERNAL</li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>Mantenimientos<i class="fas fa-angle-left right"></i></p>
                    </a>
                    <!-- Opciones de sub menu -->
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/mantenimientos/index/usuarios" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Usuarios</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/mantenimientos/index/clientes" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Clientes</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/mantenimientos/index/cias" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Compañías</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/mantenimientos/index/dominios" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Dominios</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/mantenimientos/index/prioridades" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Prioridades</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/mantenimientos/index/categorias" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Categorías</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/mantenimientos/index/metavaluesBanned" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Metavalues baneados</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-header">LOGOUT</li>
                <li class="nav-item">
                    <a href="/logout" class="nav-link">
                        <i class="fas fa-power-off"></i>
                        <p>Logout<i class="fas"></i></p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->

</aside>

<!-- J- MENU END -->