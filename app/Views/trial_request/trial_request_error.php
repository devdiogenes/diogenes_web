<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Suments Data</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- icheck bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>/dist/css/adminlte.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    </head>



    <body class="hold-transition login-page">

        <div class="login-box">
            <div class="login-logo">               
            </div>
            <div class="card card-outline card-danger">
                <div class="card-header">
                    <h3 class="card-title">
                        <a><b>No pudimos procesar tu solicitud</a>
                    </h3>
                </div>
                <div class="card-body">
                    <ul>
                        <li>Vuelve al formulario haciendo click<a href="javascript:history.back()"> aquí</a></li>
                        <li>Escríbenos a <a href="mailto:hola@suments.com">hola@suments.com</a> </li>
                        <li>Usa el widget the abajo para contactar.</li>    
                    </ul>

                </div>

            </div>
            <!-- jQuery -->
            <script src="<?php echo base_url(); ?>/plugins/jquery/jquery.min.js"></script>
            <!-- Bootstrap 4 -->
            <script src="<?php echo base_url(); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
            <!-- AdminLTE App -->
            <script src="<?php echo base_url(); ?>/dist/js/adminlte.min.js"></script>

            <script data-jsd-embedded data-key="03f17473-9257-465b-8a3c-c9b56969fca0" data-base-url="https://jsd-widget.atlassian.com" src="https://jsd-widget.atlassian.com/assets/embed.js"></script>

    </body>
</html>
