<?php

namespace App\Models;

//use CodeIgniter\Model;

use DateTime;
use DatePeriod;
use DateInterval;

class VericsDashboard //extends Model
{
    private $db;
    private $id_analysis;
    private $severity_leakage = 150;
    private $severity_sensible = 100;
    private $severity_style = 50;
    private $time_scale = "HOUR";
    public $id_customer;

    public function __construct($id_customer)
     {
        $this->db = \Config\Database::connect();
        $this->id_customer = $id_customer;
        $this->db = \Config\Database::connect();
     }
    public function getAnalisisList($dateStart, $dateEnd)
     {
        $qry = "
        SELECT ANALYSIS.id_analysis, ANALYSIS.token,ANALYSIS.date_requested, ANALYSIS.date_started,ANALYSIS.date_ended,
	           DOMAINS.domain, COMPANIES.name_comercial
         FROM ANALYSIS
   	        JOIN DOMAINS ON DOMAINS.id_domain = ANALYSIS.id_domain
            JOIN COMPANIES on COMPANIES.id_company = DOMAINS.id_company
         WHERE COMPANIES.id_customer = $this->id_customer
            AND ANALYSIS.date_requested >= '$dateStart'
            AND ANALYSIS.date_requested <= '$dateEnd'
         ORDER BY ANALYSIS.date_requested
        ";
        return $this->db->query($qry)->getResult();

     }
    public function get_dashboard_summary()
     {
        $data = array();

        $data["g131"] = $this->get_dashboard_g131();
        $data["g132"] = $this->get_dashboard_g132();
        $data["g133"] = $this->get_dashboard_g133();
        $data["table"] = $this->get_dashboard_table();
        return $data;
     }

    public function get_table($page,$itemsPerPage)
     {
        $data["table"] = $this->get_dashboard_table($page,$itemsPerPage);
        return $data;
     }
    public function get_graphs()
     {
        $data = array();
        $data["g131"] = $this->get_dashboard_g131();       
        $data["g132"] = $this->get_dashboard_g132();
        $data["g133"] = $this->get_dashboard_g133();

        return $data;
     }
    public function get_g131()
     {
        return $this->get_dashboard_g131();
     }
    public function get_g132()
     {
        return $this->get_dashboard_g132();
     }
    public function get_g133()
     {
        return $this->get_dashboard_g133();
     }
    /**
     *  Builds a table with the data of de domains analised or not
     * @param integer $customer
     * @return string Table to be shown
     */
    private function makeTable($filas)
     {
        foreach ($filas as $f) {
            $stat = $this->getStatus($f);
            $fila['name_comercial'] = $f->name_comercial;
            $fila['domain'] = $f->domain;
            $fila['date_requested'] = $f->date_requested;
            $fila['status'] = $stat[0];
            $fila['id_analysis'] = $f->id_analysis;
            $fila['id_customer'] = $f->id_customer;
            $fila['id_domain'] = $f->id_domain;
            $fila['launchable'] = $stat[1];
            $tabla[] = $fila;
        }
        return $tabla;
     }
    /**
     * Returns the content of a cell for the status of an analysis
     * @param object $row
     * @return array( string $cell, bool $launchable)
     */
    private function getStatus($row)
     {
        if (!is_null($row->date_ended))
            return ["ready", true];
        if (!is_null($row->date_started))
            return ["working", false];

        if (!is_null($row->date_requested))
            return ["queue", false];

        return ["", true];
     }
    /**
     * 
     * @param integer $customer (id_customer)
     * @return array of objects containing the list needed for the table
     */
    private function get_dashboard_table($page,$itemsPerPage)
     {   
        $LIMIT = $page == 0 ? '' : "LIMIT $itemsPerPage OFFSET " . ($page-1) * $itemsPerPage;
        $LIMIT = $itemsPerPage == 0 ? '' : $LIMIT;
        $qry = " SELECT DISTINCT 
                    DOMAINS.id_domain,domain,
                    DOMAINS.id_company,
                    date_requested,
                    date_ended,
                    date_started,
                    COMPANIES.name_comercial,
                    CUSTOMERS.id_customer,
                    ANALYSIS.id_analysis
                FROM  DOMAINS 
                LEFT JOIN ANALYSIS ON ANALYSIS.id_domain = DOMAINS.id_domain 
                JOIN COMPANIES ON DOMAINS.id_company = COMPANIES.id_company
                JOIN CUSTOMERS ON COMPANIES.id_customer = CUSTOMERS.id_customer
                WHERE CUSTOMERS.id_customer = $this->id_customer
                    AND DOMAINS.id_analysis IS NULL
                ORDER BY ANALYSIS.date_requested DESC
                $LIMIT";

        $raw = $this->db->query($qry)->getResultObject();

        if (count($raw) == 0){
            $raw[0]['name_comercial'] = '';
            $raw[0]['domain'] = '';
            $raw[0]['date_requested'] = '';
            $raw[0]['status'] = '';
            $raw[0]['id_analysis'] = '';
            $raw[0]['id_customer'] = '';
            $raw[0]['id_domain'] = '';
            $raw[0]['launchable'] = '';
            return $raw;

        }
        return $this->makeTable($raw);
     }
    private function get_dashboard_g131()
     {
        $qry = "SELECT
                    COUNT(REL_DOCUMENTS_DOMAINS.id_document) AS 'data',
                    DATE(DOMAINS.date_added) AS 'labels'            
                    FROM REL_DOCUMENTS_DOMAINS
                    JOIN DOMAINS ON DOMAINS.id_domain = REL_DOCUMENTS_DOMAINS.id_domain
                    JOIN COMPANIES ON COMPANIES.id_company = DOMAINS.id_company
                    WHERE  COMPANIES.id_customer = $this->id_customer
                    GROUP BY labels
                    ORDER BY labels ASC";
        $raw = $this->db->query($qry)->getResultArray();
        
        // If they have not performed any analysis, initialize the array to be compatible with view.
        if (count($raw) == 0){
            $raw = array("data" => array(),"labels" => array());
            return $raw;
        }
        else{
            $raw = $this->equi_space_dates($raw);
        }
        //var_dump($raw);
        //var_dump($this->format_graph_data_labels($raw));
        return $this->format_graph_data_labels($raw);
     }

    private function equi_space_dates($input, $max_items = 30, $dates_freq = 1)
     {
        $tmp = array(
            "values" => array(),
            "dates" => array(),
        );

        $formated = array();
        $dataset = array(
            "data" => array(),
            "label" => array()
        );

        foreach ($input as $current) {
            array_push($tmp["values"], $current["data"]);
            array_push($tmp["dates"], $current["labels"]);
        }

        //var_dump($tmp);
        $originOfTimes = date_create_from_format('Y-m-d', min($tmp["dates"]));
        $endOfTimes = date_create_from_format('Y-m-d', max($tmp["dates"]));
        $diff = date_diff($originOfTimes, $endOfTimes, false);

        // Check what the dates period should be grouped by.
        if ($diff->days < $max_items) {
            $interval = new DateInterval('P10D');
        } else if ($max_items <= $diff->days && $diff->days < 2 * $max_items) {
            $interval = new DateInterval('P20W');
        } else {
            $interval = new DateInterval('P30W');;
        }

        $period = new DatePeriod($originOfTimes, $interval, $endOfTimes);

        foreach ($period as $dt) {
            $formated[$dt->format("Y-m-d")] = $dataset;
            $formated[$dt->format("Y-m-d")]["label"] = $dt->format("Y-m-d");
            $formated[$dt->format("Y-m-d")]["data"] = 0;
        }
       //var_dump($tmp);
       $acumm = 0;
        for ($i = 0; $i < count($tmp["dates"]); $i++) {
            
            $formated[$tmp["dates"][$i]]["data"] = (int) $tmp["values"][$i] + $acumm;
            $formated[$tmp["dates"][$i]]["label"] = ($i % $dates_freq == 0) ? $tmp["dates"][$i] : "";
            $acumm += (int) $tmp["values"][$i];

        }
        //var_dump($formated);
        return $formated;
     }
    private function format_graph_data_labels($raw_data, $key_data = "data", $key_label = "label")
     {
        $formated = array(
            "data" => array(),
            "labels" => array()
        );
    
        foreach ($raw_data as $data) {
            array_push($formated["data"], $data[$key_data]);
            array_push($formated["labels"], $data[$key_label]);
        }
    
        return $formated;
     }
    private function get_dashboard_g132()
     {
        $qry = "SELECT 
                  DISTINCT GEO_CITIES.name,
                  GEO_CITIES.latitude,
                  GEO_CITIES.longitude,
                  DOMAINS.domain
                  FROM GEO_CITIES
                  JOIN DOMAINS ON DOMAINS.id_city = GEO_CITIES.id_city
                  JOIN COMPANIES ON COMPANIES.id_company = DOMAINS.id_company
                  JOIN CUSTOMERS ON CUSTOMERS.id_customer = COMPANIES.id_customer
                  WHERE CUSTOMERS.id_customer = $this->id_customer
                  GROUP BY domain";
        return $this->db->query($qry)->getResultArray();
     }
    private function get_dashboard_g133()
     {
        $qry = "SELECT 
        COUNT(*) AS 'data', 
        METAVALUES.metaseverity_name AS 'label'

        FROM METAVALUES 
        
        JOIN COMPANIES ON COMPANIES.id_company = METAVALUES.id_company
            
        WHERE (METAVALUES.weigth = 100  OR METAVALUES.weigth = 150)
        AND COMPANIES.id_customer = $this->id_customer
        AND METAVALUES.is_hidden = 0
        AND METAVALUES.is_hidden_by_user = 0
        GROUP BY METAVALUES.weigth
        ORDER BY METAVALUES.weigth DESC";                
         
        $raw_results = $this->db->query($qry)->getResultArray();
        return $this->format_graph_data_labels($raw_results);
     }
}
