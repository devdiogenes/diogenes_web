<?php

namespace App\Models;

use CodeIgniter\Model;

class Userlog extends Model
{
  protected $table      = 'USER_LOG';
  protected $primaryKey = 'id_user_log';

  protected $returnType = 'object';
  protected $useSoftDeletes = false;

  protected $allowedFields = ['accion', 'id_user'];


  protected $validationRules    = [];
  protected $validationMessages = [];
  protected $skipValidation     = true;

  private $id_user;


  public function __construct($id_user = NULL, \CodeIgniter\Database\ConnectionInterface &$db = null, \CodeIgniter\Validation\ValidationInterface $validation = null)
  {
    parent::__construct($db, $validation);
    $this->id_user = $id_user;
    //        
  }
  /**
   * Graba el mensaje especificado para el usuario actual.
   * @param string $msg
   */
  public function log($msg)
  {

    $builder = $this->db->table($this->table);

    if (!is_null($this->id_user)) {
      $to_insert = [
        'action' => $msg,
        'id_user'  => $this->id_user,
        'ip_user'  => $this->get_ip_user()
      ];
      $builder->insert($to_insert);
    }
  }
  //Obtiene la IP del cliente
  private function get_ip_user()
  {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
      $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
      $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
      $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
      $ipaddress = getenv('REMOTE_ADDR');
    else
      $ipaddress = 'UNKNOWN';
    return $ipaddress;
  }
}