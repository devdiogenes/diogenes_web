<?php

namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */
use CodeIgniter\Controller;

class BaseController extends Controller
{

 public $user;
 public $permitido;    //categorias autorizadas a ejecutar un controlador
 public $userCat;      //categorias del usuario activo

 /**
  * An array of helpers to be loaded automatically upon
  * class instantiation. These helpers will be available
  * to all other controllers that extend BaseController.
  *
  * @var array
  */
 //protected $helpers = [];

 /**
  * Constructor.
  */
 //public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
 public function __construct()
 {
  $this->user = New \App\Models\User2();
 }

 public function seguridad()
 {
   if (isset($_POST['token']) or !$this->user->loged) // user not loged or token passed. Login
    $this->login();
  if (in_array(256, $this->user->userLevels)) // tiene permisos de Bofh
   return true;
  foreach ($this->permitido as $p)
  {
   if (in_array($p, $this->user->userLevels)) // en cuanto encontremos que tiene uno de los permisos que autorizan,
   //  retornamos true
    return true;
  else
    return false;
  }
 }

 /*public function s_render($output, $salida = array())
 {


  foreach ($output as $key => $values)
  {


   if (count($output[$key]) == 0)
   {
    continue;
   }

   foreach ($values as $file)
   {
    echo view($file, $salida);
   }
  }
 }*/

}
