<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Suments Data</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- icheck bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>/dist/css/adminlte.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    </head>



    <body class="hold-transition login-page">


        <div class="login-box">
            <div class="login-logo">               
            </div>
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">
                        <a class= "font-weight-bold">Verics</a><a href="http://suments.com/"> Suments Data</a>
                    </h3>
                </div>

                <form class="form-horizontal" method="POST" action="/trial_request">
                    <div class="card-body">
                        <?= $msg ?>
                        <label class="col-form-label" for="inputError">Informacion sobre el analisis...</label>
                        <div class="row">
                            <div class="col-5">
                                <input type="text" class="form-control" placeholder="Compañía" name="company" value='<?= $company ?>'>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control" placeholder="www.example.com" name="domain" value="<?= $domain ?>">
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <label class="col-form-label" for="inputError">Sobre ti...</label>
                        <div class="form-group row">
                            <div class="col">
                                <input type="text" class="form-control" id="name_org" placeholder="Tu organización" name="customer" value="<?= $customer ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                <input type="email" class="form-control" id="email" placeholder="Email" name="email" value="<?= $email ?>">
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <label class="col-form-label" for="inputError">Contraseña</label>
                        <div class="form-group row">
                            <div class="col">
                                <input type="password" class="form-control" id="psw1" placeholder="Contraseña" name="psw1" value="<?= $psw1 ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                <input type="password" class="form-control" id="psw2" placeholder="Repetir contraseña" name="psw2" value="<?= $psw2 ?>">
                            </div>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck2" name="tyc"  <?= $tyc_stat ?>>
                            <label class="form-check-label" for="exampleCheck2" name="tyc" >Acepto <a href="http://suments.com/index.php/terminos-y-condiciones/">Terminos y condiciones</a></label>
                        </div>
                    </div>

                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info">Request</button>
                    </div>
            </div>
            <!-- /.card -->



        </form>

    </div>
    <!-- /.login-card-body -->

    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo base_url(); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>/dist/js/adminlte.min.js"></script>

    <script data-jsd-embedded data-key="03f17473-9257-465b-8a3c-c9b56969fca0" data-base-url="https://jsd-widget.atlassian.com" src="https://jsd-widget.atlassian.com/assets/embed.js"></script>

</body>
</html>
