<?php
 namespace App\Models;

use CodeIgniter\Model;

class Metaclean extends Model
{
  protected $table      = 'METACLEAN';
  protected $primaryKey = 'id_metaclean';
  protected $returnType = 'object';
  protected $tempReturnType = 'object';
  protected $useSoftDeletes = false;
  protected $allowedFields = ['id_metaclean,','id_analysis','date_requested','date_started','date_ended'
                             ,'date_delete','downloaded','toomanyfiles'];
  protected $db;                // database
  protected $dt;                // datetime
  protected $tz;                // timezone
  public $row ;
  public $status;
  
  public function __construct()
    {
        $this->db      = \Config\Database::connect();
        $this->dt = new \DateTime();
        $this->tz = new \DateTimeZone('Europe/Madrid');
        $this->dt->setTimezone($this->tz);
    }
  public function getNextMetaclean()    // retrieves the next analysis to clean
   {
      $qry = "
        SELECT * FROM METACLEAN
        WHERE ISNULL(date_started)
        ORDER BY date_requested
        LIMIT 1
      ";
      $res = $this->db->query($qry)->getRow();  
      if (is_null($res))                        // no results
        return false;
      $this->row = $res;                  
      return $this->row;                        // return results and store it as property
   }
  public function start()               // store date started in BD
   {
    if (!$this->row)
        return false;    
    $this->row->date_started = $this->dt->format('Y-m-d, H:i:s');
    $this->update($this->row->id_metaclean,(array) $this->row);
   }
  public function end($days)            // store date end and date delete in BD
   {
    if (!$this->row)
        return false;
    $date_end = new \DateTime();
    $date_end->setTimezone($this->tz);
    $this->row->date_ended = $date_end->format('Y-m-d, H:i:s');
    $daysAdd = New \DateInterval("P{$days}D");
    $this->row->date_delete = $date_end->add($daysAdd)->format('Y-m-d, H:i:s');
    $this->update($this->row->id_metaclean,(array) $this->row);
   }
  public function tooManyFiles()        // set toomanyfiles flag to true in BD and then call end()
   {
    $this->row->toomanyfiles=1;
    $this->end(0);
   }
  public function request($id_analysis)
   { 
    $this->row = New \stdClass();
    $this->row->date_requested = $this->dt->format('Y-m-d, H:i:s');
    $this->row->id_analysis = $id_analysis;
    $this->row->id_metaclean = $this->insert((array) $this->row);
   }
  public function getStatus($id_analysis)
   {
    $qry = "
     SELECT * FROM METACLEAN
     WHERE id_analysis = $id_analysis
     ORDER BY date_requested DESC
     LIMIT 1
     ";
   $this->row = $this->db->query($qry)->getRow();  
   if (is_null($this->row))                        // no results
    {
    $this->row = New \stdClass();
    $this->row->status = 'Not ordered yet';
    $this->row->date_requested = '';
    $this->row->date_started = '';
    $this->row->date_ended = '';
    $this->row->date_delete = '';
    $this->row->downloaded = 0;
    $this->row->toomanyfiles = false;
    $this->row->download = '';
    return $this->row;
    }
   if ($this->row->date_requested != "")
    $this->row->status = 'queued';
   if ($this->row->date_started != "")
    $this->row->status = 'ongoing';
   if ($this->row->date_ended != "")
    {
     $this->row->status = 'done';
     $this->row->download = "https://mclean.suments.com/files_to_serve/$id_analysis.zip";
    }
   if($this->row->toomanyfiles)
   $this->row->status = "too many files";                 
   return $this->row;                        // return results and store it as property
   }
}

