<div class="content">
    <div class="content-header"></div>

    <section class="content">
        <!-- SECTION SUMMARY-->
        <div class="card">
            <div class="card-header" style="background-color:#3a8bba">
                <h3 class="card-title">Summary</h3>
            </div>


            <div class="col-12 col-md-12">
                <!-- Information section -->
                <div class="card">
                    <div class="card-header" style="background-color:#d7e8f1">
                        <h3 class="card-title">
                            <a><i class="fas fa-info-circle"></i> ¿Que es esto?</a>
                        </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <p>Estas son las filtraciones que hemos encontrado en tus documentos.
                            Las tienes organizadas por frecuencia de aparición.
                            <a class="font-weight-bold" href="http://suments.com/datos-filtrados-y-sensibles/" target=”_blank”> Leer mas...</a></p>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>


            <div class="card">
                <div class="card-body">
                    <div class="col-12 col-md-12">
                        <div class="row">
                            <table class="table text-nowrap">
                                <tbody>
                                    <tr>
                                        <th>Nº de filtraciones</th>
                                        <td>
                                            <p class="<?php echo ($s_summary["total_leakages"] > 0 ? "font-weight-bold text-danger" : "text-success"); ?>">
                                                <?php echo $s_summary["total_leakages"]; ?>
                                            </p>
                                        </td>
                                        <th>Subdominios encontrados</th>
                                        <td>
                                            <p><?php echo $s_summary["total_subdomains"]; ?></p>
                                        </td>
                                        <td rowspan="3">
                                            <img style="max-height: 250px; max-width:300px; width: 100%;" src="https://api.pagelr.com/capture/javascript?key=7A9EmhQRS0mm2aVsgnPNlA&uri=<?php echo $s_summary["company_domain"]; ?>&b_width=1280&width=300&height=250&delay=1000&format=jpg&maxage=86400&">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Compañía</th>
                                        <td>
                                            <p><?php echo $s_summary["company_name"]; ?></p>
                                        </td>

                                        <th>Documentos analizados</th>
                                        <td>
                                            <p><?php echo $s_summary["total_documents"]; ?></p>
                                        </td>
                                    </tr>
                                    </tr>
                                    <tr>
                                        <th>Dominio original</th>
                                        <td>
                                            <a href="<?php echo $s_summary["company_domain"]; ?>" target="_blank">
                                                <p><?php echo $s_summary["company_domain"]; ?></p>
                                            </a>
                                        </td>
                                        <th>Tiempo de procesado</th>
                                        <td>
                                            <p><?php echo $s_summary["total_time"]; ?></p>
                                        </td>
                                    </tr>
                                    <tr>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--</div>-->

<!-- SECTION LEAKAGES -->
<div class="card">
    <div class="card-header" style="background-color:#3a8bba">
        <h3 class="card-title">Potenciales filtraciones</h3>
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-12 col-md-12">
                <!-- Information section -->
                <div class="card">
                    <div class="card-header" style="background-color:#d7e8f1">
                        <h3 class="card-title">
                            <a><i class="fas fa-info-circle"></i> ¿Que es esto?</a>
                        </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <p>Aquí tienes un resumen de la información mas relevante que hemos encontrado.
                            <a class="font-weight-bold" href="http://suments.com/resumen/" target=”_blank”> Leer mas...</a></p>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <div class="card-body">
                <!-- Graph section -->
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="s_leakages_g121" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="card-body">
                            <canvas id="s_leakages_g122" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-12">
                        <table id="s_leakages_table" class="table table-bordered table-hover display">

                            <thead>
                                <tr>
                                    <th>Nº de apariciones</th>
                                    <th>Dato</th>
                                    <th>Severidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Dato</th>
                                    <th>Nº de apariciones</th>
                                    <th>Severidad</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
                <div class="row">
                    <div class="col-12 col-md-3">
                        <blockquote>
                            <a href="#raw_data">
                                <button type="button" class="btn btn-block btn-outline-primary far fa-eye" style="color:#3a8bba ">Ver todos los detalles</button>
                            </a>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- SECTION DOCUMENTS -->
<div class="card">
    <div class="card-header" style="background-color:#3a8bba">
        <h3 class="card-title">Documentos</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        </div>
    </div>
    <div class="card-body">
        <div class="col-12 col-md-12">
            <!-- Information section -->
            <div class="card">
                <div class="card-header" style="background-color:#d7e8f1">
                    <h3 class="card-title">
                        <a><i class="fas fa-info-circle"></i> ¿Que es esto?</a>
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <p>Así es como se distribuyen las filtraciones de tus datos según los tipos de documentos.
                        <a class="font-weight-bold" href="http://suments.com/agrupacion-por-documentos/" target=”_blank”> Leer mas...</a></p>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="card-body">
                    <canvas id="s_documents_g121" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="card-body">
                    <canvas id="s_documents_g122" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
            </div>

        </div>

        <div class="card-body">
            <table id="s_documents_table" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <!-- <th>Filtraciones</th> -->
                        <th>Datos</th>
                        <th>Documento</th>
                        <th>Tipo de archivo</th>
                        <th>Dominio</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                    <tr>
                        <!-- <th>Filtraciones</th> -->
                        <th>Datos</th>
                        <th>Documento</th>
                        <th>Tipo de archivo</th>
                        <th>Dominio</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="row">
            <div class="col-12 col-md-3">
                <blockquote>
                    <a href="#raw_data">
                        <button type="button" class="btn btn-block btn-outline-primary far fa-eye" style="color:#3a8bba ">Ver todos los detalles</button>
                    </a>
                </blockquote>
            </div>
        </div>
    </div>
</div>

<!-- SECTION DOMAINS -->
<div class="card">
    <div class="card-header" style="background-color:#3a8bba">
        <h3 class="card-title">Dominios y subdominios</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        </div>
    </div>
    <div class="card-body">
        <div class="col-12 col-md-12">
            <!-- Information section -->
            <div class="card">
                <div class="card-header" style="background-color:#d7e8f1">
                    <h3 class="card-title">
                        <a><i class="fas fa-info-circle"></i> ¿Que es esto?</a>
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    Aquí tienes como es la distribución de filtraciones agrupadas por dominios.

                    <a href="http://suments.com/agrupacion-por-dominios/" target=”_blank”> Leer mas...</a>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="card-body">
                    <div class="chart">
                        <canvas id="s_domains_g121" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="card-body">
                    <div class="chart">
                        <div id="s_domains_g122" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></div>
                    </div>
                </div>
            </div>

        </div>

        <div class="card-body">
            <table id="s_domains_table" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Dato</th>
                        <th>Dominio</th>
                        <th>Riesgos</th>
                    </tr>
                </thead>
                <tbody>
                   
                </tbody>
                <tfoot>
                    <tr>
                        <th>Dato</th>
                        <th>Dominio</th>
                        <th>Riesgos</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="row">
            <div class="col-12 col-md-3">
                <blockquote>
                    <a href="#raw_data">
                        <button type="button" class="btn btn-block btn-outline-primary far fa-eye" style="color:#3a8bba ">Ver todos los detalles</button>
                    </a>
                </blockquote>
            </div>
        </div>
    </div>


    <!-- card 4 -->
    <div class="card">
        <div class="card-header" style="background-color:#3a8bba">
            <a name="raw_data"></a>
            <h3 class="card-title">Información para descargar</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>


        <div class="card-body">
            <div class="col-12 col-md-12">
                <!-- Information section -->
                <div class="card">
                    <div class="card-header" style="background-color:#d7e8f1">
                        <h3 class="card-title">
                            <a><i class="fas fa-info-circle"></i> ¿Que es esto?</a>
                        </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        Aquí tienes toda la infomación sin agrupar. ¿Hay alguna sección que te gustaría que añadiesemos? ¡Descargatela y cuentanos tu opinión!
                        <a href="http://suments.com/tus-datos/" target=”_blank”> Leer mas...</a>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <table id="s_raw_table" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Severidad</th>
                        <th>Dato</th>
                        <th>Tipo</th>
                        <th>Ciudad</th>
                        <th>Dominio</th>
                        <th>Link</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                    <tr>
                        <th>Severidad</th>
                        <th>Dato</th>
                        <th>Tipo</th>
                        <th>Ciudad</th>
                        <th>Dominio</th>
                        <th>Link</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    </section>
</div>

<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detalles</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>En contrucción =D</p>
                <!-- <img src="<?php echo base_url(); ?>/images/icons/under_construction.png"> </div> -->
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <script src="<?php echo base_url(); ?>/plugins/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/jquery-ui/jquery-ui.min.js"></script>

    <script src="<?php echo base_url(); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>/plugins/jquery/jquery.min.js"></script> -->

    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="<?php echo base_url(); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/chart.js/Chart.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#s_leakages_table').DataTable({
                    dom: 'Bfrtip',
                    buttons: ['csv', 'excel'],
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": false,
                    "info": true,
                    "autoWidth": false,
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "ajax": "/verics/ajax/leakages/<?= $id_analysis ?>"
                }),
                $('#s_documents_table').DataTable({
                    dom: 'Bfrtip',
                    buttons: ['csv', 'excel'],
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": false,
                    "info": true,
                    "autoWidth": false,
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "ajax": "/verics/ajax/doct/<?= $id_analysis ?>"
                }),
                $('#s_domains_table').DataTable({
                    dom: 'Bfrtip',
                    buttons: ['csv', 'excel'],
                    "paging": true,
                    "lengthChange": false,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "ajax": "/verics/ajax/domt/<?= $id_analysis ?>"

                }),
                $('#s_raw_table').DataTable({
                    dom: 'Bfrtip',
                    buttons: ['csv', 'excel'],
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": false,
                    "info": true,
                    "autoWidth": false,
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "ajax": "/verics/ajax/rt/<?= $id_analysis ?>"
                });
        });
    </script>

    <script>
        $(function() {

            var donutChartCanvas = $('#s_leakages_g122').get(0).getContext('2d')
            var donutData = {
                labels: [<?php
                            foreach ($s_leakages["g122"]["labels"] as $label)
                                echo '"' . $label . '", ';
                            ?>],
                datasets: [{
                    data: [<?php foreach ($s_leakages["g122"]["data"] as $data) : ?> <?php echo $data; ?>, <?php endforeach; ?>],
                    backgroundColor: ['#3a8bba', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
                }]
            }
            var donutOptions = {
                maintainAspectRatio: false,
                responsive: true,
                legend: {
                    display: true,
                    position: 'right',
                    align: 'left'
                },
                title: {
                    display: false,
                    text: "<?php echo $s_leakages["g122"]["title"]; ?>"
                }
            }
            var donutChart = new Chart(donutChartCanvas, {
                type: 'pie',
                data: donutData,
                options: donutOptions,
            })
        });
    </script>
    <script>
        $(function() {
            var barChartCanvas = $('#s_documents_g121').get(0).getContext('2d')
            var barChartData = {
                labels: [<?php foreach ($s_documents["g121"]["labels"] as $label) : ?> "<?php echo $label; ?>", <?php endforeach; ?>],
                datasets: [{
                    backgroundColor: 'rgba(60,141,188,0.9)',
                    borderColor: 'rgba(60,141,188,0.8)',
                    pointRadius: false,
                    pointColor: '#3b8bba',
                    pointStrokeColor: 'rgba(60,141,188,1)',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data: [<?php foreach ($s_documents["g121"]["data"] as $data) : ?> <?php echo $data; ?>, <?php endforeach; ?>],
                }, ]
            }
            var barChartOptions = {
                responsive: true,
                maintainAspectRatio: false,
                datasetFill: false,
                legend: {
                    display: false
                },
                title: {
                    display: false,
                    text: "<?php echo $s_leakages["g121"]["title"]; ?>"
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false,

                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: true,
                        },
                        ticks: {
                            Min: 0,
                        }
                    }]
                }

            }
            var barChart = new Chart(barChartCanvas, {
                type: 'bar',
                data: barChartData,
                options: barChartOptions
            })
        });
    </script>
    <script>
        $(function() {
            var stackedBarChartCanvas = $('#s_domains_g121').get(0).getContext('2d')

            var my = {
                labels: [<?php foreach ($s_domains["g121"]["labels"] as $label) : ?> "<?php echo $label; ?>", <?php endforeach; ?>],
                datasets: [
                    <?php foreach ($s_domains["g121"]["datasets"] as $data) : ?> {
                            label: '<?php echo $data["label"]; ?>',
                            data: [<?php foreach ($data["data"] as $datapoints) : ?> <?php echo $datapoints; ?>, <?php endforeach; ?>],
                            backgroundColor: "<?php echo $data["backgroundColor"]; ?>"
                        },
                    <?php endforeach; ?>
                ]
            }

            var stackedBarChartData = jQuery.extend(true, {}, my)

            var stackedBarChartOptions = {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: {
                            display: false,
                        }
                    }],
                    yAxes: [{
                        stacked: true,

                    }]
                }
            }

            var stackedBarChart = new Chart(stackedBarChartCanvas, {
                type: 'bar',
                data: stackedBarChartData,
                options: stackedBarChartOptions
            })
        });
    </script>
    <script>
        $(function() {
            var ctx = document.getElementById("s_documents_g122").getContext('2d');
            // End Defining data
            var scatterOptions = {
                animation: false,
                tooltips: {
                    enabled: false
                },
                responsive: true, // Instruct chart js to respond nicely.
                maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height  
                responsive: true,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false,

                        },
                        ticks: {
                            stepSize: 1,
                            callback: function(value, index, values) {
                                xTicks = ["", <?php foreach ($s_documents["g122"] as $filetype => $data) : ?> "<?php echo $filetype; ?>", <?php endforeach; ?>, ""];
                                return xTicks[index];
                            }
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: true
                        },
                        ticks: {
                            stepSize: 1,
                            Min: 0.5,
                            Max: <?php echo count($s_documents["g122"]) + 0.5; ?>,
                            callback: function(value, index, values) {
                                yTicks = ["", "Filtraciones", "", "Sensibles", "", "Estilo", ""];
                                return yTicks[index];
                            }
                        }
                    }]
                }
            }
            // End Defining data
            function generateData(n, xCenter, yCenter) {
                var data = [];
                var spread = 0.30
                for (var i = 0; i < n; i++) {
                    data.push({
                        //x: ((Math.random() - 0.5) * spread + xCenter),
                        //y: (Math.random() - 0.5) * spread + yCenter
                        x: xCenter + Math.random() * spread * Math.cos((360*i)/n),
                        y: yCenter + Math.random() * spread * Math.sin((360*i)/n),
                    })
                }
                return data
            }
            var scatterChartData = {
                labels: <?php echo $s_leakages["g121"]["labels"]; ?>,
                datasets: [
                    <?php foreach ($s_documents["g122"] as $filetype => $data) : ?> {
                            label: "<?php echo $data["label"]; ?>",
                            backgroundColor: "#3b8bba33",
                            data: generateData(<?php echo $data["total_style"]; ?>, <?php echo $data["position"]; ?>, <?php echo $data["position_style"]; ?>)
                        },
                        {
                            label: "<?php echo $data["label"]; ?>",
                            backgroundColor: "#ffeebdAA",
                            data: generateData(<?php echo $data["total_sensible"]; ?>, <?php echo $data["position"]; ?>, <?php echo $data["position_sensible"]; ?>)
                        },
                        {
                            label: "<?php echo $data["label"]; ?>",
                            backgroundColor: "#f7c6cbDD",
                            data: generateData(<?php echo $data["total_leakage"]; ?>, <?php echo $data["position"]; ?>, <?php echo $data["position_leakage"]; ?>)
                        },
                    <?php endforeach; ?>
                ]
            }

            var myChart = new Chart(ctx, {
                showTooltips: false,
                type: 'scatter',
                data: scatterChartData,
                options: scatterOptions
            });
        });
    </script>
    <script>
        $(function() {
            var ctx = document.getElementById('s_leakages_g121');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: [<?php foreach ($s_leakages["g121"]["labels"] as $label) : ?> "<?php echo $label; ?>", <?php endforeach; ?>],
                    datasets: [{
                        data: [<?php foreach ($s_leakages["g121"]["data"] as $data) : ?> <?php echo $data; ?>, <?php endforeach; ?>],
                        backgroundColor: '#3b8bba',
                    }]
                },
                options: {
                    legend: {
                        display: false,
                    },
                    scales: {
                        xAxes: [{
                            stacked: false,
                            gridLines: {
                                display: false,
                            },
                            ticks: {
                                display: true,
                            },
                        }],
                        yAxes: [{
                            stacked: false,
                            ticks: {
                                display: true,
                                min: 0
                            },
                        }]
                    }
                }
            })
        });
    </script>


    <script src="<?php echo base_url(); ?>/plugins/jvectormap/jquery-jvectormap-2.0.5.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/jvectormap/jquery-jvectormap-world-mill.js"></script>
    <script>
        $(function() {
            $('#s_domains_g122').vectorMap({
                map: 'world_mill',
                scrollWheelZoom: false,
                scaleColors: ['#00FF00', '#0000FF'],
                normalizeFunction: 'polynomial',
                hoverOpacity: 0.7,
                hoverColor: false,
                markerStyle: {
                    initial: {
                        fill: '#F8E23B',
                        stroke: '#383f47'
                    }
                },
                regionStyle: {
                    initial: {
                        fill: '#128da7'
                    },
                    hover: {
                        fill: "#A0D1DC"
                    }
                },
                backgroundColor: '#FFFFFF',
                markers: [
                    <?php foreach ($s_domains["g122"] as $city) : ?> {
                            latLng: [<?php echo $city["latitude"]; ?>, <?php echo $city["longitude"]; ?>],
                            name: '<?php echo $city["name"]; ?>'
                        },
                    <?php endforeach; ?>

                ]
            });
        });
    </script>