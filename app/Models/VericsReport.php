<?php

namespace App\Models;

//use CodeIgniter\Model;

class VericsReport //extends Model
{
  private $db;
  public  $id_analysis;
  private $severity_leakage  = 150;
  private $severity_sensible = 100;
  private $severity_style    = 50;
  private $default_items_table = 30;
  private $severity_ok       = 10;
  private $limit_preview     = 30;
  public  $analysis;
  public  $domain;
  public function __construct($id)
  {
    $this->db          = \Config\Database::connect();
    $this->analysis = new \App\Models\Analysis($id);
    $this->id_analysis = $id;
    $this->domain = $this->analysis->domain_name;
  }
  public function get_all()
  {
    $data = array(
      "s_summary" => $this->get_s_summary(),
      "s_leakages" => $this->get_s_leakages(),
      "s_documents" => $this->get_s_documents(),
      "s_domains" => $this->get_s_domains(),
      "get_s_raw_table" => $this->get_s_raw_table(),
    );
    return $data;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////
  public function get_s_summary()
  {
    return array(
      "total_leakages"   => $this->get_s_summary_total_leakages(), //Int. COUNT METAVALUES WHERE ID_METASEVEIRY = 4
      "total_time"       => $this->get_s_summary_total_time(), //Diff datetime. DATE_ENDED - DATE_STARTED from ANALYSIS
      "total_subdomains" => $this->get_s_summary_total_domains(), // Int. COUNT TOTAL DOMAINS WHERE ID_ANALYSIS
      "total_documents"  => $this->get_s_summary_total_documents(), // Int. COUNT TOTAL REL_DOCS_ANALYSIS WHERE ID_ANALYSIS
      "company_name"     => $this->get_company_name(), // Str. COMERCIAL NAME COMPANY
      "company_domain"   => $this->get_domain_seed(), // Str. ORIGINAL DOMAIN WHERE COMPANY
    );
  }
  public function get_s_summary_total_leakages()
  {
    $qry = "
     SELECT COUNT(METAVALUES.id_metavalue) AS cuenta 
      FROM METAVALUES
     WHERE weigth = $this->severity_leakage
      AND id_analysis = $this->id_analysis
      AND is_hidden = 0
      AND is_hidden_by_user = 0";

    return $this->db->query($qry)->getRow()->cuenta;
  }
  public function get_s_summary_total_documents()
  {
    $qry = "SELECT COUNT(DISTINCT DOCUMENTS.id_document) AS cuenta 
                        FROM DOCUMENTS
                        JOIN REL_DOCUMENTS_DOMAINS ON REL_DOCUMENTS_DOMAINS.id_document = DOCUMENTS.id_document
                        JOIN DOMAINS ON REL_DOCUMENTS_DOMAINS.id_domain = DOMAINS.id_domain
                        WHERE DOMAINS.id_analysis = $this->id_analysis";
    return $this->db->query($qry)->getRow()->cuenta;
    /* quizá el query correcto sea el siguiente:
    SELECT COUNT(distinct id_document) AS cuenta 
                        FROM METAVALUES
                        WHERE id_analysis = 16
    uno da 4235 filas y el otro 31758
          */
  }
  public function get_s_summary_total_domains()
  {
    $qry = "SELECT COUNT(*) AS cuenta
                        FROM  DOMAINS 
                        WHERE id_analysis = $this->id_analysis";
    return $this->db->query($qry)->getRow()->cuenta;
  }
  public function get_s_summary_total_time()
  {
    $qry      = "SELECT date_ended - date_started as cuenta
            FROM ANALYSIS
            WHERE id_analysis = $this->id_analysis";
    $time     = $this->db->query($qry)->getRow()->cuenta;
    $horas    = intdiv($time, 3600);
    $time     = $time % 3600;
    $minutos  = intdiv($time, 60);

    return "$horas h $minutos m";
  }
  ///////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////
  public function get_s_leakages()
  {
    return array(
      "g121" => $this->get_s_leakages_g121(),
      "g122" => $this->get_s_leakages_g122(),
      "table" => $this->get_s_leakages_table(),
    );
  }
  public function get_s_leakages_g121()
  {
    // Eliminar distinct aumentaria la velocidad de la consulta
    $qry = "SELECT COUNT( DISTINCT id_metavalue) AS 'data',
              value as 'labels' 
            FROM METAVALUES 
            WHERE METAVALUES.weigth = $this->severity_leakage 
              AND METAVALUES.id_analysis = $this->id_analysis
              AND is_hidden = 0
              AND is_hidden_by_user = 0
            GROUP BY METAVALUES.value 
            ORDER BY data DESC";


    return $this->format_data_label($this->db->query($qry)->getResultArray(), 15);
  }
  public function get_s_leakages_g122()
  {

    $qry = " SELECT COUNT( DISTINCT METAVALUES.id_metavalue) AS 'data',
                METAVALUES.value as 'labels' 
              FROM METAVALUES 
              WHERE METAVALUES.weigth = $this->severity_leakage 
              AND METAVALUES.id_analysis = $this->id_analysis
              AND is_hidden = 0
              AND is_hidden_by_user = 0
              GROUP BY value 
              ORDER BY data DESC";
    $results = $this->format_data_label($this->db->query($qry)->getResultArray(), 5);

    return $results;
  }
  public function get_s_leakages_table($pag)
  {
    $page = $pag[0];
    $itemsPerPage = $pag[1];
    $LIMIT = $page == 0 ? '' : "LIMIT $itemsPerPage OFFSET " . ($page-1) * $itemsPerPage;
    $LIMIT = $itemsPerPage == 0 ? '' : $LIMIT;
    $qry = "SELECT value AS metadata,
            url, 
            filetype,
            metaseverity_name AS severity,
            COUNT(id_metavalue) AS metadata_count
     FROM METAVALUES
     WHERE weigth = $this->severity_leakage
      AND id_analysis = $this->id_analysis
      AND is_hidden = 0
      AND is_hidden_by_user = 0
     GROUP BY metadata
     ORDER BY metadata_count DESC
                  $LIMIT";
    $results = $this->db->query($qry)->getResultArray();
    // LIMIT $this->limit_preview";

    $i = 0;
    foreach ($results as &$current) {
      $results[$i]["metadata"] = urldecode($current['metadata']);
      $results[$i]["url"] = urldecode($current['url']);
      $i++;
    }

    return $results;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////
  public function get_s_documents()
  {

    return array(
      "g121" => $this->get_s_documents_g121(),
      "g122" => $this->get_s_documents_g122(),
      "table" => $this->get_s_documents_table(0, 0),
    );
  }
  public function get_s_documents_g121()
  {
    $qry = "SELECT 
              METAVALUES.filetype AS 'labels', 
              COUNT(METAVALUES.id_metavalue) AS 'data'
            FROM METAVALUES
            WHERE id_analysis = $this->id_analysis
              AND (METAVALUES.weigth = $this->severity_leakage
                    OR METAVALUES.weigth = $this->severity_sensible 
                    OR METAVALUES.weigth = $this->severity_style)
              AND is_hidden = 0
              AND is_hidden_by_user = 0
            GROUP BY METAVALUES.filetype
            ORDER BY data DESC";

    $raw = $this->db->query($qry)->getResultArray();
    return $this->format_data_label($raw, 10);
  }
  public function get_s_documents_g122()
  {
    $qry = "
     SELECT COUNT(id_metavalue) AS 'data', filetype AS 'labels',
     metaseverity_name AS 'severity', weigth AS 'weight'
      FROM METAVALUES
    WHERE id_analysis = $this->id_analysis
     AND (weigth = $this->severity_sensible 
          OR weigth = $this->severity_leakage)
     AND is_hidden = 0
     AND is_hidden_by_user = 0
    GROUP BY labels, severity";
    $raw = $this->db->query($qry)->getResultArray();
    return $this->format_s_documents_g122($raw);
  }
  private function format_s_documents_g122($raw)
  {
    $formated = array();
    $dataset  = array(
      "position_style"    => 1,
      "position_sensible" => 3,
      "position_leakage"  => 5,
      "total_style"       => 0,
      "total_sensible"    => 0,
      "total_leakage"     => 0,
      "position"          => 0
    );
    $i        = 0;
    foreach ($raw as $data) {
      // Check if "labels" is a key in formated array.
      // If not:
      //    Create it and initialize with "dataset"
      if (!array_key_exists($data["labels"], $formated)) {
        $i++;
        $formated[$data["labels"]]             = $dataset;
        $formated[$data["labels"]]["position"] = $i; // x axis position for "label"
      }

      if ($data["weight"] == $this->severity_style) {
        $formated[$data["labels"]]["total_style"] = $data["data"];
      }
      if ($data["weight"] == $this->severity_sensible) {
        $formated[$data["labels"]]["total_sensible"] = $data["data"];
      }
      if ($data["weight"] == $this->severity_leakage) {
        $formated[$data["labels"]]["total_leakage"] = $data["data"];
      }
    }

    return $formated;
  }
  public function get_s_documents_table($pag)
  {
    $page = $pag[0];
    $itemsPerPage = $pag[1];
    $LIMIT = $page == 0 ? '' : "LIMIT $itemsPerPage OFFSET " . ($page-1) * $itemsPerPage;
    $LIMIT = $itemsPerPage == 0 ? '' : $LIMIT;
    $qry = "SELECT value AS metadata, 
                url, 
                filetype, 
                domain, 
                metaseverity_name AS severity,
                COUNT(id_metavalue) AS metadata_total
              FROM METAVALUES
          WHERE (weigth = $this->severity_sensible 
                OR weigth = $this->severity_leakage)
          AND id_analysis = $this->id_analysis
          AND is_hidden = 0
          AND is_hidden_by_user = 0
          GROUP BY metadata
          ORDER BY metadata_total DESC 
          $LIMIT";
    $results = $this->db->query($qry)->getResultArray();
    $i = 0;
    foreach ($results as &$current) {
      $results[$i]['url']      = urldecode($current['url']);
      $results[$i]['document'] = substr(strrchr($current['url'], '/'), 1);
      $results[$i]['metadata'] = urldecode($current['metadata']);
      $i++;
    }
    return $results;
  }
  /////////////////////////////////////////////////////////////////
  /////// TO BE DELETED
  /////////////////////////////////////////////////////////////////
  private function pofe_format_s_domains_g121($raw)
  {
    //var_dump($raw);
    //die();
    $ret   = array(
      'labels'   => array(),
      'ids'      => array(),
      'datasets' => array(
        array(
          "label"           => "Sensibles",
          "backgroundColor" => "#ffeebd99",
          "data"            => array()
        ),
        array(
          "label"           => "Filtraciones",
          "backgroundColor" => "#f7c6cbBB",
          "data"            => array()
        ),
      )
    ); // inicializamos el array para ser devuelto
    $index = 0;                          // contador de elementos del array
    foreach ($raw as $r)                 // Viene ordenado por dominios
    {
      if (in_array($r['id'], $ret['ids']))  // el  dominio ya ha sido introducido.
      {
        // Vamos a meter la segunda columna de valores de un dominio que acabamos de introducir 
        // Recuerdese que $raw viene ordenado por dominios
        $ret['datasets'][1]['data'][$index - 1] = $r['data'];  // ponemos el numero de ocurrencias en el array de 'Filtraciones'
      } else        // es la primera vez que vemos el dominio
      {
        $ret['labels'][$index] = $r['domain']; // añadimos la etiqueta
        $ret['ids'][$index]    = $r['id'];        // añadimos el id
        if ($r['weigth'] == $this->severity_sensible)  // sensibles
        {
          $ret['datasets'][0]['data'][$index] = $r['data'];  // ponemos el numero de ocurrencias en el array de 'sensibles'
          //$ret['datasets'][1]['data'][$index] = 0;   // y ponemos el de filtraciones a cero por si no aparece
        } else  // Filtraciones
        {
          //$ret['datasets'][0]['data'][$index] = 0;    // ponemos el de sensibles a cero por no ha aparecido
          $ret['datasets'][1]['data'][$index] = $r['data'];  // ponemos el numero de ocurrencias en el array de 'Filtraciones'
        }
        $index++; // incrementamos el puntero solo en el caso de que se trataba de un nuevo dominio 
      }
    }

    //var_dump($ret);
    //die();
    return $ret;
  }
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  public function get_s_domains()
  {

    return array(
      "g121" => $this->get_s_domains_g121(),
      "g122" => $this->get_s_domains_g122(),
      "table" => $this->get_s_domains_table(),
    );
  }
  public function get_s_domains_g121()
  {
    $qry = "
     SELECT COUNT(id_metavalue) AS 'data',
       domain, weigth, metaseverity_name, id_domain as id
      FROM METAVALUES
     WHERE id_analysis = $this->id_analysis
      AND (weigth = $this->severity_leakage
           OR weigth = $this->severity_sensible)
      AND is_hidden = 0
      AND is_hidden_by_user = 0
     GROUP BY weigth, domain
     ORDER BY domain, weigth DESC";
    // echo $qry;
    $raw = $this->db->query($qry)->getResultArray();

    return $this->format_s_domains_g121($raw);
  }
  private function format_s_domains_g121($raw)
  {
    //var_dump($raw);

    $tmp = array();
    $formatted = array();

    foreach ($raw as $current) {
      $tmp["meta"] = $current["domain"];
      $tmp["metaseverity_name"] = $current["metaseverity_name"];
      $tmp["value"] = $current["data"];
      array_push($formatted, $tmp);
    }
    //var_dump($formatted);
    //die();
    return $formatted;
  }
  private function format_s_domains_g121_legacy($raw)
  {
    var_dump($raw);

    $formated   = array(
      'labels'   => array(),
      'ids'      => array(),
      'datasets' => array(
        array(
          "label"           => "Sensibles",
          "backgroundColor" => "#ffeebd",
          "data"            => array()
        ),
        array(
          "label"           => "Filtraciones",
          "backgroundColor" => "#f7c6cb",
          "data"            => array()
        ),
      )
    );
    $formated_pre = array();
    $domain_tmp = array(
      "domain_name" => "",
      $this->severity_style => 0,
      $this->severity_leakage => 0,
    );
    /*
      organizar array en la forma: 
        DOMINIO => 
              LEAKAGES => n
              SENSIBLES => m
    */
    foreach ($raw as $current) {
      // Si es la primera vez que aparece un dominio, incluirlo en pre_formated e inicializar.
      if (!in_array($current['id'], $formated['ids'])) {
        array_push($formated['ids'], $current['id']);
        $formated_pre[$current["domain"]] =  $domain_tmp;
      };

      $formated_pre[$current["domain"]]["domain_name"] = $current["domain"];
      $formated_pre[$current["domain"]][$current["weigth"]] = (int) $current["data"];
    }

    /**
     * Calcular % y poner array en la forma:
     * $formated
     */
    foreach ($formated_pre as $current) {

      // Si se ha recuperado NULL significa que no hay nada en BBDD.
      if (!isset($current[$this->severity_sensible])) {
        $current[$this->severity_sensible] = 0;
      }
      // Si se ha recuperado NULL significa que no hay nada en BBDD.
      if (!isset($current[$this->severity_leakage])) {
        $current[$this->severity_leakage] = 0;
      }

      $total = $current[$this->severity_leakage] + $current[$this->severity_sensible];

      if ($total == 0) {
        continue;
      }

      $current[$this->severity_sensible] = round($current[$this->severity_sensible] * 100 / $total);
      $current[$this->severity_leakage] = round($current[$this->severity_leakage] * 100 / $total);
      array_push($formated["labels"], $current["domain_name"]);
      array_push($formated["datasets"][0]["data"], $current[$this->severity_sensible]);
      array_push($formated["datasets"][1]["data"], $current[$this->severity_leakage]);
    }
    var_dump($formated);
    die();
    return $formated;
  }
  public function get_s_domains_g122()
  {
    $qry = "SELECT 
              DISTINCT GEO_CITIES.name,
              GEO_CITIES.latitude,
              GEO_CITIES.longitude
            FROM GEO_CITIES
            JOIN DOMAINS ON GEO_CITIES.id_city = DOMAINS.id_city
            WHERE DOMAINS.id_analysis = $this->id_analysis
            ORDER BY GEO_CITIES.name DESC";
    return $this->db->query($qry)->getResultArray();
  }
  public function get_s_domains_table($pag)
  {
    $page = $pag[0];
    $itemsPerPage = $pag[1];
    $LIMIT = $page == 0 ? '' : "LIMIT $itemsPerPage OFFSET " . ($page-1) * $itemsPerPage;
    $LIMIT = $itemsPerPage == 0 ? '' : $LIMIT;
    $qry = "SELECT METAVALUES.value, 
             METAVALUES.domain, 
              METAVALUES.metaseverity_name,
              METAVALUES.city_name 
            FROM METAVALUES 
            WHERE METAVALUES.id_analysis = $this->id_analysis
            AND METAVALUES.weigth = $this->severity_leakage
            AND is_hidden = 0
            AND is_hidden_by_user = 0
            ORDER BY METAVALUES.weigth DESC
            $LIMIT";
    $raw = $this->db->query($qry)->getResultArray();

    $i = 0;
    foreach ($raw as $current) {
      $raw[$i]["value"] = urldecode($current["value"]);
      $i++;
    }

    return $raw;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////
  public function get_company_name()
  {
    $qry = "SELECT name_comercial 
                        FROM COMPANIES
                        JOIN DOMAINS ON (DOMAINS.id_company = COMPANIES.id_company)
                        JOIN ANALYSIS ON (ANALYSIS.id_domain = DOMAINS.id_domain)
                        WHERE ANALYSIS.id_analysis = $this->id_analysis";
    return $this->db->query($qry)->getRow()->name_comercial;
  }
  public function get_domain_seed()
  {
    $id_company = $this->getCompanyId();

    if (is_null($id_company)) {
      return "";
    }

    $qry        = "SELECT  domain
                        FROM DOMAINS
                        JOIN ANALYSIS ON (ANALYSIS.id_domain = DOMAINS.id_domain)
                        WHERE DOMAINS.id_analysis IS NULL 
                        AND DOMAINS.id_company = $id_company";
    return $this->db->query($qry)->getRow()->domain;
  }
  private function getCompanyId()
  {
    $qry = "SELECT  COMPANIES.id_company
                        FROM COMPANIES
                        JOIN DOMAINS ON (DOMAINS.id_company = COMPANIES.id_company)
                        JOIN ANALYSIS ON (ANALYSIS.id_analysis = DOMAINS.id_analysis)
                        WHERE ANALYSIS.id_analysis = $this->id_analysis";
    return $this->db->query($qry)->getRow()->id_company;
  }
  public function get_s_raw_table($pag)
  {
    $page = $pag[0];
    $itemsPerPage = $pag[1];
    $LIMIT = $page == 0 ? '' : "LIMIT $itemsPerPage OFFSET " . ($page-1) * $itemsPerPage;
    $LIMIT = $itemsPerPage == 0 ? '' : $LIMIT;
   
    $qry = "SELECT 
              *
          FROM METAVALUES
          WHERE weigth = $this->severity_leakage 
          AND id_analysis = $this->id_analysis 
          AND is_hidden = 0 
          AND is_hidden_by_user = 0
          ORDER BY value ASC
          $LIMIT";

    $results = $this->db->query($qry)->getResultArray();

    $i = 0;
    foreach ($results as $result) {

      $results[$i]['value'] = urldecode($result['value']);
      $results[$i]['url'] = urldecode($result['url']);
      $results[$i]['domain'] = $result['domain'];
      $results[$i]['filetype'] = $result['filetype'];
      $results[$i]['metaseverity_name'] = $result['metaseverity_name'];
      $results[$i]['city_name'] = $result['city_name'];

      $i++;
    }
    return $results;
  }
  //////////////////////////////////////////////////////////////////////////////
  /////////   TOOLS
  // JAVI
  /**
   Dado un array de la forma:
   $raw   array(
   array(
   "key1" => int1,
   "key2" => Str1,
   ),
   array(
   "key1" => int1,
   "key2" => Str1,
   )
   )
   Lo formatea de como:
   array(
   "data" => array(int1, int2,...),
   "labels" => array(Str1, Str2,...),
   )
   @param $bucket_size
   * */
  private function format_data_label(
    $raw,
    $bucket_size = 0,
    $bucket_name = "Otros",
    $key_data = "data",
    $key_labels = "labels",
    $empty_label_value = "Vacio",
    $empty_value_value = 0,
    $label_length = 20
  ) {

    if (count($raw) == 0) // el array de datos viene vacio
      return [
        "data"   => [0],
        "labels" => ['-']
      ];
    $formated = array(
      $key_data   => array(),
      $key_labels => array(),
    );

    for ($i = 0; $i < count($raw); $i++) {

      if ($i >= $bucket_size and $bucket_size != 0) {
        $formated[$key_data][$bucket_size] = (isset($formated[$key_data][$bucket_size]) ? $formated[$key_data][$bucket_size] + $raw[$i][$key_data] : $raw[$i][$key_data]);
        $formated[$key_labels][$bucket_size] = $bucket_name;

        continue;
      }
      $formated[$key_data][$i] = (int) $raw[$i][$key_data];

      // Check if Label is too long. Cut it to "label_length" and add "..."
      $raw[$i][$key_labels] = strlen($raw[$i][$key_labels]) > $label_length ?
        substr($raw[$i][$key_labels], 0, $label_length) . "..." :
        $raw[$i][$key_labels];

      $formated[$key_labels][$i] = urldecode($raw[$i][$key_labels]);
    }

    if (empty($formated[$key_data])) {
      $formated[$key_data] = $empty_value_value;
    };
    if (empty($formated[$key_labels])) {
      $formated[$key_labels] = $empty_label_value;
    }

    return $formated;
  }
}
