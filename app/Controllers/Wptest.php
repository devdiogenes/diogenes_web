<?php

namespace App\Controllers;

class Wptest extends BaseControllerApi
{

  public $permitido = [1, 2, 4, 8, 16, 256]; // permitidos todos los niveles de seguridad
  private $path_internal = "/var/www/html/diogenes_dev/public/wptest/";

  public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
  {
    parent::initController($request, $response, $logger);

    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
  }

  public function index()
  {
    echo "Hola";
    $incoming_url = $this->request->getPost("url");

    if ($incoming_url == '') {
      return $this->send_response("No URL provided... =( || Provide a POST URL with name ['url']");

    }

    $myFile = file_get_contents($incoming_url);

    $info = explode("/", $incoming_url);

    $original_filename = end($info);
    $tnow =  date("dmyhms");
    
    $tmp_filename = $tnow."-".$original_filename;
    
    $n = file_put_contents($this->path_internal.$tmp_filename, $myFile, FILE_USE_INCLUDE_PATH);
    if (!$n){
      $this->send_response("ERROR TO WRITE");
    }
    
    $path_external = base_url() . "/wptest/".$tmp_filename;
    
    $response = array();
    $response["url"] = $path_external;
    $response["filename_tmp"] = $tmp_filename;
    $response["original_filename"] = $original_filename;
    $response["original_url"] = $incoming_url;
    
    
    
    $this->send_response($response);
  }

  private function send_response($response){
    echo json_encode($response);
    die();
  }

  public function readFileUrl(){
    $input_url = $this->request->getPost("url");
    $input_headers = $this->request->getHeaders();

    //echo $input_url;
    //var_dump($input_headers);
    if ($input_url == ''){
      echo "Did you add the URL? =)";
      die();
    }

    $info = explode("/", $input_url);
    $name = end($info);
    $tnow =  date("dmyhms");
    $tmp_filename = $tnow."-".$name;

    $input_file = file_get_contents($input_url);
    file_put_contents($this->path_internal.$tmp_filename, $input_file);

    echo base_url()."/wptest/".$tmp_filename;
    //echo $this->path_internal.$name;
    die();

  }
}