define({ "api": [
  {
    "type": "post",
    "url": "/AAddCompany",
    "title": "AAddCompany",
    "name": "AddCompany",
    "group": "Acción",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "32",
            "optional": false,
            "field": "company_name",
            "description": "<p>Nombre para identificar a la compañía</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "64",
            "optional": false,
            "field": "company_domain",
            "description": "<p>Dominio a analizar</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "32",
            "optional": true,
            "field": "company_email",
            "description": "<p>Email de contacto de la compañía</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "32",
            "optional": true,
            "field": "company_contact",
            "description": "<p>Persona de contacto</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "32",
            "optional": true,
            "field": "company_phone",
            "description": "<p>Telefono de contacto</p>"
          }
        ],
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n   company_name : \"Monsters Inc\",\n   company_domain : \"https://www.pixar.com\",\n   company_email  : \"JamesPSullivan@MostersInc.com\",\n   company_contact : \"James P. Sullivan\",\n   phone : \"555-468-789\",\n   token : \"6fc75ac20a807a7e0c0814a5215af250\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "payload",
            "description": "<p>Obj con los detalles de la empresa añadida</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   dataUser : {...},\n   dataCustomer :{...},\n   dataWorkingCenter : {...},\n   payload : [ \n       {\n         name_comercial: \"Monsters Inc.\"\n         domain: \"https://www.pixar.com\"\n         date_requested: \"2020-05-19 00:25:54\"\n         status: \"ready\"\n         id_analysis: 480\n         id_customer: 35\n         id_domain: 2289\n         launchable: true\n        }\n      ]\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "400",
            "description": "<p>badRequest</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "401",
            "description": "<p>Acceso restringido a $id_analysis</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n    payload : [\n          {\n             code : 400\n             error : badRequest\n             details : \"Alguno de los parámetros nos está bien formateado\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "401",
          "content": "{\n    payload : [\n          {\n             code : 401\n             error : Unauthorized,\n             details : \"Acceso restringido a Verics Dashboard de $id_customer\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n     payload : [\n          {\n             code : 499\n             error : invalidToken,\n             details : \"Token no valido\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Acción"
  },
  {
    "type": "post",
    "url": "/ALogin",
    "title": "Login",
    "name": "Login",
    "group": "Identificacion",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": false,
            "field": "token",
            "description": "<p>Token de identificación único</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    token : \"6fc75ac20a807a7e0c0814a5215af250\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "dataUser",
            "description": "<p>Información relativa al usuario</p>"
          },
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "dataCustomer",
            "description": "<p>Información relativa al cliente</p>"
          },
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "dataWorkingCenter",
            "description": "<p>Información relativa al centro de trabajo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n       dataUser: {\n           id_user : 31,\n           name : \"James P. Sullivan\",\n           email : \"JamesSullivan@MonstersInc.com\",\n           date_created : \"2020-11-09 03:50:45\"\n         },\n        dataCustomer: {\n           id_customer : 59,\n           id_user : 31,\n           id_priority : 3,\n           date_added : \"2020-09-10 11:19:32\",\n           name_comercial : \"Monsters\",\n           name_legal : \"Monsters Inc.\",\n           address : \"Monstropolis address\",\n           id_city : 15,\n           contact : \"James P. Sullivan\",\n           phone : \"555-489-789\",\n           email : info@MostersInc.com,\n           web : \"https://www.pixar.com\",\n           coment : \"¡Que gran película!\"\n         },\n        dataWorkingCenter: {\n           id_working_center : 7,\n           name : \"Headquartes Monsters Inc.\",\n           parent : -1,\n           id_customer : 59,\n           facturable : 1\n         },\n      payload : []\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "404",
            "description": "<p>NotFount</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "404",
          "content": "{\n     payload : [\n          {\n             code : 404\n             error : notFound,\n             details : \"Token not found\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n     payload : [\n          {\n             code : 499\n             error : invalidToken,\n             details : \"Token no valido\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Identificacion"
  },
  {
    "type": "post",
    "url": "/ALogout",
    "title": "Logout",
    "name": "Logout",
    "group": "Identificacion",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": false,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    token : \"6fc75ac20a807a7e0c0814a5215af250\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "success",
            "description": "<p>Logged out</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   payload : [ \n         {\n             code : 200,\n             details: \"Logged out\"\n         }\n        ]\n}",
          "type": "Obj"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Identificacion"
  },
  {
    "type": "POST",
    "url": "/VRDocuments_Cloud",
    "title": "Documents_Cloud",
    "name": "RDocuments_Cloud",
    "group": "Verics_Report",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id_analysis",
            "description": "<p>Identificador único de un análisis</p>"
          }
        ],
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    id_analysis : 9875,\n    token : \"6fc75ac20a807a7e0c0814a5215af250\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "payload",
            "description": "<p>Tipos de archivos con el número de filtraciones y datos sensibles encontrados</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   dataUser : {...},\n   dataCustomer :{...},\n   dataWorkingCenter : {...},\n   payload : [\n             { \n                 DOC : {\n                     position_style : 1,\n                     position_sensible : 3,\n                     position_leakage : 5,\n                     total_style : 0,\n                     total_sensible : 0,\n                     total_leakage : 273,\n                     position : 1 \n                 },\n                 PDF : { \n                         position_style :1,\n                         position_sensible : 3,\n                         position_leakage : 5,\n                         total_style : 0,\n                         total_sensible : 0,\n                         total_leakage : 129 ,\n                         position : 2\n                     },\n                 ... : {...}\n            }\n       ]\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "401",
            "description": "<p>Acceso restringido a $id_analysis</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "404",
            "description": "<p>$id_analysis no entontrado</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n    payload : [\n          {\n             code : 401\n             error : Unauthorized,\n             details : \"Acceso restringido a Verics Dashboard de $id_customer\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "404",
          "content": "{\n    payload : [\n          {\n             code : 404\n             error : NotFound,\n             details : \"$id_analysis no encontrado\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n     payload : [\n          {\n             code : 499\n             error : invalidToken,\n             details : \"Token no valido\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Verics_Report"
  },
  {
    "type": "POST",
    "url": "/VRDocuments_Equalizer",
    "title": "Documents_Equalizer",
    "name": "RDocuments_Equalizer",
    "group": "Verics_Report",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id_analysis",
            "description": "<p>Identificador único de un análisis</p>"
          }
        ],
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    id_analysis : 9875,\n    token : \"6fc75ac20a807a7e0c0814a5215af250\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "payload",
            "description": "<p>Número de filtraciones y datos sensibles encontrados en cada tipo de archivos</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   dataUser : {...},\n   dataCustomer :{...},\n   dataWorkingCenter : {...},\n   payload : [\n         { \n           data: [243980, 34794, 15537, 2779, 838, 515, 404, 345, 263, 249, 242],\n           labels\":[\"JPG\", \"PDF\",\"PNG\", \"GIF\", \"DOC\", \"XLSX\", \"HTML\", \"DOCX\", \"MP4\", \"XLS\", \"Otros\"]\n         }\n     ]\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "401",
            "description": "<p>Acceso restringido a $id_analysis</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "404",
            "description": "<p>$id_analysis no entontrado</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n    payload : [\n          {\n             code : 401\n             error : Unauthorized,\n             details : \"Acceso restringido a Verics Dashboard de $id_customer\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "404",
          "content": "{\n    payload : [\n          {\n             code : 404\n             error : NotFound,\n             details : \"$id_analysis no encontrado\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n     payload : [\n          {\n             code : 499\n             error : invalidToken,\n             details : \"Token no valido\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Verics_Report"
  },
  {
    "type": "POST",
    "url": "/VRDocuments_Table",
    "title": "Documents_Table",
    "name": "RDocuments_Table",
    "group": "Verics_Report",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id_analysis",
            "description": "<p>Identificador único de un análisis</p>"
          }
        ],
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ],
        "Control": [
          {
            "group": "Control",
            "type": "int",
            "optional": true,
            "field": "page",
            "description": "<p>Página de la tabla a visualizar</p>"
          },
          {
            "group": "Control",
            "type": "int",
            "optional": true,
            "field": "itemsPerPage",
            "description": "<p>Número de entradas en la tabla</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    id_analysis : 9875,\n    token : \"6fc75ac20a807a7e0c0814a5215af250\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "payload",
            "description": "<p>Información sobre el archivo afectado y el dominio al que pertenece</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   dataUser : {...},\n   dataCustomer :{...},\n   dataWorkingCenter : {...},\n   payload : [\n             {\n                 metadata\":\"Mike Wazowski\",\n                 url\":\"https://www.pixar.com/myDocument.JPG\",\n                 filetype\":\"JPG\",\n                 domain\":\"https://www.pixar.com\",\n                 severity\":\"Leakage\",\n                 metadata_total\":\"1639\",\n                 document\":\"myDocument.jpg\"\n               },\n             {\n                 metadata\":\"James P. Sullivan\",\n                 url\":\"https://www.pixar.com/myOtherDocument.PDF\",\n                 filetype\":\"PDF\",\n                 domain\":\"https://www.pixar.com\",\n                 severity\":\"Leakage\",\n                 metadata_total\":\"1639\",\n                 document\":\"myOtherDocument.pdf\"\n             },\n             {...}\n          ]\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "401",
            "description": "<p>Acceso restringido a $id_analysis</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "404",
            "description": "<p>$id_analysis no entontrado</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n    payload : [\n          {\n             code : 401\n             error : Unauthorized,\n             details : \"Acceso restringido a Verics Dashboard de $id_customer\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "404",
          "content": "{\n    payload : [\n          {\n             code : 404\n             error : NotFound,\n             details : \"$id_analysis no encontrado\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n     payload : [\n          {\n             code : 499\n             error : invalidToken,\n             details : \"Token no valido\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Verics_Report"
  },
  {
    "type": "POST",
    "url": "/VRDomains_Equalizer",
    "title": "Domains_Equalizer",
    "name": "RDomains_Equalizer",
    "group": "Verics_Report",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id_analysis",
            "description": "<p>Identificador único de un análisis</p>"
          }
        ],
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    id_analysis : 9875,\n    token : \"6fc75ac20a807a7e0c0814a5215af250\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "payload",
            "description": "<p>Número de filtraciones y datos sensibles encontrados en cada dominio y subdominio</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{    \n   dataUser : {...},\n   dataCustomer :{...},\n   dataWorkingCenter : {...},\n   payload: [\n         {\n             meta : \"https://www.pixar.com\",\n             metaseverity_name :\"Filtracion\",\n             value : 43\n         },\n         {\n             meta : \"https://www.pixar.com,\n             metaseverity_name : \"Sensible\",\n             value : 157\n         },\n         {...}\n    ]\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "401",
            "description": "<p>Acceso restringido a $id_analysis</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "404",
            "description": "<p>$id_analysis no entontrado</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n    payload : [\n          {\n             code : 401\n             error : Unauthorized,\n             details : \"Acceso restringido a Verics Dashboard de $id_customer\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "404",
          "content": "{\n    payload : [\n          {\n             code : 404\n             error : NotFound,\n             details : \"$id_analysis no encontrado\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n     payload : [\n          {\n             code : 499\n             error : invalidToken,\n             details : \"Token no valido\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Verics_Report"
  },
  {
    "type": "POST",
    "url": "/VRDomains_Map",
    "title": "Domains_Map",
    "name": "RDomains_Map",
    "group": "Verics_Report",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id_analysis",
            "description": "<p>Identificador único de un análisis</p>"
          }
        ],
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    id_analysis : 9875,\n    token : \"6fc75ac20a807a7e0c0814a5215af250\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "payload",
            "description": "<p>Coordenadas y nombre de la ciduad donde se han encontrado dominios/subdominios que presentan filtraciones</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   dataUser : {...},\n   dataCustomer :{...},\n   dataWorkingCenter : {...},\n   payload: [\n         {\n             name : \"Belfast\",\n             latitude : \"54.5973\",\n             longitude\":\"5.9301\"\n         },\n         {\n             name : \"Albacete\",\n             latitude : \"38.9943\",\n             longitude\":\"1.8585\"\n         },\n         {\n             name : \"Madrid\",\n             latitude : \"40.4168\",\n             longitude\":\"3.7038\"\n         },\n]\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "401",
            "description": "<p>Acceso restringido a $id_analysis</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "404",
            "description": "<p>$id_analysis no entontrado</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n    payload : [\n          {\n             code : 401\n             error : Unauthorized,\n             details : \"Acceso restringido a Verics Dashboard de $id_customer\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "404",
          "content": "{\n    payload : [\n          {\n             code : 404\n             error : NotFound,\n             details : \"$id_analysis no encontrado\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n     payload : [\n          {\n             code : 499\n             error : invalidToken,\n             details : \"Token no valido\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Verics_Report"
  },
  {
    "type": "POST",
    "url": "/VRDomains_Table",
    "title": "Domains_Table",
    "name": "RDomains_Table",
    "group": "Verics_Report",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id_analysis",
            "description": "<p>Identificador único de un análisis</p>"
          }
        ],
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ],
        "Control": [
          {
            "group": "Control",
            "type": "int",
            "optional": true,
            "field": "page",
            "description": "<p>Página de la tabla a visualizar</p>"
          },
          {
            "group": "Control",
            "type": "int",
            "optional": true,
            "field": "itemsPerPage",
            "description": "<p>Número de entradas en la tabla</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    id_analysis : 9875,\n    token : \"6fc75ac20a807a7e0c0814a5215af250\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "payload",
            "description": "<p>Información sobre las filtraciones y datos sensibles encontrados en cada archivo de un dominio/subdominio</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   dataUser : {...},\n   dataCustomer :{...},\n   dataWorkingCenter : {...},\n   payload: [\n         {\n             value : \"Mike Wazowski\",\n             domain\": \"https://www.pixar.com\",\n             metaseverity_name : \"Filtracion\",\n             city_name : \"Mostropolis\"\n         },\n         {\n             value : \"James P Sullivan\",\n             domain\": \"https://www.pixar.com\",\n             metaseverity_name : \"Filtracion\",\n             city_name : \"Mostropolis\"\n         },\n         {...}\n     ]\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "401",
            "description": "<p>Acceso restringido a $id_analysis</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "404",
            "description": "<p>$id_analysis no entontrado</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n    payload : [\n          {\n             code : 401\n             error : Unauthorized,\n             details : \"Acceso restringido a Verics Dashboard de $id_customer\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "404",
          "content": "{\n    payload : [\n          {\n             code : 404\n             error : NotFound,\n             details : \"$id_analysis no encontrado\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n     payload : [\n          {\n             code : 499\n             error : invalidToken,\n             details : \"Token no valido\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Verics_Report"
  },
  {
    "type": "POST",
    "url": "/VRLeakages_Cheddar",
    "title": "Leakages_Cheddar",
    "name": "RLeakages_Cheddar",
    "group": "Verics_Report",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id_analysis",
            "description": "<p>Identificador único de un análisis</p>"
          }
        ],
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    id_analysis : 9875,\n    token : \"6fc75ac20a807a7e0c0814a5215af250\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "payload",
            "description": "<p>Distribución de las 5 filtraciones más frecuentes</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   dataUser : {...},\n   dataCustomer :{...},\n   dataWorkingCenter : {...},\n   payload: [\n             data : [386, 268, 202, 200, 134, 3920],\n             labels : [\"Mike\", \"Wazowski\", \"James\", \"Sullivan\", \"Robert\", \"Otros\"]\n     ]\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "401",
            "description": "<p>Acceso restringido a $id_analysis</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "404",
            "description": "<p>$id_analysis no entontrado</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n    payload : [\n          {\n             code : 401\n             error : Unauthorized,\n             details : \"Acceso restringido a Verics Dashboard de $id_customer\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "404",
          "content": "{\n    payload : [\n          {\n             code : 404\n             error : NotFound,\n             details : \"$id_analysis no encontrado\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n     payload : [\n          {\n             code : 499\n             error : invalidToken,\n             details : \"Token no valido\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Verics_Report"
  },
  {
    "type": "POST",
    "url": "/VRLeakages_Equalizer",
    "title": "Leakages_Equalizer",
    "name": "RLeakages_Equalizer",
    "group": "Verics_Report",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id_analysis",
            "description": "<p>Identificador único de un análisis</p>"
          }
        ],
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    id_analysis : 9875,\n    token : \"6fc75ac20a807a7e0c0814a5215af250\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "payload",
            "description": "<p>Numero de veces que aparecen las 5 filtraciones más frecuentes</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    dataUser : {...},\n    dataCustomer :{...},\n    dataWorkingCenter : {...},\n    payload : [\n      {\n         data : [386, 268, 589, 3920],\n         labels : [\"Mike Wazowski\", \"James P. Sullivan\", \"Randall Boggs\", \"Otros\"]\n      }\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "401",
            "description": "<p>Acceso restringido a $id_analysis</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "404",
            "description": "<p>$id_analysis no entontrado</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n    payload : [\n          {\n             code : 401\n             error : Unauthorized,\n             details : \"Acceso restringido a Verics Dashboard de $id_customer\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "404",
          "content": "{\n    payload : [\n          {\n             code : 404\n             error : NotFound,\n             details : \"$id_analysis no encontrado\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n     payload : [\n          {\n             code : 499\n             error : invalidToken,\n             details : \"Token no valido\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Verics_Report"
  },
  {
    "type": "POST",
    "url": "/VRLeakages_Table",
    "title": "Leakages_Table",
    "name": "RLeakages_Table",
    "group": "Verics_Report",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id_analysis",
            "description": "<p>Identificador único de un análisis</p>"
          }
        ],
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ],
        "Control": [
          {
            "group": "Control",
            "type": "int",
            "optional": true,
            "field": "page",
            "description": "<p>Página de la tabla a visualizar</p>"
          },
          {
            "group": "Control",
            "type": "int",
            "optional": true,
            "field": "itemsPerPage",
            "description": "<p>Número de entradas en la tabla</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    id_analysis : 9875,\n    token : \"6fc75ac20a807a7e0c0814a5215af250\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "payload",
            "description": "<p>Información sobre la filtración que se ha encontrado</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   dataUser : {...},\n   dataCustomer :{...},\n   dataWorkingCenter : {...},\n   payload: [\n         {\n          metadata : \"Mike\",\n          url : \"https://www.pixar.com/myDocument.JPG\",\n          filetype : \"JPG\",\n          severity : \"Filtracion\",\n          metadata_count :\"386\"\n         },\n     ]\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "401",
            "description": "<p>Acceso restringido a $id_analysis</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "404",
            "description": "<p>$id_analysis no entontrado</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n    payload : [\n          {\n             code : 401\n             error : Unauthorized,\n             details : \"Acceso restringido a Verics Dashboard de $id_customer\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "404",
          "content": "{\n    payload : [\n          {\n             code : 404\n             error : NotFound,\n             details : \"$id_analysis no encontrado\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n     payload : [\n          {\n             code : 499\n             error : invalidToken,\n             details : \"Token no valido\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Verics_Report"
  },
  {
    "type": "POST",
    "url": "/VRRaw_Table",
    "title": "Raw_Table",
    "name": "RRaw_Table",
    "group": "Verics_Report",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id_analysis",
            "description": "<p>Identificador único de un análisis</p>"
          }
        ],
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ],
        "Control": [
          {
            "group": "Control",
            "type": "int",
            "optional": true,
            "field": "page",
            "description": "<p>Página de la tabla a visualizar</p>"
          },
          {
            "group": "Control",
            "type": "int",
            "optional": true,
            "field": "itemsPerPage",
            "description": "<p>Número de entradas en la tabla</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    id_analysis : 9875,\n    token : \"6fc75ac20a807a7e0c0814a5215af250\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "payload",
            "description": "<p>Toda la información del análisis en bruto</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   dataUser : {...},\n   dataCustomer :{...},\n   dataWorkingCenter : {...},\n   payload: [\n     {\n          id_metavalue : 29793609,\n          id_document : 565484,\n          id_metatag : 10979,\n          value : \"James P. Sullivan\",\n          weigth : 150,\n          id_analysis : 187,\n          url : \"https://www.pixar.com\",\n          domain : \"https://SUBDOMAIN.pixar.com\",\n          filetype : \"JPG\",\n          metaseverity_name : \"Filtracion\",\n          id_domain : 2239,\n          id_city : 81,\n          city_name : \"Monstropolis\",\n          is_hidden : 0\n       },\n       {...}\n   ]\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "401",
            "description": "<p>Acceso restringido a $id_analysis</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "404",
            "description": "<p>$id_analysis no entontrado</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n    payload : [\n          {\n             code : 401\n             error : Unauthorized,\n             details : \"Acceso restringido a Verics Dashboard de $id_customer\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "404",
          "content": "{\n    payload : [\n          {\n             code : 404\n             error : NotFound,\n             details : \"$id_analysis no encontrado\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n     payload : [\n          {\n             code : 499\n             error : invalidToken,\n             details : \"Token no valido\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Verics_Report"
  },
  {
    "type": "POST",
    "url": "/VRSummary",
    "title": "Summary",
    "name": "RSummary",
    "group": "Verics_Report",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id_analysis",
            "description": "<p>Identificador único de un análisis</p>"
          }
        ],
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    id_analysis : 9875,\n    token : \"6fc75ac20a807a7e0c0814a5215af250\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "payload",
            "description": "<p>Resumen de los datos que se han encontrado en el análisis del dominio</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    dataUser : {...},\n    dataCustomer :{...},\n    dataWorkingCenter : {...},\n    payload : [\n         {\n              total_leakages : 5995,\n              total_time : \"25 h 40 m\",\n              total_subdomains : 840,\n              total_documents : 17109,\n              company_name : \"Monsters Inc.\",\n              company_domain : \"https://www.pixar.com\"\n           }\n     ]\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "401",
            "description": "<p>Acceso restringido a $id_analysis</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "404",
            "description": "<p>$id_analysis no entontrado</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Obj",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n    payload : [\n          {\n             code : 401\n             error : Unauthorized,\n             details : \"Acceso restringido a Verics Dashboard de $id_customer\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "404",
          "content": "{\n    payload : [\n          {\n             code : 404\n             error : NotFound,\n             details : \"$id_analysis no encontrado\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n     payload : [\n          {\n             code : 499\n             error : invalidToken,\n             details : \"Token no valido\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Verics_Report"
  },
  {
    "type": "POST",
    "url": "/VDCheddar",
    "title": "Cheddar",
    "name": "DCheddar",
    "group": "Verics_dashboard",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    token : \"69abd4abf577d7cfd6d370f146611fea\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "payload",
            "description": "<p>Datos y etiquetas para el gráfico Cheddar</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    dataUser : {...},\n    dataCustomer :{...},\n    dataWorkingCenter : {...},\n    payload : [\n         {\n              data : [\"5518\",\"4167\"],\n              labels : [\"Filtracion\",\"Sensible\"]\n         }\n       ]\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "int",
            "optional": false,
            "field": "404",
            "description": "<p>notFound</p>"
          },
          {
            "group": "Error 4xx",
            "type": "int",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "int",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "404",
          "content": "{\n    payload : [\n          {\n             code : 404\n             error : NotFound\n             details : Token no encontrado\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n    payload : [\n          {\n             code : 499\n             error : invalidToken\n             details : \"invalidToken\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Verics_dashboard"
  },
  {
    "type": "POST",
    "url": "/VDMap",
    "title": "Map",
    "name": "DMap",
    "group": "Verics_dashboard",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    token : \"69abd4abf577d7cfd6d370f146611fea\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "payload",
            "description": "<p>Datos y etiquetas para el mapa</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    dataUser : {...},\n    dataCustomer :{...},\n    dataWorkingCenter : {...},\n    payload : [\n         {\n           name : \"Diemen\"\n           latitude : 52.3091\n           longitude : 4.94019\n           domain : \"https://www.pixar.com\"\n         },\n         {\n            name : Alboraya\n            latitude : 39.4845,\n            longitude : -0.38072,\n            domain : https://www.dreamworks.com\n         },\n         {...}\n       ]\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "int",
            "optional": false,
            "field": "404",
            "description": "<p>notFound</p>"
          },
          {
            "group": "Error 4xx",
            "type": "int",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "int",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "404",
          "content": "{\n    payload : [\n          {\n             code : 404\n             error : NotFound\n             details : Token no encontrado\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n    payload : [\n          {\n             code : 499\n             error : invalidToken\n             details : \"invalidToken\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Verics_dashboard"
  },
  {
    "type": "POST",
    "url": "/VDSnake",
    "title": "Snake",
    "name": "DSnake",
    "group": "Verics_dashboard",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    token : \"69abd4abf577d7cfd6d370f146611fea\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Obj",
            "optional": false,
            "field": "payload",
            "description": "<p>Datos y etiquetas para el gráfico Snake</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    dataUser : {...},\n    dataCustomer :{...},\n    dataWorkingCenter : {...},\n    payload : [\n       {\n         data : [455,839,10765],\n         labels : [\"2020-08-04\",\"2020-08-05\",\"2020-08-12\"]\n        }\n       ]\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "int",
            "optional": false,
            "field": "404",
            "description": "<p>notFound</p>"
          },
          {
            "group": "Error 4xx",
            "type": "int",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "int",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "404",
          "content": "{\n    payload : [\n          {\n             code : 404\n             error : NotFound\n             details : Token no encontrado\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n    payload : [\n          {\n             code : 499\n             error : invalidToken\n             details : \"invalidToken\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Verics_dashboard"
  },
  {
    "type": "POST",
    "url": "/VDTable",
    "title": "Table",
    "name": "DTable",
    "group": "Verics_dashboard",
    "version": "2.0.0",
    "parameter": {
      "fields": {
        "Identificacion": [
          {
            "group": "Identificacion",
            "type": "string",
            "size": "32",
            "optional": true,
            "field": "token",
            "description": "<p>Token único de identificación del usuario</p>"
          }
        ],
        "Control": [
          {
            "group": "Control",
            "type": "int",
            "optional": true,
            "field": "page",
            "description": "<p>Página de la tabla a visualizar</p>"
          },
          {
            "group": "Control",
            "type": "int",
            "optional": true,
            "field": "itemsPerPage",
            "description": "<p>Número de entradas en la tabla</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    token : \"69abd4abf577d7cfd6d370f146611fea\"\n}",
          "type": "Obj"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "payload",
            "description": "<p>Array con los objetos que describen cada empresa</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    dataUser : {...},\n    dataCustomer :{...},\n    dataWorkingCenter : {...},\n    payload :[\n\t              {\n                name_comercial: \"Monsters S.A.\"\n                domain: \"https://www.pixar.com\"\n                date_requested: \"2020-05-19 00:25:54\"\n                status: \"ready\"\n                id_analysis: 480\n                id_customer: 35\n                id_domain: 2289\n                launchable: true\n               },\n               {\n                name_comercial: \"The Boss Baby\"\n                domain: \"https://www.dreamworks.com\"\n                date_requested: \"2019-09-21 16:37:10\"\n                status: \"ready\"\n                id_analysis: 87\n                id_customer: 35\n                id_domain: 1067\n                launchable: true\n               },\n\n             {...}\n      ]\n}",
          "type": "Obj"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "int",
            "optional": false,
            "field": "404",
            "description": "<p>notFound</p>"
          },
          {
            "group": "Error 4xx",
            "type": "int",
            "optional": false,
            "field": "498",
            "description": "<p>notActiveUser</p>"
          },
          {
            "group": "Error 4xx",
            "type": "int",
            "optional": false,
            "field": "499",
            "description": "<p>invalidToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "404",
          "content": "{\n    payload : [\n          {\n             code : 404\n             error : NotFound\n             details : Token no encontrado\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "498",
          "content": "{\n    payload : [\n          {\n             code : 498\n             error : notActiveUser\n             details : \"Usuario no activo\"\n           }\n         ]\n}",
          "type": "json"
        },
        {
          "title": "499",
          "content": "{\n    payload : [\n          {\n             code : 499\n             error : invalidToken\n             details : \"invalidToken\"\n           }\n         ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api2_doc.php",
    "groupTitle": "Verics_dashboard"
  }
] });
