<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard VERICS</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>


<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header" style="background-color:#3a8bba">
                <h3 class="card-title">Acciones</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <form action="/verics" method="POST">
                                <select class="form-control select2" style="width: 100%;" name="customer" onchange="this.form.submit()">
                                    <?php foreach ($s_internal["customer_dropdown"] as $option) : ?>)
                                    <option <?php echo ($option["selected"] === True ? "selected='selected'" : ""); ?> " value = <?php echo $option["id_customer"]; ?>>
                                            <?php echo $option["name_comercial"]; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <!-- <button type=" submit" class="btn btn-block btn-default">Seleccionar</button> -->
                            </form>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_company">
                            Añadir Compañía
                        </button>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="info-box">
                <div class="card-body">
                    <div class="chart">
                        <canvas id="g131" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info-box">
                <div class="card-body">
                    <div id="g132" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info-box">
                <div class="card-body">
                    <canvas id="g133" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
            </div>
        </div>
    </div>


    <div class="card">
        <div class="card-body">
            <table id="s_summary_table" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Compañía</th>
                        <th>Dominio</th>
                        <th>Fecha de solicitud</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($s_summary["table"] as $row) : ?>
                        <tr>
                            <td><?php echo $row["name_comercial"]; ?></td>
                            <td><a href="<?php echo $row["domain"]; ?>" target="_blank">
                                    <?php echo $row["domain"]; ?></a></td>
                            <td><?php echo $row["date_requested"]; ?></td>
                            <td>
                                <?php $id_analysis = $row["id_analysis"]; ?>
                                <?php echo ($row["status"] == "ready" ?
                                    " <a href='/verics/report/$id_analysis'>
                                                <small class='badge badge-success'><i class='fas fa-search'></i>
                                                    Ver informe
                                                </small></a>"
                                    :
                                    ""); ?>
                                <?php echo ($row["status"] == "working" ?
                                    "<small class='badge badge-warning'<i class='fas fa-cogs'></i></i>
                                                    Analizando
                                                </small>"
                                    :
                                    ""); ?>
                                <?php echo ($row["status"] == "queue" ?
                                    "<small class='badge badge-danger'><i class='fas fa-pause'></i>
                                                    En cola
                                                </small>"
                                    :
                                    ""); ?>
                            </td>
                            <td>
                                <?php $customer = $row["id_customer"]; ?>
                                <?php $domain = $row["id_domain"]; ?>
                                <?php echo ($row["launchable"] == True ?
                                    "<a href='/verics/launch/$customer/$domain'
                                                <small class='badge badge-info'><i class='fas fa-play'></i>
                                                    Analizar
                                                </small>"
                                    :
                                    ""); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>



                    <?= $table ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Compañía</th>
                        <th>Dominio</th>
                        <th>Fecha de solicitud</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    </div>
</section>

<div class="modal fade" id="add_company">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Añadir Compañía</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>/verics/add_customer">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="company_name" class="col-sm-2 col-form-label">Nombre</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="company_name" name="company_name" placeholder="¿Como se llama la empresa?">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="company_contact" class="col-sm-2 col-form-label">Contacto</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="company_contact" name="company_contact" placeholder="¿A quien conoces en la empresa?">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="company_email" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="company_email" name="company_email" placeholder="contacto@email.com">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="company_phone" class="col-sm-2 col-form-label">Teléfono</label>
                            <div class="col-sm-10">
                                <input type="tel" class="form-control" id="company_phone" name="company_phone" placeholder="555 469 458">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="company_domain" class="col-sm-2 col-form-label">Web</label>
                            <div class="col-sm-10">
                                <input type="url" class="form-control" id="company_domain" name="company_domain" placeholder="https://www.suments.com">
                            </div>
                        </div>
                        <?php foreach ($s_internal["customer_dropdown"] as $option) {
                            if ($option["selected"] === True) {
                                $id_customer = $option["id_customer"];
                                break;
                            }
                        } ?>
                        <input type="hidden" id="id_customer" name="id_customer" value="<?php echo $id_customer;?>">

                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Añadir</button>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->




<script src="<?php echo base_url(); ?>/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/jquery-ui/jquery-ui.min.js"></script>

<script src="<?php echo base_url(); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>



<script src="<?php echo base_url(); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/chart.js/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/sparklines/sparkline.js"></script>
<script src="<?php echo base_url(); ?>/plugins/jqvmap/jquery.vmap.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>/plugins/jqvmap/maps/jquery.vmap.world.js"></script> -->
<script src="<?php echo base_url(); ?>/plugins/jquery-knob/jquery.knob.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/summernote/summernote-bs4.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="<?php echo base_url(); ?>/dist/js/adminlte.js"></script>

<script>
    $.widget.bridge('uibutton', $.ui.button)
    $(function() {


        var areaChartCanvas = $('#g131').get(0).getContext('2d')

        var areaChartData = {
            labels: [<?php foreach ($s_summary["g131"]["labels"] as $label) : ?> "<?php echo $label; ?>", <?php endforeach; ?>],
            datasets: [{
                backgroundColor: 'rgba(60,141,188,0.9)',
                borderColor: 'rgba(60,141,188,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(60,141,188,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data: [<?php foreach ($s_summary["g131"]["data"] as $data) : ?> <?php echo $data; ?>, <?php endforeach; ?>],
            }]
        }

        var areaChartOptions = {
            maintainAspectRatio: true,
            responsive: true,
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false,
                    }
                }],
                yAxes: [{
                    gridLines: {
                        display: true,
                    }
                }]
            }
        }

        // This will get the first returned node in the jQuery collection.
        var areaChart = new Chart(areaChartCanvas, {
            type: 'line',
            data: areaChartData,
            options: areaChartOptions
        })

        var donutChartCanvas = $('#g133').get(0).getContext('2d')
        var donutData = {
            labels: [<?php foreach ($s_summary["g133"]["labels"] as $label) : ?> "<?php echo $label; ?>", <?php endforeach; ?>],

            datasets: [{
                data: [<?php foreach ($s_summary["g133"]["data"] as $data) : ?> <?php echo $data; ?>, <?php endforeach; ?>],
                backgroundColor: ['#f56954', '#F8E23B', '#3b8bba'],
            }]
        }
        var donutOptions = {
            maintainAspectRatio: false,
            responsive: true,
            legend: {
                display: true,
                position: 'top',
                align: 'left'
            },
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        var donutChart = new Chart(donutChartCanvas, {
            type: 'polarArea',
            data: donutData,
            options: donutOptions
        })

        $('#s_summary_table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'csv', 'excel'
            ],
            "paging": false,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });


        ////////////////////////////////////////////////////////
    });
</script>

<script src="<?php echo base_url(); ?>/plugins/jvectormap/jquery-jvectormap-2.0.5.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/jvectormap/jquery-jvectormap-world-mill.js"></script>
<script>
    $(function() {
        $('#g132').vectorMap({
            map: 'world_mill',
            scaleColors: ['#00FF00', '#0000FF'],
            normalizeFunction: 'polynomial',
            hoverOpacity: 0.7,
            hoverColor: false,
            markerStyle: {
                initial: {
                    fill: '#F8E23B',
                    stroke: '#383f47'
                }
            },
            regionStyle: {
                initial: {
                    fill: '#128da7'
                },
                hover: {
                    fill: "#A0D1DC"
                }
            },
            backgroundColor: '#FFFFFF',
            markers: [
                <?php foreach ($s_summary["g132"] as $city) : ?> {
                        latLng: [<?php echo $city["latitude"]; ?>, <?php echo $city["longitude"]; ?>],
                        name: '<?php echo $city["name"]; ?>'
                    },
                <?php endforeach; ?>

            ]
        });
    });
</script>