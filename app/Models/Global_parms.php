<?php
 namespace App\Models;

use CodeIgniter\Model;

class Global_parms extends Model
{
  protected $table      = 'GLOBAL_PARMS';
  protected $primaryKey = 'parm';
 
  protected $returnType = 'object';
  protected $tempReturnType = 'object';
  protected $useSoftDeletes = false;

  protected $allowedFields = ['parm','value','notes'];

  
  protected $db;
  
  public $row;
  
  public function __construct()
    {
        $this->db      = \Config\Database::connect();
    }
   public function getParm($parm)
   {
       $this->row = $this->find($parm);
       return $this->row;
   }
   public function setParm($parm,$value)
   {
     $this->update($parm,array('value' => $value));
   }
  }