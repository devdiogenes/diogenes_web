<?php
namespace App\Controllers;
use CodeIgniter\Controller;

class metacleaner extends Controller
{
    private $token = "";
    private $demo_token = "814f06ab7f40b2cff77f2c7bdffd3415"; // MD5 of "0101".
    private $file_default = "/var/www/metacleaner/public/metacleaner/archive/Suments.png";

    private $file_original = "";
    private $file_processed = "";
    
    private $path_archive = "/var/www/metacleaner/public/metacleaner/archive/";
    private $path_tmp = "/var/www/metacleaner//public/metacleaner/tmp";
    private $path_internal = "/var/www/metacleaner/public";

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);

        //parent::__construct();
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT");

        $token = $this->request->getPost("token");
        $url = $this->request->getPost("url");
        if ($url == NULL){
            $url_parts = explode("/", $this->file_default);
            $file_name_original = end($url_parts);
            
            $this->file_original = $this->file_default;
        }else{
            $url_parts = explode("/", $url);
            $file_name_original = end($url_parts);
            file_put_contents($this->path_archive.$file_name_original, file_get_contents($url));

            $this->file_original = $this->path_archive.$file_name_original;
        }
        $this->file_processed = $this->path_tmp ."SUMENTS_METACLEANER-" . date('YmdHis') . $file_name_original;
        copy($this->file_original, $this->file_processed);
    }

    public function index(){

        $this->send_response(["answer" => "hola"]);
        
        die();
    }

    public function demo(){
        
        $tmp = array();
        $response = array();

        $tmp = $this->metadata_read($this->file_original);
        $tmp = $this->format_output_shell($tmp);
        $response["original"] = $this->output_clean($tmp);

        $this->metadata_clean(($this->file_processed));
        $this->metadata_write_tags(($this->file_processed));
        $tmp = $this->metadata_read($this->file_processed);
        $tmp = $this->format_output_shell($tmp);
        $response["processed"] = $this->output_clean($tmp);

        $response["url"] = str_replace($this->path_internal, base_url(), $this->file_processed);

        $response["info"] = array();
        $response["info"]["datetime"] = date('YmdHis');
                
        $this->send_response($response);
    //////////////////////////////////////////////////
        if ($token != $this->demo_token){
        //raise error

        }
        else {
        //$this->get_demo($url);
        }


        return 1;
    }

    private function metadata_clean($file){
        shell_exec("exiftool -all= $file");

        return 1;
    }

    private function metadata_read($file){
        $metadata = shell_exec("exiftool $file");
        return ;
    }


    private function metadata_write_tags($file){
        $tags = array();

        $date = date('YmdHis');

        $tags["Author"] = "Suments Data. Metacleaner";
        $tags["url"] = "www.suments.com";

        $i = 0;
        $tags_str = ' ';
        foreach(array_keys($tags) as $key){
            $tags[$key] = str_replace("/", "\/", $tags[$key]);

            $tags_str = $tags_str . " -" . $key . "=" . '"'.$tags[$key] .'"'." ";
        }
        $cmd = "exiftool " . $tags_str . $file;
        echo $cmd;
        shell_exec($cmd);
    }


    private function format_output_shell($input){
        $tmp = array();
        $formatted = array();
        $tmp = explode("\n",$input);
        foreach($tmp as $current){
            
            $current = str_replace("  ", '', $current);

            $key_value = explode(":", $current);

            if (count($key_value) < 2){
                continue;
            }
            //echo var_dump($key_value);
            $formatted[$key_value[0]] = $key_value[1];
        }
        
        return $formatted;
    }

    private function output_clean($output){
        $to_be_removed = ["ExifTool Version Number ", "Directory"];
        foreach($to_be_removed as $current){
            unset($output[$current]);
            }
        
            return $output;
    }

    public function readFileUrl(){
        $input_url = $this->request->getPost("url");
        $input_headers = $this->request->getHeaders();

        //echo $input_url;
        //var_dump($input_headers);
        if ($input_url == ''){
        echo "Did you add the URL? =)";
        die();
        }

        $info = explode("/", $input_url);
        $name = end($info);
        $tnow =  date("dmyhms");
        $tmp_filename = $tnow."-".$name;

        $input_file = file_get_contents($input_url);
        file_put_contents($this->path_internal.$tmp_filename, $input_file);

        echo base_url()."/wptest/".$tmp_filename;
        //echo $this->path_internal.$name;
        die();

    }


    private function send_response($response){
        echo json_encode($response);
        die();
    }
}