<div class="content-header"></div>
<section class="content">
    <div class="container-fluid">

        <!-- summary-->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Summary</h3>
            </div>

            <div class="row">
                <div class="col-12 col-md-6">
                    <table class="table table-hover text-nowrap">
                        <tbody>
                            <tr>
                                <th>Total Leakages</th>
                                <td>[TOTAL LEAKAGES]</td>
                            </tr>
                            <tr>
                                <th>Company</th>
                                <td>[COMPANY NAME]</td>
                            </tr>
                            <tr>
                                <th>Seed domain</th>
                                <td>[SEED DOMAIN]</td>
                            </tr>
                            <tr>
                                <th>processing time</th>
                                <td>[TOTAL TIME]</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-12 col-md-6">
                    <table class="table table-hover text-nowrap">
                        <tbody>
                            <tr>
                                <th>Total subdomains</th>
                                <td>[TOTAL SUBDOMAINS]</td>
                            </tr>
                            <tr>
                                <th>Documents found</th>
                                <td>[TOTAL DOCUMENTS]</td>
                            </tr>
                            <tr>
                                <th>Total datafields</th>
                                <td>[TOTAL DATAFIELDS]</td>
                            </tr>
                            <tr>
                                <th>Seed domain</th>
                                <td>[SEED DOMAIN]</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- card 1 -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Potential security breaches</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="card-body">
                        <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                    </div>
                </div>

            </div>

            <div class="card-body">
                <table id="table_leakages" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Ocurrences</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>[METAVALUE WHERE SEVERITY >x] </td>
                            <td>[TOTAL OCURRENCES]</td>
                            <td>
                                <a href="#raw_data">
                                    <small class="badge badge-info" style="background-color:#3a8bba"><i class="far fa-clock"></i> Click to view details</small>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Ocurrences</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <!-- card 2 -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Documents</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="card-body">
                        <canvas id="barChart2" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="card-body">
                        <canvas id="pieChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                    </div>
                </div>

            </div>

            <div class="card-body">
                <table id="table_documents" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th></th>
                            <th>Country</th>
                            <th>Domain</th>
                            <th>Leakages</th>
                            <th>Link</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>[DOCUMENT ID]</td>
                            <td>[COUNTRY FALG UNICODE]</td>
                            <td>[COUNTRY NAME]</td>
                            <td>[DOMAIN NAME]</td>
                            <td>[TOTAL METAVALUES WHERE SEVERITY >x]</td>
                            <td>[DOCUMENT URL]</td>
                            <td>
                                <a href="#raw_data">
                                    <small class="badge badge-info" style="background-color:#3a8bba"><i class="far fa-clock"></i> Click to view details</small>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th></th>
                            <th>Country</th>
                            <th>Domain</th>
                            <th>Leakages</th>
                            <th>Link</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <!-- card 3 -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Domains</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="stackedBarChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="card-body">
                        <div id="world-map-front" style="height: 250px; width: 100%;"></div>
                    </div>
                </div>

            </div>

            <div class="card-body">
                <table id="table_domains" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Country</th>
                            <th>Domain</th>
                            <th>Leakages</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>[DOCUMENT ID]</td>
                            <td>[COUNTRY FALG UNICODE]</td>
                            <td>[DOMAIN NAME + DOMAIN HREF]</td>
                            <td>[TOTAL METAVALUES WHERE SEVERITY >x]</td>
                            <td>
                                <a href="#raw_data">
                                    <small class="badge badge-info" style="background-color:#3a8bba"><i class="far fa-clock"></i> Click to view details</small>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Country</th>
                            <th>Domain</th>
                            <th>Leakages</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <!-- card 4 -->
    <div class="card">
        <div class="card-header">
            <a name="raw_data"></a>
            <h3 class="card-title">Raw data</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>


        <div class="card-body">
            <table id="table_raw" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Data</th>
                        <th>Document</th>
                        <th>Domain</th>
                        <th>Country</th>
                        <th>Link</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>[ID METAVALUE]</td>
                        <td>[METAVALUE WHERE SEVERITY > x]</td>
                        <td>[DOCUMENT NAME WHERE id_metavalue = ID_METAVALUE]</td>
                        <td>[DOMAIN WHERE DOCUMENT IS]</td>
                        <td>[DOMAIN COUNTRY FLAG UNICODE</td>
                        <td>[DOCUMENT URL</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Data</th>
                        <th>Document</th>
                        <th>Domain</th>
                        <th>Country</th>
                        <th>Link</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</section>
</div>
