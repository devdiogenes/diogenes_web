<?php

namespace App\Models;

use CodeIgniter\Model;

class User2 extends Model
{
  protected $db;
  protected $table      = 'USERS';
  protected $primaryKey = 'id_user';

  protected $returnType = 'object';
  protected $allowedFields = ['name', 'password', 'email','language'];
  protected $useTimestamps = false;
  protected $createdField  = 'date_created';

  public $id;                       // user id field.
  public $loged = false;            // boolean. True if user is loged.
  public $userLevels;               // array con los niveles de permisos del usuario activo
  public $usuarioInterno = false;   // Flag. True if Sument's user
  public $userdata;                 // object with Database fields
  public $customer;                 // customer row (as object)
  public $wcenter;                  // object wcenter
  public $log;                      // object userLog

  public function __construct()
  {
    $this->db = \Config\Database::connect();
    $this->loged = !empty($_SESSION['user']);           // if true, the user is already loged
    if ($this->loged)
      $this->setvars();
    else
      $this->log = new \App\Models\Userlog(0);          // dummy userlog
  }
  public function log($text)
  {
    $this->log->log($text);
  }
  private function setVars()
  {

    $this->id = $_SESSION['user'];
    $this->log = new \App\Models\Userlog($this->id);
    $qry = "
      SELECT id_user, name, email, date_created,language
       FROM USERS
      WHERE id_user = {$this->id}
    ";
    $this->userdata = $this->db->query($qry)->getRow(); // Can't use find method, because we're in 
    //constructor and it's not ready yet.
    $this->getLevels();             // set up the userLevels array
    if ($this->usuarioInterno)
      $id_customer = 2;
    else {
      $qry = "
      SELECT id_customer
       FROM CUSTOMERS
      WHERE id_user = {$this->id}
      LIMIT 1
      ";
      $id_customer = $this->db->query($qry)->getRow()->id_customer;
    }
    $customer = new \App\Models\Customer();
    $this->customer = $customer->asObject()->where('id_customer', $id_customer)->findAll()[0]; // just the 1st element
    $this->wcenter = \App\Models\Wcenters::getMain($id_customer);
  }
  public function logout()
  {
    unset($_SESSION['user']);
  }
  public function login()
  {
    $token = $_POST['token'];
    $qry = "
        SELECT id_user, name, email, date_created,language
         FROM USERS
        WHERE md5(concat(name,password)) = '$token'
           OR md5(concat(email,password)) = '$token'
    ";
    $usr = $this->db->query($qry)->getRow();
    if ($usr == false)       // not found
      return "Invalid login/password";
    // success.
    $this->id = $usr->id_user;
    $_SESSION['user'] = $this->id;
    $this->loged = true;
    $this->setVars();
    return true;
  }
  public function ownsAnalysis($id_analysis)
  {
    if ($this->usuarioInterno)
      return true;
    $qry = "
      SELECT id_customer FROM ANALYSIS
        JOIN DOMAINS ON    (ANALYSIS.id_domain = DOMAINS.id_domain)
        JOIN COMPANIES ON  (DOMAINS.id_company = COMPANIES.id_company)
      WHERE ANALYSIS.id_analysis = $id_analysis
    ";
    $res = $this->db->query($qry)->getRow();
    if (!$res) // not found
    {
      $this->log("id_analysis not found ($id_analysis)");
      return (false);
    }
    if ($res->id_customer != $this->customer->id_customer) {
      $this->log("Ilegal attempt to acces to an analysis not owned ($id_analysis)");
      return (false);
    }
    return true;
  }
  private function getLevels()
  {
    $qry = " 
    SELECT level 
      FROM REL_USERS_CATEGORIES
      JOIN USERS_CATEGORIES ON (REL_USERS_CATEGORIES.id_category = USERS_CATEGORIES.id_user_category)
    WHERE id_user = {$this->id}
    ";
    $result = $this->db->query($qry)->getResult();
    foreach ($result as $r) {
      $this->userLevels[] = $r->level;
      if ($r->level > 1)
        $this->usuarioInterno = true;     // set flag to true 
    }
    if (!$result)  // no categories where set
    {
      $this->userLevels[] = [1];     //set to external user by default
      $this->usuarioInterno = false;     // set flag to false
    }
  } // end of function
} // end of class