<?php
 namespace App\Models;

use CodeIgniter\Model;

class UserTemp extends Model
{
  protected $table      = 'USERS_PENDING';
  protected $primaryKey = 'id';
 
  protected $returnType = 'object';
  protected $useSoftDeletes = false;

  protected $allowedFields = ['company', 'domain','customer','email','psw1','rndkey'];

  protected $useTimestamps = false;
  protected $createdField  = 'fecha';
  protected $updatedField  = '';
  protected $deletedField  = '';

  protected $validationRules    = [];
  protected $validationMessages = [];
  protected $skipValidation     = true;

  public $usertemp;

        
  /**
   * Retrieves one record from transitory table
   * @param string $key
   * @return object database row
   */
  public function getKey($key)
  {
   $query = "SELECT * FROM USERS_PENDING 
             WHERE rndkey = '$key'";
   $this->usertemp = $this->db->query($query)->getRow();
   return $this->usertemp;
  }
  
}
